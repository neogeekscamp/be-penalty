﻿using System.ComponentModel.DataAnnotations;
using Abp.Auditing;

namespace VDI.Demo.Authorization.Accounts.Dto
{
    public class ResetPasswordInput
    {
        //public string encryptedParam { get; set; }

        public string Username { get; set; }

        //[Required]
        //public string ResetCode { get; set; }

        [Required]
        [DisableAuditing]
        public string Password { get; set; }

    //    public string ReturnUrl { get; set; }

    //    public string SingleSignIn { get; set; }
    }
}