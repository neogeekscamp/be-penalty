﻿using System.Threading.Tasks;
using Abp.Application.Services;
using VDI.Demo.Authorization.Accounts.Dto;
using VDI.Demo.FM_API.UserPP.dto;

namespace VDI.Demo.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {

        Task<RegisterOutput> RegisterMember(RegisterMemberInput input);

        Task<RegisterOutput> RegisterUserPP(RegisterUserPPInput input);

        Task<RegisterOutput> RegisterCustomer(RegisterCustomerInput input);

        Task SendPasswordResetCode(SendPasswordResetCodeInput input);


    }
}
