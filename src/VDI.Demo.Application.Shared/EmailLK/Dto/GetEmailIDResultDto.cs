﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.EmailLK.Dto
{
    public class GetEmailIDResultDto
    {
        public string emailID { get; set; }

        public string counterCode { get; set; }
    }
}
