﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.FM_API.Personals.Dto;
using VDI.Demo.FM_API.SalesWeb;

namespace VDI.Demo.EmailLK
{
    public interface IEmailLKAppService : IApplicationService
    {
        string SendEmailAttachmentUniversal(SendEmailAttachmentInputDto input);

        string SendEmail(SendEmailPersInputDto input);
    }
}
