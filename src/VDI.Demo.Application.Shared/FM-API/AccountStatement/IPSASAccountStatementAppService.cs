﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.FM_API.Payment.Dto;

namespace VDI.Demo.FM_API.AccountStatement
{
    public interface IPSASAccountStatementAppService : IApplicationService
    {
        GetListBookingHistory GetListBookingHistoryByPhoneOrEmail(string mobilePhone, string email);
        GetNewDetailAccountStatementListDto GetDetailAccountStatement(string psCode, string email);
    }
}
