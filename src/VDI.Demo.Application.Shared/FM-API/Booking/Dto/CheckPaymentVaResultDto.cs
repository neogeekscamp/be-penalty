﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Booking.Dto
{
    public class CheckPaymentVaResultDto
    {
        public string noVirtualAccount { get; set; }
        public string amount { get; set; }
        public string transactionStatus { get; set; }
        public string executionTime { get; set; }
        public string statusEngine3 { get; set; }
    }
}
