﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Booking.Dto
{
    public class DoBookingE3InputDto
    {
        public string order_id { get; set; }
        public string gross_amount { get; set; }
        public string transaction_status { get; set; }


    }
}
