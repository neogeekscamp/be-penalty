﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Booking.Dto
{
    public class DoBookingsInputDto
    {
        public string noVirtualAccount { get; set; }
        public decimal amount { get; set; }
        public string transactionStatus { get; set; }
    }
}
