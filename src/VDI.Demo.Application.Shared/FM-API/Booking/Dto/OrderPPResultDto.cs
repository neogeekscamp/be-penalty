﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Booking.Dto
{
    public class OrderPPResultDto
    {
        public string ordercode { get; set; }
        public int qty { get; set; }
        public decimal ppamt { get; set; }
        public decimal totalAmount { get; set; }
        public string statustypename { get; set; }
        public string paymenttypename { get; set; }
    }
}
