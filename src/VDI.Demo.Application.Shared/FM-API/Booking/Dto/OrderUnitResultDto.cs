﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Booking.Dto
{
    public class OrderUnitResultDto
    {
        public decimal bfamount { get; set; }
        public string VA_BankAccNo { get; set; }
        public string ordercode { get; set; }
        public string paymenttypename { get; set; }
    }
}
