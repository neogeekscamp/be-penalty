﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Customer.Dto
{
    public class BuyPPMidtransReqInputDto
    {
        public int PPOrderID { get; set; }

        public int paymentTypeID { get; set; }

        public string orderCode { get; set; }

        public string psCode { get; set; }

        public double totalOrder { get; set; }

        public string voucher { get; set; }

    }
}
