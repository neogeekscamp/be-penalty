﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Customer.Dto
{
    public class GetBillsMeikartaInputDto
    {
        public string bookCode { get; set; }

        public string unitCode { get; set; }
        
        public DateTime? dueDate { get; set; }
        
    }
}
