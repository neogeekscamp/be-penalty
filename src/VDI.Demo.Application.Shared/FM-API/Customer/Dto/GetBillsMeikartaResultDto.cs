﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Customer.Dto
{
    public class GetBillsMeikartaResultDto
    {
        public string bookCode { get; set; }

        public string unitCode { get; set; }

        public string unitNo { get; set; }

        public string paymentMethod { get; set; }

        public List<InstallmentDto> listInstallment { get; set; }
        
    }

    public class InstallmentDto
    {
        public string noOfInstallment { get; set; }

        public string paymentType { get; set; }
        
        public decimal amount { get; set; }

        public DateTime dueDate { get; set; }
    }
    
}
