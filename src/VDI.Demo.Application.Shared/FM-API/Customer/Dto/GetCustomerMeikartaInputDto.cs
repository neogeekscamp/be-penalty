﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Customer.Dto
{
    public class GetCustomerMeikartaInputDto
    {
        public string bookCode { get; set; }

        public string unitCode { get; set; }
        
        public string email { get; set; }
        
        public string virtualAccount { get; set; }

        public int numberOfView { get; set; }
    }
}
