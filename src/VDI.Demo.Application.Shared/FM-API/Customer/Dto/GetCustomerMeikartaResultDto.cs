﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Customer.Dto
{
    public class GetCustomerMeikartaResultDto
    {
        public string bookCode { get; set; }

        public string unitCode { get; set; }

        public string unitNo { get; set; }

        public string psCode { get; set; }

        public string paymentMethod { get; set; }

        public string virtualAccount { get; set; }

        public double area { get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public string phoneNumber { get; set; }

        public string address { get; set; }

        public string province { get; set; }

        public string city { get; set; }

        public string zipCode { get; set; }

        public int isInstitute { get; set; }

        public string NPWP { get; set; }

        public string document { get; set; }

        public string documentBinary { get; set; }

        public string customerType { get; set; }
        
    }
    
}
