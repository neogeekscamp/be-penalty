﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.FM_API.Customer.Dto
{
    public class TrBuyPPOnlineInputDto
    {
        public int projectProductID { get; set; }

        public string projectCode { get; set; }

        public int termRefID { get; set; }

        public string preferredTypeName { get; set; }

        public int PPQuantity { get; set; }

        public decimal totalPrice { get; set; }

        public int? bankID1 { get; set; }

        public int? bankID2 { get; set; }

        public int paymentTypeID { get; set; }

        public string psCodeCust { get; set; }

        public int userID { get; set; }

        //[Required]
        public string orderCode { get; set; }
    }
}
