﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.DocumentKPR.Dto
{
    public class DocumentKPRInputDto
    {
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string psCode { get; set; }
        //public int bookingHeaderID { get; set; }
        public string documentType { get; set; }
        public string documentFile { get; set; }
        public string documentPicType { get; set; }
        public bool isVerified { get; set; }
        public string remarkCustomer { get; set; }
    }

    public class UpdateDocumentKPRInputDto
    {
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string psCode { get; set; }
        public string documentType { get; set; }
        public bool isVerified { get; set; }
        public string remarks { get; set; }

    }

}
