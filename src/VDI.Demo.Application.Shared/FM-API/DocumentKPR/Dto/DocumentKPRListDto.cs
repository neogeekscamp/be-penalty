﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.DocumentKPR.Dto
{
    public class DocumentKPRListDto
    {
        public int bookingHeaderId { get; set; }
        public string bookCode { get; set; }
        public string psCode { get; set; }
        public string documentType { get; set; }
        public string documentName { get; set; }
        public bool isVerified { get; set; }
        public string remarks { get; set; }
        public string remarkCustomer { get; set; }
        public DateTime? canceldate { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string documentPicType { get; set; }
        public byte[] documentBinary { get; set; }

    }

    public class DocumentKPRResultDto
    {
        public int bookingHeaderId { get; set; }
        public string bookCode { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string psCode { get; set; }
        public string documentType { get; set; }
        public string documentName { get; set; }
        public bool isVerified { get; set; }
        public string remarks { get; set; }
        public string remarkCustomer { get; set; }
        public string documentPicType { get; set; }
        public string documentBase64 { get; set; }

    }
}
