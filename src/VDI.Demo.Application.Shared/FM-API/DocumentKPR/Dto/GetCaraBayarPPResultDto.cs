﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.DocumentKPR.Dto
{
    public class GetCaraBayarPPResultDto
    {
        public int no { get; set; }
        public string OrderCode { get; set; }
        public string PPNo { get; set; }
        public int termRefID { get; set; }
        public string termRefName { get; set; }
    }
}
