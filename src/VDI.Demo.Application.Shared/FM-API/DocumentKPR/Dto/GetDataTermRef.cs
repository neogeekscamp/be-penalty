﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.DocumentKPR.Dto
{
    public class GetDataTermRef
    {
        public int termRefID { get; set; }
        public string termRefName { get; set; }
    }
}
