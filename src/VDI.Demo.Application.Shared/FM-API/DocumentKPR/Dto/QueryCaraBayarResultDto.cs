﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.DocumentKPR.Dto
{
    public class QueryCaraBayarResultDto
    {
        public string ORDERCODE { get; set; }
        public string PPNO { get; set; }
        public int TERMREFID { get; set; }
        public int URUT { get; set; }
        public string NAMATABLE { get; set; }
        public int IDTABLE { get; set; }
    }
}
