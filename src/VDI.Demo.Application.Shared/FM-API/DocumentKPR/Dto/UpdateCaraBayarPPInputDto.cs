﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.DocumentKPR.Dto
{
    public class UpdateCaraBayarPPInputDto
    {
        public string PPNo { get; set; }
        public int termRefId { get; set; }
    }
}
