﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.EmailPP.Dto
{
    public class SendEmailPPInputDto
    {
        public string toAddress { get; set; }

        public string ccAddress { get; set; }

        public string subject { get; set; }

        public string body { get; set; }

        public string pathTTBF { get; set; }

    }
}
