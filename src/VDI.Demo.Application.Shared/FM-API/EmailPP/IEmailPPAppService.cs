﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.FM_API.Personals.Dto;

namespace VDI.Demo.FM_API.EmailPP
{
    public interface IEmailPPAppService
    {
        string BookingPPSuccess(BookingSuccessPPInputDto input);

        string bookingSuccess(BookingSuccessInputDto input);

        void ConfigurationEmailPP(SendEmailPPInputDto input);

        void ConfigurationEmail(SendEmailInputDto input);
    }
}
