﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.InventoryUnit.Dto
{
    public class GetInventoryUnitDto
    {
        public string projectcode { get; set; }
        public string projectName { get; set; }
        public int unitID { get; set; }
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public string unitNo { get; set; }
        public string blockCode { get; set; }
        public string clusterName { get; set; }
        public string status { get; set; }
        public double size { get; set; }
        public decimal price { get; set; }
        public double pctTax { get; set; }
        public double pctDisc { get; set; }
    }
}
