﻿namespace VDI.Demo.FM_API.InventoryUnit.Dto
{
    public class GetInventoryUnitInputDto
    {
        public string unitCode { get; set; }
        public int numberOfView { get; set; }
    }
}
