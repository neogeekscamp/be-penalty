﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.InventoryUnit.Dto
{
    public class GetInventoryUnitListDto
    {
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public string noUnit { get; set; }
        public string blockCode { get; set; }
        public string clusterName { get; set; }
        public string towerName { get; set; }

        public string projectCode { get; set; }
        public string projectName { get; set; }

        public string status { get; set; }
        public double typeSize { get; set; }

        public List<ListPriceDto> listPrice { get; set; }
    }
    public class ListPriceDto
    {
        public string termName { get; set; }
        public decimal price { get; set; }
        public string termCode { get; set; }
        public string renovCode { get; set; }
        public short termNo { get; set; }
    }
}
