﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Notifications.Dto
{
    public class GetDataEmailReminderPaymentDto
    {
        public string bookCode { get; set; }
        public string psCode { get; set; }
        public string coCode { get; set; }
        public string allocDesc { get; set; }
        public string unitNo { get; set; }
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public string templateHtml { get; set; }
        public string letterType { get; set; }
        public DateTime dueDate { get; set; }
        public int projectID { get; set; }
        public int clusterID { get; set; }
        public decimal totalAmt { get; set; }
        public string inputEmail { get; set; }
        public string messageInd { get; set; }
        public string messageEng { get; set; }
        public bool isOver { get; set; }
    }
}
