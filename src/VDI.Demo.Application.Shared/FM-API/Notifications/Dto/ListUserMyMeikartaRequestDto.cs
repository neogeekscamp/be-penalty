﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Notifications.Dto
{
    public class ListUserMyMeikartaRequestDto
    {
        public RequestListUser filter { get; set; }
    }

    public class RequestListUser
    {
        public int project_id { get; set; }
        public bool is_portal { get; set; }
        public bool is_mobile { get; set; }
        public bool has_external_id { get; set; }
    }
}
