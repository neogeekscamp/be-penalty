﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Notifications.Dto
{
    public class ListUserMyMeikartaResponseDto
    {
        public List<LoginMyMeikartaResponseDto> users { get; set; }
    }
}
