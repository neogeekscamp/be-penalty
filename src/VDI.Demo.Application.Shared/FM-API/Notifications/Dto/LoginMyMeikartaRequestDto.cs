﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Notifications.Dto
{
    public class LoginMyMeikartaRequestDto
    {
        public string phone_number { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public bool is_sosmed { get; set; }
        public int project_id { get; set; }
        public bool is_portal { get; set; }
    }
}
