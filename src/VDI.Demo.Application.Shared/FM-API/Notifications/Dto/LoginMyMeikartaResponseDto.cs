﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Notifications.Dto
{
    public class LoginMyMeikartaResponseDto
    {
        public string phone_number { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int user_id { get; set; }
        public int project_id { get; set; }
        public string nick_name { get; set; }
        public string token { get; set; }
        public DateTime token_expired { get; set; }

        public string email { get; set; }
        public string external_id { get; set; }
    }
}
