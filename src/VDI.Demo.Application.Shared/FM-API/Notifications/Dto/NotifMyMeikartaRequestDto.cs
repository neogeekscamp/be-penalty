﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Notifications.Dto
{
    public class NotifMyMeikartaRequestDto
    {
        public string token { get; set; }
        public List<RequestNotif> listRequest { get; set; }
    }

    public class RequestNotif
    {
        public List<MessageDto> messages { get; set; }
        public int project_id { get; set; }
        public int site_id { get; set; }
        public string email { get; set; }
        public bool is_upload_image { get; set; }
        public int notification_sub_category_id { get; set; }

        public string bookCode { get; set; }
        public string notifTypeCode { get; set; }
    }

    public class MessageDto
    {
        public string message { get; set; }
        public string header { get; set; }
        public string language { get; set; }

        public string messageEnglish { get; set; }
        public string psCode { get; set; }
        public string bookCode { get; set; }
        public string letterType { get; set; }
        public string email { get; set; }
    }
}
