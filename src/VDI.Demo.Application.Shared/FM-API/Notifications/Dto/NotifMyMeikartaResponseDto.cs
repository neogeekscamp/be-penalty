﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Notifications.Dto
{
    public class NotifMyMeikartaResponseDto
    {
        public string id { get; set; }
        public int receipts { get; set; }
        public string code { get; set; }
        public string detail { get; set; }
        public string status { get; set; }
    }
}
