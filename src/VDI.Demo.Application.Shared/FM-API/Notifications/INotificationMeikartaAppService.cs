﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.FM_API.Notifications.Dto;

namespace VDI.Demo.FM_API.Notifications
{
    public interface INotificationMeikartaAppService : IApplicationService
    {
        LoginMyMeikartaResponseDto LoginMyMeikarta();
        List<NotifMyMeikartaResponseDto> NotifMyMeikarta(NotifMyMeikartaRequestDto input);
        ListUserMyMeikartaResponseDto ListUserMyMeikarta(string token);
        void SchedulerNotifOR();
        void SchedulerNotifORByTransNo(List<string> listTransNo);
        void SchedulerNotifRP();
        void SchedulerNotifRPByBookCode(List<string> listBookCode);
        void SchedulerNotifSP();
        void SchedulerNotifSPByBookCode(List<string> listBookCode);
    }
}
