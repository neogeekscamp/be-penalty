﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.PSASSchedule.Dto
{
    public class GenerateTransNoInputDto
    {
        public int accID { get; set; }

        public int entityID { get; set; }
    }
}
