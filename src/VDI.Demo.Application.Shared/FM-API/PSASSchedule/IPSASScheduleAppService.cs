﻿using Abp.Application.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.FM_API.PSASSchedule.Dto;

namespace VDI.Demo.FM_API.PSASSchedule
{
    public interface IPSASScheduleAppService : IApplicationService
    {
        JObject GenerateTransNo(GenerateTransNoInputDto input);
    }
}
