﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class GetAgingOutstandingInputDto
    {
        public string bookNo { get; set; }
        public DateTime? dueDate { get; set; }
    }
}
