﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class GetAgingOutstandingResultDto
    {
        public string bookNo { get; set; }
        public string allocCode { get; set; }
        public short schedNo { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public decimal amount { get; set; }
        public DateTime dueDate { get; set; }
    }
}
