﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class GetDataSchedulePaymentInputDto
    {
        public int payForID { get; set; }
        public int bookingHeaderID { get; set; }
        public int accountID { get; set; }
    }
}
