﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class GetDetailAccountStatementNewListDto
    {
        public List<GetDetailAccountStatementListDto> ListAccountStatement { get; set; }
    }
    public class GetDetailAccountStatementListDto
    {
        public string psCode { get; set; }
        public string name { get; set; }
        public List<GetPeriode> periode { get; set; }
    }

    public class GetPeriode
    {
        public int periodeNo { get; set; }
        public decimal totalPreviousBalance { get; set; }
        public decimal totalCurrentTransaction { get; set; }
        public decimal totalPayment { get; set; }
        public decimal totalBillingOutstanding { get; set; }
        public List<GetUnit> unit { get; set; }
    }

    public class GetUnit
    {
        public string periodeName { get; set; }
        public string periodeDate { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string virtualAccount { get; set; }
        public decimal previousBalance { get; set; }
        public decimal currentTransaction { get; set; }
        public decimal payment { get; set; }
        public decimal billingOutstanding { get; set; }
        public List<GetDetail> detail { get; set; }
    }

    public class GetDetail
    {
        public string transactionDate { get; set; }
        public string transactionDesc { get; set; }
        public decimal transactionAmount { get; set; }
        public decimal totalPayment { get; set; }
    }

    public class GetDueDate
    {
        public DateTime dueDate { get; set; }
        public DateTime dueDateLast { get; set; }
        public decimal amt { get; set; }
        public decimal outs { get; set; }
        public int bookingHeaderID { get; set; }
        public int allocID { get; set; }
        public string allocDesc { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string virtualAccount { get; set; }
        public decimal previousBalance { get; set; }
    }

    public class GetUnitDetail
    {
        public int unitID { get; set; }
        public string bookCode { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public decimal previousBalance { get; set; }
        public decimal currentTransaction { get; set; }
        public decimal payment { get; set; }
        public decimal billingOutstanding { get; set; }
        public DateTime transactionDate { get; set; }
        public string transactionDesc { get; set; }
        public decimal transactionAmount { get; set; }
    }
}