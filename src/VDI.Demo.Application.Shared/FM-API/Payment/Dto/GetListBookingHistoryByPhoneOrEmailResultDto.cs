﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class GetListBookingHistory
    {
        public List<GetListBookingHistoryByPhoneOrEmailResultDto> ListBookingHistory { get; set; }
    }
    public class GetListBookingHistoryByPhoneOrEmailResultDto
    {
        public string psCode { get; set; }
        public DateTime? birthDay { get; set; }
        public string email { get; set; }
        public string mobilePhone { get; set; }
        public string name { get; set; }
        public List<UnitHistoryDto> Units { get; set; }

    }
    public class UnitHistoryDto
    {
        public int orgID { get; set; }
        public int siteID { get; set; }
        public string psCode { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string unitClusterCode { get; set; }
        public string bookingCode { get; set; }
        public string virtualAccount { get; set; }
    }
}
