﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class GetNewDetailAccountStatementListDto
    {
        public decimal FinalTotalBillingOutstanding { get; set; }
        public decimal FinalTotalpayment { get; set; }
        public string ReturnMessage { get; set; }
        public List<GetDetailAccountStatementListDtoNew> ListAccountStatement { get; set; }
    }

    public class GetDetailAccountStatementListDtoNew
    {
        public string psCode { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public List<Bookcodes> Bookcodes { get; set; }
    }

    public class Bookcodes
    {
        public string Bookcode { get; set; }
        public List<GetPeriodeNew> periodes { get; set; }
    }

    public class GetPeriodeNew
    {
        public int periodeNo { get; set; }
        public decimal totalPreviousBalance { get; set; }
        public decimal totalCurrentTransaction { get; set; }
        public decimal totalPayment { get; set; }
        public decimal totalBillingOutstanding { get; set; }
        public int unitID { get; set; }
        public List<GetUnitNew> unit { get; set; }
    }

    public class GetUnitNew
    {
        public string periodeName { get; set; }
        public string periodeDate { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string virtualAccount { get; set; }
        public decimal previousBalance { get; set; }
        public decimal currentTransaction { get; set; }
        public decimal payment { get; set; }
        public decimal billingOutstanding { get; set; }
        public List<GetDetailNew> detail { get; set; }
    }

    public class GetDetailNew
    {
        public string transactionDate { get; set; }
        public string transactionDesc { get; set; }
        public decimal transactionAmount { get; set; }
        public decimal totalPayment { get; set; }
    }

    public class StatementModel
    {
        public int bookingHeaderID { get; set; }
        public string psCode { get; set; }
        public string allocDesc { get; set; }
        public int unitID { get; set; }
        public DateTime dueDate { get; set; }
        public int month_ { get; set; }
        public int year_ { get; set; }
        public decimal currentB { get; set; }
        public decimal PreviousB { get; set; }
        public decimal TotalBill { get; set; }
        public decimal Amount { get; set; }
    }

    
}
