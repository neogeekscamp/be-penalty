﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class GetPaymentInputDto
    {
        public string bookNo { get; set; }
        public string unitCode { get; set; }
    }
}
