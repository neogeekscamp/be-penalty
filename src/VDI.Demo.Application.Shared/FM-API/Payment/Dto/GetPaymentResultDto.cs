﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class GetPaymentResultDto
    {
        public string bookNo { get; set; }
        public string unitCode { get; set; }
        public string paymentResource { get; set; }
        public decimal amount { get; set; }
        public DateTime paymentDate { get; set; }
        public string paymentType { get; set; }
        public decimal? taxBasisAmount { get; set; }
        public decimal? taxAmount { get; set; }
        public string accCode { get; set; }
        public string noFakturPajak { get; set; }
        public string receiptNumber { get; set; }
        public string projectCode { get; set; }
    }
}
