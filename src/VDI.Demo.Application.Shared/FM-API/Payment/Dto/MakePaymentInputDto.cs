﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.Dto
{
    public class MakePaymentInputDto
    {
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string paymentType { get; set; }
        public decimal amount { get; set; }
        public DateTime paymentDate { get; set; }
        public string payForCode { get; set; }
        public string payType { get; set; }
        public string othersTypeCode { get; set; }
    }
}
