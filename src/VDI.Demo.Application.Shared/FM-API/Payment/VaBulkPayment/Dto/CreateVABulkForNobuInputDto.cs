﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.VaBulkPayment.Dto
{
    public class CreateVABulkForNobuInputDto
    {
        public string virtualAccount { get; set; }
        public string virtualAccountBank { get; set; }
        public string jenisTransaksi { get; set; }
        public string jamTransaksi { get; set; }
        public string keterangan { get; set; }
        public decimal amount { get; set; }
        public DateTime clearDate { get; set; }
        public DateTime paymentDate { get; set; }
        public string refNo { get; set; }
        public string accountNo { get; set; }
    }

}
