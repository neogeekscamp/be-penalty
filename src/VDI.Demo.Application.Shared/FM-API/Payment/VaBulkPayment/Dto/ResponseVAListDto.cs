﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Payment.VaBulkPayment.Dto
{
    public class ResponseVAListDto
    {
        public string response { get; set; }
        public string unit { get; set; }
        public string psName { get; set; }
    }

}
