﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.FM_API.Payment.VaBulkPayment.Dto;

namespace VDI.Demo.FM_API.Payment.VaBulkPayment
{
    public interface IVaBulkPaymentAppService : IApplicationService
    {
        List<GetDataCheckUploadVaBulkListDto> CheckDataUploadVaBulk(List<CheckDataVaBulkInputDto> input);
        ResponseVAListDto CreateVABulkForNobu(CreateVABulkForNobuInputDto input);
        List<GetDataCheckUploadVaBulkListDto> CreateUniversalBulkPayment(List<CreateUniversalVaBulkPaymentInputDto> input);
    }
}
