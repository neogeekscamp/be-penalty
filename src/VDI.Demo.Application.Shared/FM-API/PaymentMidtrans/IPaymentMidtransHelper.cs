﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.FM_API.PaymentMidtrans.Dto;

namespace VDI.Demo.FM_API.PaymentMidtrans
{
    public interface IPaymentMidtransHelper : IApplicationService
    {
        PaymentOnlineBookingResponse ValidateResponseStatus(PaymentOnlineBookingResponse orderCode);

        RequestTokenResultDto ValidateResponseToken(RequestTokenResultDto orderCode);

    }
}
