﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.FM_API.Customer.Dto;
using VDI.Demo.FM_API.PaymentMidtrans.Dto;

namespace VDI.Demo.FM_API.PaymentMidtrans
{
    public interface IPaymentOBAppService :IApplicationService
    {
        Task<PaymentOnlineBookingResponse> CreatePayment(PaymentOnlineBookingRequest input);
        Task<PaymentOnlineBookingResponse> CreatePaymentLite(PaymentOnlineBookingRequest input);
    }
}
