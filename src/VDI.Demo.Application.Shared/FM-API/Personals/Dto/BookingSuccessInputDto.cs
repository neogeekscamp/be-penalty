﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class BookingSuccessInputDto
    {
        public string projectImage { get; set; }

        public string customerName { get; set; }

        public string bookDate { get; set; }

        public string devPhone { get; set; }

        public string memberName { get; set; }

        public string memberPhone { get; set; }

        public string projectName { get; set; }

        public string email { get; set; }

        public string urlNotaris { get; set; }

        public KonfirmasiPesananDto listDataOrder { get; set; }

        public ListunitDto listDataUnit { get; set; }
    }
}
