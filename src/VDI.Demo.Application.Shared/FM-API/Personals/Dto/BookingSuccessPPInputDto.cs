﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class BookingSuccessPPInputDto
    {
        public string projectName { get; set; }

        public string projectImage { get; set; }

        public string customerName { get; set; }

        public string BFPaymentDate { get; set; }

        public string PPNo { get; set; }

        public string qty { get; set; }

        public string totalPaymentAmount { get; set; }

        public string emailDealCloser { get; set; }

        public string emailCustomer { get; set; }

        public string developerName { get; set; }

    }
}
