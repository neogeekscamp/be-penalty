﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class CreatePersonalInputDto
    {
        public string NPWP { get; set; }
        public string birthPlace { get; set; }
        public DateTime birthDate { get; set; }
        public bool isInstitute { get; set; }
        public string marCode { get; set; }
        public string name { get; set; }
        public string nationId { get; set; }
        public string occID { get; set; }
        public string sex { get; set; }
        public List<CreateAddressInputDto> listAddress { get; set; }
        public string email { get; set; }
        public List<CreatePhoneInputDto> listPhoneNumber { get; set; }
        public List<CreateIDDto> listID { get; set; }
        public List<CreateDocumentDto> listDocument { get; set; }

        public int projectProductID { get; set; }
        public string projectCode { get; set; }
        public int termRefID { get; set; }
        public int PPQuantity { get; set; }
        public decimal totalPrice { get; set; }
        public int? bankID1 { get; set; }
        public int? bankID2 { get; set; }
        public int paymentTypeID { get; set; }
        public int userID { get; set; }
        public double totalOrder { get; set; }

        public string unitNo { get; set; }
        public string unitCode { get; set; }
        public short termNo { get; set; }
        public string termCode { get; set; }
        public string renovCode { get; set; }

        //Param DoBookingMidtransReq
        public int? tujuanTransaksiID { get; set; }
        public int? sumberDanaID { get; set; }
        public string nomorRekeningPemilik { get; set; }
        public string bankRekeningPemilik { get; set; }
    }

    public class CreateAddressInputDto
    {
        public string addrType { get; set; }
        public string address { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string postCode { get; set; }
        public string province { get; set; }

    }

    public class CreatePhoneInputDto
    {
        public string entityCode { get; set; }
        public int refID { get; set; }
        public string phoneType { get; set; }
        public string number { get; set; }
        public string remarks { get; set; }
    }

    public class CreateIDDto
    {
        public int refID { get; set; }
        public string idType { get; set; }
        public string idNo { get; set; }
    }

    public class CreateDocumentDto
    {
        public string entityCode { get; set; }
        public string psCode { get; set; }
        public string documentType { get; set; }
        public int? documentRef { get; set; }
        public string documentBinary { get; set; }
        public string documentPicType { get; set; }
    }
}
