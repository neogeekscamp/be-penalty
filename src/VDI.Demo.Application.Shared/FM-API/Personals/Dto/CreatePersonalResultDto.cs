﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class CreatePersonalResultDto
    {
        public string bookCode { get; set; }

        public string message { get; set; }
        
        public bool result { get; set; }
    }
}
