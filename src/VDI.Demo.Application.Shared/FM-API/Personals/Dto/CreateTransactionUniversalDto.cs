﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class CreateTransactionUniversalDto
    {
        public int payTypeID { get; set; }
        public string pscode { get; set; }
        public decimal totalAmt { get; set; }
        public int orderHeaderID { get; set; }
        public string orderCode { get; set; }

        public int? tujuanTransaksiID { get; set; }
        public int? sumberDanaID { get; set; }
        public string nomorRekeningPemilik { get; set; }
        public string bankRekeningPemilik { get; set; }

        public string otherPaymentType { get; set; }

        public List<UnitUniversalResultDto> arrUnit { get; set; }
        //public List<PPNUniversalResultDto> arrPP { get; set; }

        public long userID { get; set; }
        public string memberCode { get; set; }
        public string memberName { get; set; }
        public string scmCode { get; set; }
    }

    public class PDFUniversalResultDto
    {
        public string urlPdf { get; set; }
    }

    public class PPNUniversalResultDto
    {
        public string PPNo { get; set; }
    }

    public class UnitUniversalResultDto
    {
        public int unitID { get; set; }
        public int termID { get; set; }
        public int renovID { get; set; }
        public int projectID { get; set; }
        public decimal sellingprice { get; set; }
        public decimal bfAmount { get; set; }
        public int? bankID1 { get; set; }
        public int? bankID2 { get; set; }
        public string ppNo { get; set; }
    }
    public class ListPriceDto
    {
        public decimal grossprice { get; set; }
        public double pctTax { get; set; }
        public double pctDisc { get; set; }
    }
}
