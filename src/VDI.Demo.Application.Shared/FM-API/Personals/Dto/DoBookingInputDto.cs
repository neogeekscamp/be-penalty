﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class DoBookingInputDto
    {
        public string orderCodePP { get; set; }
        public string orderCodeUnit { get; set; }
        public string transaction_status { get; set; }
    }
}
