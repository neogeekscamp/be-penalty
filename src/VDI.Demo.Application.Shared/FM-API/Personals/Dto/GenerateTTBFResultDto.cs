﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GenerateTTBFResultDto
    {
        public string url { get; set; }
        public string path { get; set; }
        public List<GenerateTTBFInputDto> dataTTBF { get; set; }
        public string totalAmountTTBF { get; set; }
        public string emailDealCloser { get; set; }
        public string fileName { get; set; }
    }
}
