﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GetBankKPAPPListDto
    {
        public int bankId { get; set; }

        public string bankName { get; set; }

        public string bankCode { get; set; }
    }
}
