﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GetCustomerOccupationDto
    {
        public string psCode { get; set; }
        public string name { get; set; }
        public string occID { get; set; }
        public string occDescription { get; set; }
    }
}
