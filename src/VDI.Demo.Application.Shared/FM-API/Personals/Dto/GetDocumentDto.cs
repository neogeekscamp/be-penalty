﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GetDocumentDto
    {
        public string psCode { get; set; }
        public string documentType { get; set; }
        public string documentPicType { get; set; }
        public string documentTypeName { get; set; }
        public string filename { get; set; }
        public string documentBinary { get; set; }
        public string remarkCustomer { get; set; }
        public string LastModificationTime { get; set; }
        public string LastModifierUserId { get; set; }
        public string CreationTime { get; set; }
        public string CreatorUserId { get; set; }
    }
}
