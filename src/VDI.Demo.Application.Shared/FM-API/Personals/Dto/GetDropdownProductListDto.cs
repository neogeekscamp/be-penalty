﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GetDropdownProductListDto
    {
        public int projectPPID { get; set; }

        public int projectProductID { get; set; }

        public string productCode { get; set; }

        public string productName { get; set; }
    }
}
