﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GetDropdownProjectListDto
    {
        public string projectCode { get; set; }

        public string projectName { get; set; }
    }
}
