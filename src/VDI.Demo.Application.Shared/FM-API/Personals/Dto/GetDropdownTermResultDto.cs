﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GetDropdownTermResultDto
    {
        public int termID { get; set; }

        public string termName { get; set; }
    }
}
