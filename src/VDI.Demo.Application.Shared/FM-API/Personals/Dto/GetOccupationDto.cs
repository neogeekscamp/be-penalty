﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GetOccupationDto
    {
        public string occupationID { get; set; }
        public string occupationName { get; set; }
    }
}
