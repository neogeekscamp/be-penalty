﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class GetScheduleUniversalsDto
    {
        public double pctTax { get; set; }
        public List<GetSchedulerListDto> dataSchedule { get; set; }
    }
}
