﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class InsertPaymentOfflineInputDto
    {
        public string paymentDate { get; set; }

        public int paymentTypeID { get; set; }

        public int bankID { get; set; }

        public string location { get; set; }

        public string accountNo { get; set; }

        public string accountName { get; set; }

        public string docFile { get; set; }

        public int PPOrderID { get; set; }

        public double paymentAmt { get; set; }
    }
}
