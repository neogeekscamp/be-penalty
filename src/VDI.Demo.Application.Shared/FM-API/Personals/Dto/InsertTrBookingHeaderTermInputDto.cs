﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class InsertTrBookingHeaderTermInputDto
    {
        public int unitID { get; set; }

        public long userID { get; set; }
    }
}
