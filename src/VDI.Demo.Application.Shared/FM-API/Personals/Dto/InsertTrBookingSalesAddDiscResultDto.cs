﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class InsertTrBookingSalesAddDiscResultDto
    {
        public int bookingHeaderID { get; set; }

        public double pctTax { get; set; }

        public int itemID { get; set; }

        public double pctDisc { get; set; }
    }
}
