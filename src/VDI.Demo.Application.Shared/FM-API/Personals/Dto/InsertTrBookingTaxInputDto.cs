﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class InsertTrBookingTaxInputDto
    {
        public int bookingDetailID { get; set; }

        public int unitID { get; set; }

        public int termID { get; set; }

        public decimal sellingPrice { get; set; }

        public decimal netNetPrice { get; set; }

        public long userID { get; set; }
    }
}
