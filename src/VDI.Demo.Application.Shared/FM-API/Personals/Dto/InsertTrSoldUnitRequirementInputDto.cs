﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class InsertTrSoldUnitRequirementInputDto
    {
        public string scmCode { get; set; }

        public long userID { get; set; }

        public int unitID { get; set; }

        public string memberCode { get; set; }

        public string bookNo { get; set; }

        public string entityCode { get; set; }

        public string devCode { get; set; }

        public List<bookingDetailIDDto> listBookingDetail { get; set; }
    }
}
