﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class InsertTrSoldUnitResultDto
    {
        public string entityCode { get; set; }

        public string devCode { get; set; }

        public string bookNo { get; set; }

        public string scmCode { get; set; }
    }
}
