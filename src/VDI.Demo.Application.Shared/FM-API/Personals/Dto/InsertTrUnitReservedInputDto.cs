﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class InsertTRUnitReservedInputDto
    {
        public int unitID { get; set; }
        public int userID { get; set; }
        public string psCode { get; set; }

        public int renovID { get; set; }

        public decimal sellingPrice { get; set; }

        public int projectID { get; set; }
        public int termID { get; set; }

        public int? bankID1 { get; set; }
        public int? bankID2 { get; set; }

        public string ppNo { get; set; }
    }
}
