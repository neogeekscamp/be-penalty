﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class KonfirmasiPesananDto
    {
        public string orderCode { get; set; }
        public string psCode { get; set; }
        public string tglBooking { get; set; }
        public DateTime bookDate { get; set; }
        //public string tglJatuhTempo { get; set; }
        public string projectImage { get; set; }
        public string psName { get; set; }
        public string psNoHp { get; set; }
        public string psEmail { get; set; }
        public string psAddress { get; set; }
        public string psJob { get; set; }
        public string membercode { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string unitName { get; set; }
        public string luas { get; set; }
        public string tipe { get; set; }
        public string cluster { get; set; }
        public string categoryCode { get; set; }
        public string tglSerahTerima { get; set; }
        public string netPrice { get; set; }
        public string termDiscAmt { get; set; }
        //public string termDisc { get; set; }
        public string netNetPrice { get; set; }
        public string ppn { get; set; }
        public decimal sellingPrices { get; set; }
        public string termNo { get; set; }
        public string termName { get; set; }
        public string bookingFee { get; set; }
        public List<listDownPayment> listDP { get; set; }
        public List<listCicilanDto> listCicilan { get; set; }
        //public List<listBankDto> listBank { get; set; }
        public string bankName { get; set; }
        public string noVA { get; set; }
        public string companyName { get; set; }
        public string companyAddress { get; set; }
        public string companyPhone { get; set; }
        public string companyWebAddress { get; set; }
        public string companyEmail { get; set; }
        public string subKomplek { get; set; }
        public string komplekAddress { get; set; }
        public double luasKavling { get; set; }
        public double luasBangunan { get; set; }
        public string parameterCode { get; set; }
        public string headerTemplate { get; set; }
        public string footerTemplate { get; set; }
        public string bodyTemplate { get; set; }
        public string totalDP { get; set; }
        public string totalDPAmt { get; set; }
        public string totalDPVat { get; set; }
        public decimal bfAmount { get; set; }
        public decimal bfAmt { get; set; }
        public decimal bfVat { get; set; }
        public List<string> listPathTnc { get; set; }
        public string kadasterPath { get; set; }
        public double dpPersen { get; set; }
        public string sellingPrice { get; set; }
        public decimal vat { get; set; }
        public decimal netNetPrices { get; set; }
        public string clusterField { get; set; }
        public string roadField { get; set; }
        public string kavNoField { get; set; }
        public string areaField { get; set; }
        public string diagramaticType { get; set; }
        public string landArea { get; set; }
        public string renovation { get; set; }
        public string priceLandBuild { get; set; }
        public string priceRenov { get; set; }
    }
    public class listBankDto
    {
        public string bankName { get; set; }
        public string noVA { get; set; }
    }
    public class listDownPayment
    {
        public string DPAmount { get; set; }
        public string DPDueDate { get; set; }
        public decimal totalDP { get; set; }
        public DateTime akhirDP { get; set; }
        public decimal dpAmt { get; set; }
        public decimal dpVat { get; set; }
    }
    public class ListunitDto
    {
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string luas { get; set; }
        public string tipe { get; set; }
        public string cluster { get; set; }
        public string category { get; set; }
    }

    public class listCicilanDto
    {
        public int cicilanNo { get; set; }
        public decimal cicilanAmt { get; set; }
        public string cicilanDueDate { get; set; }
        public decimal pelunasanAmt { get; set; }
        public DateTime tglLunas { get; set; }
        public decimal insAmt { get; set; }
        public decimal insVat { get; set; }
    }
}
