﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class SendEmailInputDto
    {
        public string toAddress { get; set; }

        public string subject { get; set; }

        public string ccAddress { get; set; }

        public string body { get; set; }

        public string pathKP { get; set; }
    }
}
