﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class Step19InsertSlikCheckingInputDto
    {
        public string bookCode { get; set; }

        public int? bankID1 { get; set; }

        public int? bankID2 { get; set; }

        public string psCode { get; set; }

        public long userID { get; set; }
    }
}
