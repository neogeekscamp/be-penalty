﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class TrBookingDetailInputDto
    {
        public int unitID { get; set; }

        public int termID { get; set; }

        public int bookingHeaderID { get; set; }

        public int renovID { get; set; }

        public long userID { get; set; }
    }
}
