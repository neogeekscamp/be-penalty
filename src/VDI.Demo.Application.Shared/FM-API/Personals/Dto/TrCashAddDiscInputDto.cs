﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class TrCashAddDiscInputDto
    {
        public int unitID { get; set; }

        public int termID { get; set; }

        public int bookingDetailID { get; set; }

        public long userID { get; set; }
    }
}
