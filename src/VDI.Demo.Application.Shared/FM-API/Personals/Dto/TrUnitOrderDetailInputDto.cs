﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class TrUnitOrderDetailInputDto
    {
        public int orderHeaderID { get; set; }

        public long userID { get; set; }
        public string otherPaymentType { get; set; }
        public string paymentTypeName { get; set; }

        public List<UnitUniversalResultDto> arrUnit { get; set; }
    }
    public class ListUnitOrderDetail
    {
        public int projectID { get; set; }
        public int unitID { get; set; }
        public decimal sellingPrice { get; set; }
        public decimal bfAmount { get; set; }

    }
}
