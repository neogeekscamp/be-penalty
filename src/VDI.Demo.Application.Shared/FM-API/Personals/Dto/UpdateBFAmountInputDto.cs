﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class UpdateBFAmountInputDto
    {
        public int unitID { get; set; }

        public int renovID { get; set; }

        public int termID { get; set; }
    }
}
