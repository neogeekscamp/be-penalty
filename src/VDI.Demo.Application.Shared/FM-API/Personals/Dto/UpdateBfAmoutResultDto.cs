﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class UpdateBfAmoutResultDto
    {
        public decimal bfAmount { get; set; }

        public bool result { get; set; }
    }
}
