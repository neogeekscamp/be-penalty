﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class UpdateNetPriceResultDto
    {
        public bool result { get; set; }

        public decimal netNetPrice { get; set; }

        public int bookingDetailID { get; set; }

        public string message { get; set; }

        public decimal netPriceComm { get; set; }

        public int renovID { get; set; }
    }
}
