﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class UpdateOrderStatusFullyPaid
    {
        public int orderHeaderID { get; set; }

        public int bookingHeaderID { get; set; }

        public int unitID { get; set; }

        public long userID { get; set; }
    }
}
