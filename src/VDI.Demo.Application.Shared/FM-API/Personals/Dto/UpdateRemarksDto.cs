﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class UpdateRemarksDto
    {
        public int bookingHeaderID { get; set; }

        public decimal sellingPrice { get; set; }

        public string bookCode { get; set; }
    }
}
