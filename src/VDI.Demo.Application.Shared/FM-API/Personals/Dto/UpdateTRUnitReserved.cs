﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class UpdateTRUnitReserved
    {
        public string psCode { get; set; }
        public int userID { get; set; }

        public int bankID1 { get; set; }
        public int bankID2 { get; set; }

        public List<UnitTrUnitReservedDto> unit { get; set; }
    }

    public class UnitTrUnitReservedDto
    {
        public int unitID { get; set; }
    }
}
