﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class UploadDocumentInputDto
    {
        public string psCode { get; set; }
        public string documentType { get; set; }
        public string documentFile { get; set; }
        public string documentPicType { get; set; }
        public string remarkCustomer { get; set; }
    }
}
