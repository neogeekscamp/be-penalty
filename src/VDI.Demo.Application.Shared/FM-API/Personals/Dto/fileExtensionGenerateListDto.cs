﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Personals.Dto
{
    public class fileExtensionGenerateListDto
    {
        public string phisicalPath { get; set; }
        public int orderNumber { get; set; }
    }
}
