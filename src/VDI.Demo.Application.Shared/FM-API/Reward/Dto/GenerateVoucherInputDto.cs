﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GenerateVoucherInputDto
    {
        public DateTime inputTime { get; set; }
        public string voucherDate { get; set; }
        public string voucherCode { get; set; }
        public string rewardName { get; set; }
        public DateTime? expireOn { get; set; }
    }
}
