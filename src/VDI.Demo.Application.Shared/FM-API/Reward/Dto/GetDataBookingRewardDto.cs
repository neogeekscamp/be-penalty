﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetDataBookingRewardDto
    {
        public int Id { get; set; } 

        public int? rewardId { get; set; }

        public string voucherCode { get; set; }

        public string status { get; set; }

    }
}
