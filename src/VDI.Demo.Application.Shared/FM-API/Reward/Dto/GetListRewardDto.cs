﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetListRewardDto
    {
        public int rewardId { get; set; }
        public int? projectId { get; set; }
        public string projectName { get; set; }
        public string rewardName { get; set; }
        public bool? isBookingFee { get; set; }
        public bool? isBilling { get; set; }
        public bool? isParking { get; set; }
        public bool? isActive { get; set; }
    }
}
