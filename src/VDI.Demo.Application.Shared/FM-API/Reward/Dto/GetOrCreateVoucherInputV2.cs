﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetOrCreateVoucherInputV2
    {
        [Range(1, AppConsts.MaxPageSize)]
        public int MaxResultCount { get; set; }

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }
        public string receiptCode { get; set; }
        public string keywords { get; set; }
        public int projectId { get; set; }

        public GetOrCreateVoucherInputV2()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }
    }
}
