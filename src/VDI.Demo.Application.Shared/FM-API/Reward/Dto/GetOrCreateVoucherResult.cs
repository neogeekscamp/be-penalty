﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetOrCreateVoucherResult
    {
        public string VoucherCode { get; set; }
        public bool IsUsingExistingVoucher { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public decimal VoucherAmt { get; set; }
        public string voucherPdf { get; set; }
    }
}
