﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetOrCreateVoucherV2Input
    {
        public string ReceiptCode { get; set; }
        public DateTime ReceiptDate { get; set; }
        public decimal ReceiptAmt { get; set; }
        public int IdLocation { get; set; }
        public int ProjectID { get; set; }
        public string VoucherTypeCode { get; set; }
        public List<int> rewardID { get; set; }
        public string attachment { get; set; }
        public DateTime expiredDate { get; set; }
    }
}
