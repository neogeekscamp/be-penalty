﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetRewardMyMInput
    {
        public int MaxResultCount { get; set; }

        public int SkipCount { get; set; }

        public string psCode { get; set; }

        public bool isBilling { get; set; }

        public bool isParking { get; set; }

        public string status { get; set; }

        public bool? expired { get; set; }
    }
}
