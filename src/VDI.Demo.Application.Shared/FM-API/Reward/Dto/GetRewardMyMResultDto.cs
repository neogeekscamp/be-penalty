﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetRewardMyMResultDto
    {
        public string unitCode { get; set; }

        public string unitNo { get; set; }

        public string psCode { get; set; }

        public string rewardName { get; set; }

        public int rewardID { get; set; }

        public string voucherCode { get; set; }

        public bool? isBilling { get; set; }

        public bool? isParking { get; set; }

        public int bookingRewardID { get; set; }

        public string status { get; set; }

        public DateTime expiredDate { get; set; }
    }
}
