﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetUnitOrderHeaderIDResultDto
    {
        public int unitOrderHeaderID { get; set; }
        public int bookingRewardID { get; set; }
        public string unitNo { get; set; }
        public string unitCode { get; set; }
        public string voucherCode { get; set; }
    }
}
