﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetVoucherAndRewardResultDto
    {
        public string voucherCode { get; set; }
        public string ReceiptCode { get; set; }
        public string rewardName { get; set; }
        public string rewardID { get; set; }
        public DateTime createdDate { get; set; }
        public bool isBilling { get; set; }
        public bool isParking { get; set; }
        public bool isExpired { get; set; }
        public bool isUsed { get; set; }
    }
}
