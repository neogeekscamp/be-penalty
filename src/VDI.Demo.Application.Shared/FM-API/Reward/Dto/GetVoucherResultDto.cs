﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class GetVoucherResultDto
    {
        public string ReceiptCode { get; set; }
        public DateTime ReceiptDate { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime? expiredDate { get; set; }
        public decimal ReceiptAmt { get; set; }
        public int IdLocation { get; set; }
        public string LocationName { get; set; }
        public int projectID { get; set; }
        public string projectName { get; set; }
        public string rewardName { get; set; }
        public string rewardID { get; set; }
        public string voucherCode { get; set; }
        public string attachment { get; set; }
        public string VoucherTypeCode { get; set; }
        public DateTime inputTime { get; set; }
        public bool isExpired { get; set; }
        public bool isUsed { get; set; }
    }
}
