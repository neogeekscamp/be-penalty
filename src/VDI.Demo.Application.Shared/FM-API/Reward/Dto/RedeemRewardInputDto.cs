﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class RedeemRewardInputDto
    {
        public int bookingRewardID { get; set; }
        public int projectID { get; set; }
        public int unitOrderHeaderID { get; set; }
        public string voucherCode { get; set; }
        public int flag { get; set; }// kebutuhan status reward 1 = "Unredeemed", 2 = "Redeemed"
        public int? typeVoucher { get; set; } // 1 : billing, 2 : parking
    }

    public class NewRedeemRewardInputDto
    {
        public int projectID { get; set; }
        public string voucherCode { get; set; }
        public int flag { get; set; }// kebutuhan status reward 1 = "Unredeemed", 2 = "Redeemed"
    }

    public class VoucherListDto
    {
        public string voucherEventTypeCode { get; set; }
        public int rewardID { get; set; }
        public string voucherTypeCode { get; set; }
        public DateTime expirationDate { get; set; }
    }
}
