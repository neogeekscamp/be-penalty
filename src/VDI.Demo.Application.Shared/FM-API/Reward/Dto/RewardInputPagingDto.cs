﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class RewardInputPagingDto
    {
        [Range(1, AppConsts.MaxPageSize)]
        public int MaxResultCount { get; set; }

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }

        public string keywords { get; set; }

        public int projectID { get; set; }

        public string voucherCode { get; set; }

        public RewardInputPagingDto()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }
    }
}
