﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.Reward.Dto
{
    public class UpdateRewardInput
    {
        public int projectID { get; set; }
        public int idBookingReward { get; set; }
        public string voucherCode { get; set; }
    }
}
