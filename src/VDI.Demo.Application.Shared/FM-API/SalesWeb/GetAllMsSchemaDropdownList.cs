﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class GetAllMsSchemaDropdownList
    {
        public string scmCode { get; set; }
        public string scmName { get; set; }
        public bool isAutomaticMemberStatus { get; set; }
    }
}
