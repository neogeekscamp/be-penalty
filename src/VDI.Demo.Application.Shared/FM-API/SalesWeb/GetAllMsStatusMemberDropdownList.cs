﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class GetAllMsStatusMemberDropdownList
    {
        public string statusCode { get; set; }
        public string statusName { get; set; }
    }
}
