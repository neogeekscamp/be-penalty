﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class GetAllPersonalMemberDto
    {
        public string memberCode { get; set; }
        public string name { get; set; }
    }
}
