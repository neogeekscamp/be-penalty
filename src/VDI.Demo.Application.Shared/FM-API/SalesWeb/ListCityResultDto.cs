﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class ListCityResultDto
    {
        public string cityCode { get; set; }
        public string cityName { get; set; }
    }
}
