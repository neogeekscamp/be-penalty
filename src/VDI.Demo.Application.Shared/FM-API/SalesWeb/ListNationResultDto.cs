﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class ListNationResultDto
    {
        public string nationID { get; set; }

        public string nationality { get; set; }
    }
}
