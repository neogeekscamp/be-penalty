﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class ListProvinceResultDto
    {
        public string provinceCode { get; set; }

        public string provinceName { get; set; }
    }
}
