﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class SendEmailPersInputDto
    {
        public string counterCode { get; set; }
        public string emailTo { get; set; }
        public string emailCC { get; set; }
        public string emailBCC { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
        public string nik { get; set; }
        public string emailFrom { get; set; }
        public string emailFromDisplayName { get; set; }
        public string emailID { get; set; }
    }
}
