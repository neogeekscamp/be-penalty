﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class SignUpCustomerResultDto
    {
        public string psCode { get; set; }

        public string message { get; set; }
    }
}
