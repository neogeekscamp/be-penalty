﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SalesWeb
{
    public class getListMaritalStatusResultDto
    {
        public string marStatus { get; set; }

        public string marStatusName { get; set; }
    }
}
