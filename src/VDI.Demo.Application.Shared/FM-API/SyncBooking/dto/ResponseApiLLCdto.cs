﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.SyncBooking.dto
{
    public class ResponseApiLLCdto
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
