﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.UserPP.dto
{
    public class ConnStringUserInputDto
    {
        public string IpServer { get; set; }
        public string credentialUser { get; set; }
        public string credentialPass { get; set; }
        public string databaseName { get; set; }
        public string tableName { get; set; }
        public string linkServer { get; set; }
        public string scmCode { get; set; }
        public string query { get; set; }
    }
}
