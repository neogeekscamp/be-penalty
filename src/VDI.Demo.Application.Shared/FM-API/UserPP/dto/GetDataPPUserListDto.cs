﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.UserPP.dto
{
    public class GetDataPPUserListDto
    {
        public string userName { get; set; }
        public string pass { get; set; }
        public string psCode { get; set; }
    }
}
