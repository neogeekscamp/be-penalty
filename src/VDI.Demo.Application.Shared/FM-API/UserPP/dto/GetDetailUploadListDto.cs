﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.UserPP.dto
{
    public class GetDetailUploadListDto
    {
        public string userName { get; set; }
        public string message { get; set; }
    }
}
