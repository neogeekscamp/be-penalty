﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.UserPP.dto
{
    public class InsertUserPPResultDto
    {
        public int successCount { get; set; }
        public int exceededLengthCount { get; set; }
        public int alreadyExistCount { get; set; }
        public int memberNotExistCount { get; set; }
        public int inactiveCount { get; set; }
        public int otherErrorCount { get; set; }
    }
}
