﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.FM_API.UserPP.dto
{
    public class RegisterUserPPInput
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }
}
