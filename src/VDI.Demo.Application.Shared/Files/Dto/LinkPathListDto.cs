﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Files.Dto
{
    public class LinkPathListDto
    {
        public string filename { get; set; }
        public string linkFile { get; set; }
        public string linkServerFile { get; set; }
        public string filePhysicalPath { get; set; }
    }
}
