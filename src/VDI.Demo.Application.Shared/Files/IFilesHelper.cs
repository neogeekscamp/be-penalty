﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Files
{
    public interface IFilesHelper : IApplicationService
    {
        string MoveFiles(string filename, string oldPath, string newPath);
        string getAbsoluteUri();
    }
}
