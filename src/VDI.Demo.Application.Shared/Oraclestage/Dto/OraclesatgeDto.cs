﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Oraclestage.Dto
{
    public class OraclesatgeDto
    {
        
        public string STATUS { get; set; }

        public int? SET_OF_BOOKS_ID { get; set; }

        
        public string ACCOUNTING_DATE { get; set; }

        
        public string CURRENCY_CODE { get; set; }

        
        public string DATE_CREATED { get; set; }

        public int? CREATED_BY { get; set; }

        
        public string ACTUAL_FLAG { get; set; }

        
        public string USER_JE_CATEGORY_NAME { get; set; }

        
        public string USER_JE_SOURCE_NAME { get; set; }

        
        public string SEGMENT1 { get; set; }

        
        public string SEGMENT2 { get; set; }

        
        public string SEGMENT3 { get; set; }

        
        public string SEGMENT4 { get; set; }

        
        public string SEGMENT5 { get; set; }

        
        public string SEGMENT6 { get; set; }

        
        public string SEGMENT7 { get; set; }

        
        public decimal? ENTERED_DR { get; set; }

        
        public decimal? ENTERED_CR { get; set; }

        
        public string TRANSACTION_DATE { get; set; }

        
        public string GROUP_ID { get; set; }

        
        public string LK_ATTRIBUTE1 { get; set; }

        
        public string LK_ATTRIBUTE2 { get; set; }
        
        public string LK_ATTRIBUTE3 { get; set; }        
        public string LK_ATTRIBUTE4 { get; set; }        
        public string LK_ATTRIBUTE5 { get; set; }        
        public string LK_ATTRIBUTE6 { get; set; }        
        public string ACC_CODE { get; set; }
        public DateTime? INPUT_TIME { get; set; }
        public string INPUT_UN { get; set; }
        public DateTime? MODIF_TIME { get; set; }
        public string MODIF_UN { get; set; }
    }
}
