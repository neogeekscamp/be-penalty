﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.Oraclestage.Dto;

namespace VDI.Demo.Oraclestage
{
    public interface IOraclestageAppService : IApplicationService
    {
        Task<int> GenerateGroupID();
        Task SaveOraclestage(OraclesatgeDto input);
    }
}
