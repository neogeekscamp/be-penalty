﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Penalty.Dto
{
    public class DataPenaltyListDto
    {
        public List<DataPenaltyDto> PenaltyList { get; set; }
    }

    public class SummaryPenaltyDto
    {
        public int aging { get; set; }
        public int bookingHeaderID { get; set; }
        public int bookingDetailID { get; set; }
        public int schedNo { get; set; }
        public string allocCode { get; set; }
        public int entityID { get; set; }
        public DateTime dueDate { get; set; }
        public decimal totalAmountSchedule { get; set; }
        public decimal totalAmount { get; set; }
        public decimal? totalPenalty { get; set; }
        public decimal penaltyRate { get; set; }
        public string penaltyFreq { get; set; }
        public int penaltyBaseRate { get; set; }
        public bool isAdj { get; set; }
    }

    public class DataPenaltyDto
    {
        public string transNo { get; set; }
        public int PaymentDetailID { get; set; }
        public int entityID { get; set; }
        public string penaltyFreq { get; set; }
        public decimal penaltyRate { get; set; }
        public int penaltyBaseRate { get; set; }
        public int bookingHeaderID { get; set; }
        public int bookingDetailID { get; set; }
        public DateTime creationTime { get; set; }
        public DateTime dueDate { get; set; }
        public DateTime clearDate { get; set; }
        public int aging { get; set; }
        public int schedNo { get; set; }
        public string allocCode { get; set; }
        public decimal netAmt { get; set; }
        public decimal amountNet { get; set; }
        public decimal netOut { get; set; }

        public decimal vatAmt { get; set; }
        public decimal amountVat { get; set; }
        public decimal vatOut { get; set; }

        public decimal totalAmountSchedule { get; set; }
        public decimal totalAmount { get; set; }
        //public decimal totalOut { get; set; }
        public decimal totalPenaltyNet { get; set; }
        public decimal totalPenaltyVat { get; set; }
        public decimal totalPenalty { get; set; }
        public bool isAdj { get; set; }
        //public bool isPenaltyAfterAdj { get; set; }

    }
}
