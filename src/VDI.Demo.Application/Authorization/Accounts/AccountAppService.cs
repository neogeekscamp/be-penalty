﻿using System;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Zero.Configuration;
using Microsoft.AspNetCore.Identity;
using VDI.Demo.Authorization.Accounts.Dto;
using VDI.Demo.Authorization.Impersonation;
using VDI.Demo.Authorization.Users;
using VDI.Demo.Configuration;
using VDI.Demo.Debugging;
using VDI.Demo.MultiTenancy;
using VDI.Demo.Security.Recaptcha;
using VDI.Demo.Url;
using Abp.Domain.Repositories;
using VDI.Demo.PersonalsDB;
using System.Linq;
using VDI.Demo.EntityFrameworkCore;
using System.Security.Cryptography;
using System.IO;
using System.Collections.Generic;
using VDI.Demo.FM_API.UserPP.dto;

namespace VDI.Demo.Authorization.Accounts
{
    public class AccountAppService : DemoAppServiceBase, IAccountAppService
    {
        public IAppUrlService AppUrlService { get; set; }

        public IRecaptchaValidator RecaptchaValidator { get; set; }

        private readonly IUserEmailer _userEmailer;
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly IImpersonationManager _impersonationManager;
        private readonly IUserLinkManager _userLinkManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IWebUrlService _webUrlService;
        private readonly IRepository<PERSONALS, string> _personalsRepo;
        private readonly IRepository<PERSONALS_MEMBER, string> _personalsMemberRepo;
        private readonly IRepository<TR_Email, string> _trEmailRepo;
        private readonly PersonalsNewDbContext _personalsContext;
        private readonly DemoDbContext _contextDemo;

        public AccountAppService(
            IUserEmailer userEmailer,
            UserRegistrationManager userRegistrationManager,
            IImpersonationManager impersonationManager,
            IUserLinkManager userLinkManager,
            IPasswordHasher<User> passwordHasher,
            IWebUrlService webUrlService,
            IRepository<PERSONALS, string> personalsRepo,
            IRepository<PERSONALS_MEMBER, string> personalsMemberRepo,
            IRepository<TR_Email, string> trEmailRepo,
            PersonalsNewDbContext personalsContext,
            DemoDbContext contextDemo
            )
        {
            _personalsContext = personalsContext;
            _userEmailer = userEmailer;
            _userRegistrationManager = userRegistrationManager;
            _impersonationManager = impersonationManager;
            _userLinkManager = userLinkManager;
            _passwordHasher = passwordHasher;
            _webUrlService = webUrlService;
            _personalsRepo = personalsRepo;
            _personalsMemberRepo = personalsMemberRepo;
            _trEmailRepo = trEmailRepo;
            _contextDemo = contextDemo;

            AppUrlService = NullAppUrlService.Instance;
            RecaptchaValidator = NullRecaptchaValidator.Instance;
        }

        public async Task SendPasswordResetCode(SendPasswordResetCodeInput input)
        {
            var user = (from a in UserManager.Users
                        where a.EmailAddress == input.EmailAddress && a.UserName == input.UserName
                        select a).FirstOrDefault();
            if (user == null)
            {
                throw new UserFriendlyException("User not exist");
            }
            user.SetNewPasswordResetCode();
            await _userEmailer.SendPasswordResetLinkAsync(
                user,
                AppUrlService.CreatePasswordResetUrlFormat(AbpSession.TenantId)
                );
        }

        public async Task<ResetPasswordOutput> ResetPassword(ResetPasswordInput input)
        {

            var user = new User();

            var userID = (from x in _contextDemo.Users
                            where x.UserName == input.Username && x.TenantId == 1
                            select x.Id).FirstOrDefault();

            user = await UserManager.GetUserByIdAsync(userID);

            if (user == null)
            {
                throw new UserFriendlyException(L("InvalidPasswordResetCode"), L("InvalidPasswordResetCode_Detail"));
            }

            user.Password = _passwordHasher.HashPassword(user, input.Password);
            user.PasswordResetCode = null;
            user.IsEmailConfirmed = true;
            user.ShouldChangePasswordOnNextLogin = false;

            try
            {
                await UserManager.UpdateAsync(user);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Email"))
                {
                    var listUserID = new List<long>();

                    var getUser = (from a in _contextDemo.Users
                                   where a.UserName == input.Username && a.TenantId == 1
                                   select new
                                   {
                                       a.EmailAddress,
                                       a.Id
                                   }).FirstOrDefault();

                    var getOtherEmail = (from a in _contextDemo.Users
                                         where a.EmailAddress.ToLowerInvariant() == getUser.EmailAddress.ToLowerInvariant()
                                         && a.UserName != input.Username
                                         select a.Id).FirstOrDefault();

                    listUserID.Add((int)getUser.Id);
                    listUserID.Add((int)getOtherEmail);

                    var checkPsCode = (from a in _contextDemo.MP_UserPersonals
                                       where listUserID.Contains(a.userID)
                                       select a.psCode).Distinct().ToList();

                    if (checkPsCode.Count() > 1)
                    {
                        throw new UserFriendlyException("Email " + getUser.EmailAddress + " is already taken.");
                    }

                    _contextDemo.Users.Update(user);
                    _contextDemo.SaveChanges();
                }
                else
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            return new ResetPasswordOutput
            {
                CanLogin = user.IsActive,
                UserName = user.UserName
            };
        }
        
        private async Task<Tenant> GetActiveTenantAsync(int tenantId)
        {
            var tenant = await TenantManager.FindByIdAsync(tenantId);
            if (tenant == null)
            {
                throw new UserFriendlyException(L("UnknownTenantId{0}", tenantId));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIdIsNotActive{0}", tenantId));
            }

            return tenant;
        }

        private async Task<string> GetTenancyNameOrNullAsync(int? tenantId)
        {
            return tenantId.HasValue ? (await GetActiveTenantAsync(tenantId.Value)).TenancyName : null;
        }

        private async Task<User> GetUserByChecking(string inputEmailAddress)
        {
            var user = await UserManager.FindByEmailAsync(inputEmailAddress);
            if (user == null)
            {
                throw new UserFriendlyException(L("InvalidEmailAddress"));
            }

            return user;
        }

        public async Task<RegisterOutput> RegisterUserPP(RegisterUserPPInput input)
        {
            var user = await _userRegistrationManager.RegisterUserPPAsync(
                input.name,
                input.name,
                input.email,
                input.userName,
                input.password,
                true,
                AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId)
            );

            //var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);

            return new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed)
            };
        }

        public async Task<RegisterOutput> RegisterMember(RegisterMemberInput input)
        {
            var getPersonals = (from x in _personalsRepo.GetAll()
                                join y in _personalsMemberRepo.GetAll()
                                on x.psCode equals y.psCode
                                join z in _trEmailRepo.GetAll()
                                on x.psCode equals z.psCode
                                where y.memberCode == input.membercode
                                select new { x.name, z.email, y.memberCode }).FirstOrDefault();

            var user = await _userRegistrationManager.RegisterMemberAsync(
                getPersonals.name,
                getPersonals.name,
                getPersonals.email,
                input.membercode,
                input.password,
                true,
                AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId)
            );

            //var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);

            return new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed)
            };
        }
        public async Task<RegisterOutput> RegisterCustomer(RegisterCustomerInput input)
        {

            var user = await _userRegistrationManager.RegisterMemberAsync(
                input.name,
                input.name,
                input.email,
                input.email,
                input.password,
                true,
                AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId)
            );

            return new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed)
            };
        }

        private string Decrypt(string EncryptedText)
        {
            Logger.InfoFormat("Decrypt() - Start");

            Logger.DebugFormat("Decrypt(). Parameters sent:{0}" +
                    "value = {1}{0}"
                    , Environment.NewLine
                    , EncryptedText);

            byte[] inputByteArray = new byte[EncryptedText.Length + 1];
            byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
            byte[] key = { };

            try
            {
                key = System.Text.Encoding.UTF8.GetBytes("A0D1nX0Q");
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(EncryptedText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, rgbIV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;

                Logger.DebugFormat("Result Decrypt(). Parameters sent:{0}" +
                                   "value = {1}{0}"
                                    , Environment.NewLine
                                    , encoding.GetString(ms.ToArray()));

                Logger.InfoFormat("Decrypt() - End");

                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                Logger.InfoFormat("Decrypt() - End");

                return e.Message;
            }
        }
    }
}
