﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.Configuration;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Penalty.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.SqlExecuter;

namespace VDI.Demo.Penalty
{
    public class PSASPenaltyAppService : DemoAppServiceBase, IPSASPenaltyAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly ISqlExecuter _sqlExecuter;
        private readonly DemoDbContext _contextEngine3;
        private readonly IConfigurationRoot _appConfiguration;

        public PSASPenaltyAppService(PropertySystemDbContext propertySystemDbContext, ISqlExecuter sqlExecuter, DemoDbContext contextEngine3, IAppConfigurationAccessor configurationAccessor)
        {
            _contextPropertySystem = propertySystemDbContext;
            _sqlExecuter = sqlExecuter;
            _contextEngine3 = contextEngine3;
            _appConfiguration = configurationAccessor.Configuration;
        }

        private DataTable ColumnsLOG(DataTable dt)
        {
            dt.Columns.Add("createdDate", typeof(DateTime));
            dt.Columns.Add("bookCode", typeof(string));
            dt.Columns.Add("message_ex", typeof(string));

            return dt;
        }

        private DataTable ColumnsPenaltySchedule(DataTable dt)
        {
            dt.Columns.Add("CreationTime", typeof(DateTime));
            dt.Columns.Add("CreatorUserId", typeof(long));
            dt.Columns.Add("ScheduleAllocCode", typeof(string));
            dt.Columns.Add("ScheduleNetAmount", typeof(decimal));
            dt.Columns.Add("ScheduleTerm", typeof(int));
            dt.Columns.Add("SeqNo", typeof(int));
            dt.Columns.Add("bookingHeaderID", typeof(int));
            dt.Columns.Add("entityID", typeof(int));
            dt.Columns.Add("penaltyAging", typeof(int));
            dt.Columns.Add("penaltyAmount", typeof(decimal));
            dt.Columns.Add("scheduleStatus", typeof(string));
            dt.Columns.Add("penaltyStatus", typeof(string));


            return dt;
        }

        private async Task<int> GetUSer()
        {
            return await Task.Factory.StartNew(() =>
            {
                int? userID;

                var getUser = (from x in _contextEngine3.Users
                               where x.Id == AbpSession.UserId
                               select x);

                if (getUser.Any())
                {
                    userID = (int?)getUser.FirstOrDefault().Id;
                }
                else
                {
                    userID = 2;
                }

                return userID.Value;
            });         
        }

        public async Task<CreatePenaltyResultDto> CreatePenalty(int ProjectID)
        {
            DataTable dtLOG = new DataTable();
            ColumnsLOG(dtLOG);
            CreatePenaltyResultDto result = new CreatePenaltyResultDto();
            List<ListBookcodeFailDto> listRow = new List<ListBookcodeFailDto>();

            int succes = 0;
            int fail = 0;
            try
            {
                var listBookCode = _contextPropertySystem.Temp_Procces_Penalty.Where(ss => ss.ProjectID == ProjectID).ToList();

                int limit = listBookCode.Count / 5;
                int afterLimit = limit == 0 ? 1 : limit;
                int partition = listBookCode.Count % afterLimit == 0 ? listBookCode.Count / afterLimit : (listBookCode.Count / afterLimit) + 1;

                for (int i = 0; i < partition; i++)
                {
                    var takeData = listBookCode.Skip(afterLimit * i).Take(afterLimit);
                    foreach (var data in takeData)
                    {
                        ListBookcodeFailDto temp = new ListBookcodeFailDto();
                        try
                        {
                            DataPenaltyListDto Model = new DataPenaltyListDto();
                            var userID = await GetUSer();
                            var SP_GetDataPenalty = "exec SP_GetDataPenalty @bookCode";

                            string bookCode = data.Bookcode;
                            Model.PenaltyList = _sqlExecuter.GetFromPropertySystem<DataPenaltyDto>
                                                       (SP_GetDataPenalty, new { bookCode },
                                                       System.Data.CommandType.StoredProcedure).ToList();

                            CreatePenaltyPerBookcodeResultDto resultPerBckCode = await DoThisCreatePenalty(Model, userID);
                            if(resultPerBckCode.FailMessage != "")
                            {
                                dtLOG.Rows.Add(DateTime.Now, data.Bookcode, resultPerBckCode.FailMessage);
                            }

                            await ProsesAdj(Model, userID);
                            succes += 1;
                        }
                        catch (Exception e)
                        {
                            temp.Bookcode = data.Bookcode;
                            temp.FailMessage = e.Message;
                            listRow.Add(temp);

                            dtLOG.Rows.Add(DateTime.Now, data.Bookcode, e.Message);
                            fail += 1;
                            continue;
                            throw;
                        }
                    }
                }

                result.Succes = succes;
                result.Fail = fail;
                result.ListBookcodeFail = listRow;
            }
            catch (Exception e)
            {

                throw;
            }
       

            return result;
        }

        private async Task ProsesAdj(DataPenaltyListDto Model, int userID)
        {            
            var adj = Model.PenaltyList.Where(ss => ss.isAdj == false && ss.totalAmount < 0).OrderBy(ss => ss.clearDate).ToList();

            for (int i = 0; i < adj.Count; i++)
            {
                var endPay = Model.PenaltyList.Where(ss => ss.totalAmount > 0 && ss.isAdj == false && ss.clearDate < adj[i].clearDate && ss.schedNo == adj[i].schedNo).OrderByDescending(ss => ss.clearDate).ThenByDescending(ss => ss.creationTime).FirstOrDefault();

                endPay.totalAmount = 0;
                endPay.totalPenalty = 0;
                endPay.isAdj = true;
                //endPay.isPenaltyAfterAdj = true;
            }

            await DoThisCreatePenalty(Model, userID);
        }

        private async Task<CreatePenaltyPerBookcodeResultDto> DoThisCreatePenalty(DataPenaltyListDto Model, int userID)
        {
            CreatePenaltyPerBookcodeResultDto result = new CreatePenaltyPerBookcodeResultDto();
            result.FailMessage = "";
            DataTable dt = new DataTable();
            ColumnsPenaltySchedule(dt);

            decimal amountAfter = 0m;
            double agingAfter = 0;
            decimal totalPenaltyAfter = 0;
            double power = 0;
            decimal Net = decimal.Parse(1.1.ToString());
            decimal Vat = decimal.Parse(0.1.ToString());
            //int scheduleCekSameDay = 0;            

            var grouping = (from a in Model.PenaltyList.Where(ss => ss.totalAmount >= 0)
                            group a by new { a.bookingHeaderID, a.bookingDetailID, a.schedNo, a.dueDate } into g
                            select new SummaryPenaltyDto
                            {
                                schedNo = g.Key.schedNo,
                                dueDate = g.Key.dueDate,
                                bookingHeaderID = g.Key.bookingHeaderID,
                                bookingDetailID = g.Key.bookingDetailID,
                                totalAmountSchedule = g.Max(xx => xx.totalAmountSchedule),
                                totalAmount = g.Sum(xx => xx.totalAmount),
                                totalPenalty = g.Sum(xx => xx.totalPenalty),
                                aging = g.Max(xx => xx.aging),
                                penaltyRate = g.Max(xx => xx.penaltyRate),
                                penaltyFreq = g.Max(xx => xx.penaltyFreq),
                                penaltyBaseRate = g.Max(xx => xx.penaltyBaseRate),
                                allocCode = g.Max(xx => xx.allocCode),
                                entityID = g.Max(xx => xx.entityID),
                                isAdj = g.Max(xx => xx.isAdj)
                            }).ToList();

            for (int i = 0; i < grouping.Count; i++)
            {
                amountAfter = Math.Round(grouping[i].totalAmountSchedule - grouping[i].totalAmount, 4);

                try
                {
                    if (amountAfter > 0)
                    {

                        agingAfter = (DateTime.Now.Date - grouping[i].dueDate.Date).TotalDays;
                        power = Math.Pow(Convert.ToDouble(1 + (grouping[0].penaltyRate / grouping[0].penaltyBaseRate)), agingAfter);
                        decimal agingConvert = Math.Round(Convert.ToDecimal(agingAfter), 0);
                        decimal powerConvert = Convert.ToDecimal(power);

                        //hitung lagi penalty jika masih ada outstanding
                        totalPenaltyAfter = grouping[0].penaltyFreq.ToUpper() == "MONTHLY" ?
                                            (amountAfter * agingConvert) * ((grouping[0].penaltyRate / grouping[0].penaltyBaseRate) / 30) :
                                            (amountAfter) * (powerConvert - 1);

                        if (agingAfter > 0)
                        {
                            dt.Rows.Add(
                                                     DateTime.Now,                        //creationTime
                                                     userID,                              //creatorUserID
                                                     grouping[i].allocCode,             //scheduleAllocCode
                                                     grouping[i].totalAmountSchedule,           //scheduleNetAmount
                                                     grouping[i].schedNo,               //scheduleTerm
                                                     grouping[i].schedNo,               //seqNo
                                                     grouping[i].bookingHeaderID,       //bookingHeaderID
                                                     1,              //entityID
                                                     Convert.ToInt32(agingConvert),                       //penaltyAging
                                                     grouping[i].totalPenalty + totalPenaltyAfter,    //penaltyAmount
                                                     "Outstanding",                              //scheduleStatus                    
                                                     "Outstanding"                               //penaltyStatus
                                                 );
                        }

                    }
                    else
                    {
                        if (grouping[i].totalPenalty >= 0 && grouping[i].aging > 0 || grouping[i].isAdj == true && grouping[i].aging > 0)
                        {
                            dt.Rows.Add(
                                            DateTime.Now,                        //creationTime
                                            userID,                               //creatorUserID
                                            grouping[i].allocCode,             //scheduleAllocCode
                                            grouping[i].totalAmountSchedule,           //scheduleNetAmount
                                            grouping[i].schedNo,               //scheduleTerm
                                            grouping[i].schedNo,               //seqNo
                                            grouping[i].bookingHeaderID,       //bookingHeaderID
                                            1,              //entityID
                                            grouping[i].aging,         //penaltyAging
                                            grouping[i].totalPenalty,           //penaltyAmount
                                            "Fully Paid",                              //scheduleStatus                    
                                            "Outstanding"                               //penaltyStatus
                                        );
                        }

                    }
                }
                catch (Exception e)
                {
                    break;
                    throw;
                }

            }

            var flagBulkUpadate = await BulkUpadate(grouping.FirstOrDefault().bookingHeaderID);
            var flagBulkInsert = await BulkInsert(dt);
            var flagRefreshPenalty = await RefreshPenalty(grouping.FirstOrDefault().bookingHeaderID);
            var flagWaivePenalty = await WaivePenalty(grouping.FirstOrDefault().bookingHeaderID);

            int bookingHeaderID = grouping[0].bookingHeaderID;
            int bookingDetailID = grouping[0].bookingDetailID;
            var flagUpdateBookingSchedule = await UpdateBookingSchedule(bookingHeaderID, bookingDetailID, userID);

            result.FailMessage = FailMessage(flagBulkUpadate, flagBulkInsert, flagRefreshPenalty, flagWaivePenalty, flagUpdateBookingSchedule);
            return result;
        }

        private string FailMessage(bool flagBulkUpadate, bool flagBulkInsert, bool flagRefreshPenalty, bool flagWaivePenalty, bool flagUpdateBookingSchedule)
        {
            string msg = "";

            if(flagBulkUpadate)
            {
                msg = "Error Process BulkUpadate";
            }
            else if(flagBulkInsert)
            {
                msg = "Error Process BulkInsert";
            }
            else if (flagRefreshPenalty)
            {
                msg = "Error Process RefreshPenalty";
            }
            else if (flagWaivePenalty)
            {
                msg = "Error Process WaivePenalty";
            }
            else if (flagUpdateBookingSchedule)
            {
                msg = "Error Process UpdateBookingSchedule";
            }


            return msg;
        }

        private async Task<bool> WaivePenalty(int bookingHeaderID)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;
                try
                {
                    var SP_CekWaivePenaltyPayment = "exec SP_CekWaivePenaltyPayment @bookingHeaderID";

                    int status = _sqlExecuter.GetFromPropertySystem<int>
                                               (SP_CekWaivePenaltyPayment, new { bookingHeaderID },
                                               System.Data.CommandType.StoredProcedure).FirstOrDefault();

                    Logger.DebugFormat("WaivePenalty() - Ended WaivePenalty");
                    flag = true;
                }
                catch (Exception)
                {
                    flag = false;
                    Logger.DebugFormat("WaivePenalty() - Wrong Description");
                }

                return flag;
            });
          
        }

        private async Task<bool> BulkUpadate(int ID)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;
                string connection = _appConfiguration.GetConnectionString("PropertySystemDbContext");
                SqlConnection con = new SqlConnection(connection);

                using (SqlCommand command = new SqlCommand("", con))
                {
                    try
                    {
                        con.Open();
                        //Creating temp table on database
                        command.CommandText = "DELETE TR_PenaltySchedule WHERE bookingHeaderID =" + ID + " and penaltyStatus = 'Outstanding' or bookingHeaderID =" + ID + " and scheduleStatus = 'Outstanding'";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        // Handle exception properly
                    }
                    finally
                    {
                        flag = true;
                        con.Close();
                    }
                }

                return flag;
            });            
        }

        private async Task<bool> BulkInsert(DataTable dt)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;
                string connection = _appConfiguration.GetConnectionString("PropertySystemDbContext");
                //string connection = "Persist Security Info=True;Data Source=10.12.1.43; Initial Catalog=STGPropertySystem; User ID=sa;Password=Password1!";
                SqlConnection con = new SqlConnection(connection);
                //create object of SqlBulkCopy which help to insert  
                SqlBulkCopy objbulk = new SqlBulkCopy(con);

                try
                {
                    //assign Destination table name  
                    objbulk.DestinationTableName = "TR_PenaltySchedule";

                    objbulk.ColumnMappings.Add("CreationTime", "CreationTime");
                    objbulk.ColumnMappings.Add("CreatorUserId", "CreatorUserId");
                    objbulk.ColumnMappings.Add("ScheduleAllocCode", "ScheduleAllocCode");
                    objbulk.ColumnMappings.Add("ScheduleNetAmount", "ScheduleNetAmount");
                    objbulk.ColumnMappings.Add("ScheduleTerm", "ScheduleTerm");
                    objbulk.ColumnMappings.Add("SeqNo", "SeqNo");
                    objbulk.ColumnMappings.Add("bookingHeaderID", "bookingHeaderID");
                    objbulk.ColumnMappings.Add("entityID", "entityID");
                    objbulk.ColumnMappings.Add("penaltyAging", "penaltyAging");
                    objbulk.ColumnMappings.Add("penaltyAmount", "penaltyAmount");
                    objbulk.ColumnMappings.Add("scheduleStatus", "scheduleStatus");
                    objbulk.ColumnMappings.Add("penaltyStatus", "penaltyStatus");

                    con.Open();
                    //insert bulk Records into DataBase.  
                    objbulk.WriteToServer(dt);

                    Logger.DebugFormat("CreatePenaltyNew() count: " + dt.Rows.Count);
                }
                catch (Exception ex)
                {
                    flag = false;
                    Logger.DebugFormat("CreatePenaltyNew() " + ex);

                }
                finally
                {
                    flag = true;
                    con.Close();
                }

                return flag;
            });            
        }

        private async Task<bool> RefreshPenalty(int bookingHeaderID)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;
                var SP_RefreshDataPenalty = "exec SP_RefreshDataPenalty @bookingHeaderID";

                try
                {
                    int status = _sqlExecuter.GetFromPropertySystem<int>
                                           (SP_RefreshDataPenalty, new { bookingHeaderID },
                                           System.Data.CommandType.StoredProcedure).FirstOrDefault();

                    Logger.DebugFormat("RefreshPenalty() - Ended RefreshPenalty");
                    flag = true;

                }
                catch (Exception ex)
                {
                    flag = false;
                    throw;
                }
                
                return flag;
            });

        
        }

        private async Task<bool> UpdateBookingSchedule(int bookingHeaderID, int bookingDetailID, int userID)
        {
            bool flag = false;
            var NetPrice = (from a in _contextPropertySystem.TR_BookingDetail.Where(ss => ss.bookingHeaderID == bookingHeaderID)
                            join b in _contextPropertySystem.TR_BookingHeader on a.bookingHeaderID equals b.Id
                            select new
                            {
                                idBookingDetail = a.Id,
                                netNetPrice = a.netNetPrice
                            }
                            ).ToList();



            var sumNetPrice = NetPrice.Sum(ss => ss.netNetPrice);
            var sumPenaltyAmount = _contextPropertySystem.TR_PenaltySchedule.Where(ss => ss.bookingHeaderID == bookingHeaderID && ss.penaltyStatus.ToUpper() == "OUTSTANDING").Sum(ss => ss.penaltyAmount);
            var sumPenaltyAmountSchedule = _contextPropertySystem.TR_PenaltySchedule.Where(ss => ss.bookingHeaderID == bookingHeaderID).Sum(ss => ss.penaltyAmount);

            decimal Net = decimal.Parse(1.1.ToString());
            decimal Vat = decimal.Parse(0.1.ToString());
            int t = 1;

            var dueDate = DateTime.Now;
            for (int i = 0; i < NetPrice.Count; i++)
            {
                var persentase = NetPrice[i].netNetPrice / sumNetPrice;
                var cekExistingDataBookingSchedule = _contextPropertySystem.TR_BookingDetailSchedule.
                                                       Where(ss => ss.bookingDetailID == NetPrice[i].idBookingDetail &&
                                                                   //ss.netAmt != 0 &&
                                                                   //ss.vatAmt != 0 &&
                                                                   ss.LK_Alloc.allocCode == "PEN").FirstOrDefault();

                var sch = _contextPropertySystem.TR_BookingDetailSchedule.Where(ss => ss.bookingDetailID == NetPrice[i].idBookingDetail && ss.LK_Alloc.allocCode != "PEN").OrderByDescending(ss => ss.schedNo).FirstOrDefault().schedNo + Convert.ToInt16(t);

                try
                {
                    if (cekExistingDataBookingSchedule != null)
                    {

                        int ID = cekExistingDataBookingSchedule.Id;

                        TR_BookingDetailSchedule toUpdate = _contextPropertySystem.TR_BookingDetailSchedule.Find(ID);
                        toUpdate.netAmt = (sumPenaltyAmountSchedule / Net) * persentase;
                        toUpdate.netOut = (sumPenaltyAmount / Net) * persentase;
                        toUpdate.vatAmt = ((sumPenaltyAmountSchedule / Net) * Vat) * persentase;
                        toUpdate.vatOut = ((sumPenaltyAmount / Net) * Vat) * persentase;
                        toUpdate.LastModificationTime = DateTime.Now;
                        toUpdate.LastModifierUserId = userID;
                        toUpdate.schedNo = Convert.ToInt16(sch);

                        _contextPropertySystem.TR_BookingDetailSchedule.Attach(toUpdate);
                        _contextPropertySystem.Entry(toUpdate).State = EntityState.Modified;

                        await _contextPropertySystem.SaveChangesAsync();
                    }
                    else
                    {
                        int alloc = _contextPropertySystem.LK_Alloc.Where(ss => ss.allocCode == "PEN").FirstOrDefault().Id;

                        TR_BookingDetailSchedule toInsert = new TR_BookingDetailSchedule();
                        toInsert.CreationTime = DateTime.Now;
                        toInsert.CreatorUserId = userID;
                        toInsert.allocID = alloc;
                        toInsert.bookingDetailID = NetPrice[i].idBookingDetail;
                        toInsert.dueDate = dueDate;
                        toInsert.entityID = 1;
                        toInsert.remarks = "Update " + DateTime.Now;
                        toInsert.schedNo = Convert.ToInt16(sch);
                        toInsert.netAmt = (sumPenaltyAmountSchedule / Net) * persentase;
                        toInsert.netOut = (sumPenaltyAmount / Net) * persentase;
                        toInsert.vatAmt = ((sumPenaltyAmountSchedule / Net) * Vat) * persentase;
                        toInsert.vatOut = ((sumPenaltyAmount / Net) * Vat) * persentase;

                        _contextPropertySystem.TR_BookingDetailSchedule.Attach(toInsert);
                        _contextPropertySystem.Entry(toInsert).State = EntityState.Added;

                        _contextPropertySystem.TR_BookingDetailSchedule.Add(toInsert);
                        await _contextPropertySystem.SaveChangesAsync();

                        flag = true;
                    }
                }
                catch (Exception e)
                {
                    flag = false;
                    throw;
                }

          
            }

            return flag;
            //try
            //{
            //    if (cekExistingDataBookingSchedule != null)
            //    {
            //        int ID = cekExistingDataBookingSchedule.Id;
            //        TR_BookingDetailSchedule toUpdate = _contextPropertySystem.TR_BookingDetailSchedule.Find(ID);
            //        toUpdate.netAmt = sumPenaltyAmount / Net;
            //        toUpdate.netOut = sumPenaltyAmount / Net;
            //        toUpdate.vatAmt = (sumPenaltyAmount / Net) * Vat;
            //        toUpdate.vatOut = (sumPenaltyAmount / Net) * Vat;
            //        toUpdate.LastModificationTime = DateTime.Now;
            //        toUpdate.LastModifierUserId = userID;
            //        toUpdate.schedNo = Convert.ToInt16(sch);

            //        _contextPropertySystem.SaveChanges();
            //    }
            //    else
            //    {
            //        int alloc = _contextPropertySystem.LK_Alloc.Where(ss => ss.allocCode == "PEN").FirstOrDefault().Id;                    

            //        TR_BookingDetailSchedule toInsert = new TR_BookingDetailSchedule();
            //        toInsert.CreationTime = DateTime.Now;
            //        toInsert.CreatorUserId = userID;
            //        toInsert.allocID = alloc;
            //        toInsert.bookingDetailID = bookingDetailID;
            //        toInsert.dueDate = DateTime.Now;
            //        toInsert.entityID = 1;
            //        toInsert.remarks = "Update " + DateTime.Now;
            //        toInsert.schedNo = Convert.ToInt16(sch);
            //        toInsert.netAmt = sumPenaltyAmount / Net;
            //        toInsert.netOut = sumPenaltyAmount / Net;
            //        toInsert.vatAmt = (sumPenaltyAmount / Net) * Vat;
            //        toInsert.vatOut = (sumPenaltyAmount / Net) * Vat;

            //        _contextPropertySystem.TR_BookingDetailSchedule.Add(toInsert);
            //        _contextPropertySystem.SaveChanges();
            //    }
            //}
            //catch (Exception)
            //{

            //    throw;
            //}

        }

    }
}
