﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VDI.Demo.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}
