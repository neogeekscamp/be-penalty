﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.AccountingDB
{
    public class SYS_JournalCounter : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return accCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(5)]
        public string accCode { get; set; }

        [Column(Order = 1)]
        [StringLength(30)]
        public string journalCode { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime inputTime { get; set; }

        [Required]
        [StringLength(50)]
        public string inputUN { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime modifTime { get; set; }

        [Required]
        [StringLength(50)]
        public string modifUN { get; set; }
    }
}
