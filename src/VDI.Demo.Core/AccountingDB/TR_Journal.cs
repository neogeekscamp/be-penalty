﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.AccountingDB
{
    [Table("TR_Journal")]
    public class TR_Journal : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                  "-" + journalCode +
                  "-" + COACodeFIN +
                  "-" + COACodeAcc +
                  "-" + journalDate +
                  "-" + debit +
                  "-" + kredit +
                  "-" + remarks;
            }
            set { /* nothing */ }
        }
        
        [StringLength(1)]
        public string entityCode { get; set; }
        
        [StringLength(30)]
        public string journalCode { get; set; }
        
        [StringLength(5)]
        public string COACodeFIN { get; set; }
        
        [StringLength(5)]
        public string COACodeAcc { get; set; }
        
        public DateTime journalDate { get; set; }
        
        [Column(TypeName = "money")]
        public decimal debit { get; set; }
        
        [Column(TypeName = "money")]
        public decimal kredit { get; set; }
        
        [StringLength(50)]
        public string remarks { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime inputTime { get; set; }

        [Required]
        [StringLength(50)]
        public string inputUN { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime modifTime { get; set; }

        [Required]
        [StringLength(50)]
        public string modifUN { get; set; }
    }
}
