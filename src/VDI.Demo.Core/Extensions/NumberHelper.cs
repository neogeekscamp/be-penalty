﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace VDI.Demo.Extensions
{
    public static class NumberHelper
    {
        /// <summary>
        /// Convert a decimal into a string with the Rp. prefix.
        /// </summary>
        public static string ToRupiah(this decimal src)
        {
            var indonesianCultureInfo = new CultureInfo("id-ID");
            indonesianCultureInfo.NumberFormat.CurrencyNegativePattern = 1;
            return src.ToString("C0", indonesianCultureInfo).Replace("Rp", "Rp. ");
        }

        public static string IndoFormat(decimal num)
        {
            var numRound = Math.Round(num);
            var numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberDecimalSeparator = ",";
            numberFormatInfo.NumberGroupSeparator = ".";
            return numRound.ToString("N", numberFormatInfo);
        }

        public static string IndoFormatNoRound(decimal num)
        {
            var numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberDecimalSeparator = ",";
            numberFormatInfo.NumberGroupSeparator = ".";
            return num.ToString("N", numberFormatInfo);
        }

        internal static string IndoFormatWithoutTail(decimal num)
        {
            var numRound = Math.Round(num);
            var numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberGroupSeparator = ".";
            return numRound.ToString("N0", numberFormatInfo);
        }

        internal static string TerbilangKoma(this decimal y)
        {
            if (y.ToString().Contains("."))
            {
                string s = y.ToString();
                string[] parts = s.Split('.');
                int i1 = int.Parse(parts[0]);
                string i2 = parts[1];
                return Terbilang(Convert.ToDecimal(i1)).TrimEnd() + TerbilangKoma(i2);
            }
            else
            {
                return TerbilangCore(y);
            }
        }

        internal static string TerbilangKoma(string y)
        {
            char[] characters = y.ToCharArray();
            var result = " Koma ";
            foreach (var chr in characters)
            {
                if (chr == '0')
                {
                    result += "Nol ";
                }
                else if (chr == '1')
                {
                    result += "Satu ";
                }
                else if (chr == '2')
                {
                    result += "Dua ";
                }
                else if (chr == '3')
                {
                    result += "Tiga ";
                }
                else if (chr == '4')
                {
                    result += "Empat ";
                }
                else if (chr == '5')
                {
                    result += "Lima ";
                }
                else if (chr == '6')
                {
                    result += "Enam ";
                }
                else if (chr == '7')
                {
                    result += "Tujuh ";
                }
                else if (chr == '8')
                {
                    result += "Delapan ";
                }
                else if (chr == '9')
                {
                    result += "Sembilan ";
                }
            }
            return result;
        }

        internal static string Terbilang(this decimal y)
        {
            return TerbilangCore(y);
        }

        internal static decimal PMT(double yearlyInterestRate, int totalNumberOfMonths, double loanAmount)
        {
            var rate = (double)yearlyInterestRate;
            var denominator = Math.Pow((1 + rate), totalNumberOfMonths) - 1;
            return Math.Abs(Convert.ToDecimal((rate + (rate / denominator)) * loanAmount));
        }

        internal static string Terbilang(this decimal? y)
        {
            return TerbilangCore(y);
        }

        private static string TerbilangCore(this decimal? y)
        {
            string[] bilangan = { "", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas" };
            string temp = "";

            if (y == null)
            {
                return "-";
            }

            long x = Convert.ToInt64(y);

            if (x < 12)
            {
                temp = " " + bilangan[x];
            }
            else if (x < 20)
            {
                temp = Terbilang(x - 10).ToString() + " Belas";
            }
            else if (x < 100)
            {
                temp = Terbilang(x / 10) + " Puluh" + Terbilang(x % 10);
            }
            else if (x < 200)
            {
                temp = " Seratus" + Terbilang(x - 100);
            }
            else if (x < 1000)
            {
                temp = Terbilang(x / 100) + " Ratus" + Terbilang(x % 100);
            }
            else if (x < 2000)
            {
                temp = " Seribu" + Terbilang(x - 1000);
            }
            else if (x < 1000000)
            {
                temp = Terbilang(x / 1000) + " Ribu" + Terbilang(x % 1000);
            }
            else if (x < 1000000000)
            {
                temp = Terbilang(x / 1000000) + " Juta" + Terbilang(x % 1000000);
            }
            else if (x < 1000000000000)
            {
                temp = Terbilang(x / 1000000000) + " Miliar" + Terbilang(x % 1000000000);
            }
            else if (x < 1000000000000000)
            {
                temp = Terbilang(x / 1000000000000) + " Triliun" + Terbilang(x % 1000000000000);
            }

            return temp;
        }
    }

}
