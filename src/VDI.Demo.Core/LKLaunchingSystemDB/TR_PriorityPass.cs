﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.LKLaunchingSystemDB
{
    [Table("TR_PriorityPass")] 
    public class TR_PriorityPass : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return PPNo;
            }
            set { /* nothing */ }
        }

        [Key]
        [StringLength(6)]
        public string PPNo { get; set; }
        
        public int batchSeq { get; set; }
        
        [StringLength(8)]
        public string psCode { get; set; }
        
        [StringLength(3)]
        public string scmCode { get; set; }
        
        [StringLength(12)]
        public string memberCode { get; set; }

        [Column(TypeName = "image")]
        public byte[] idCard { get; set; }

        public DateTime? regTime { get; set; }

        public DateTime? dealingTime { get; set; }
        
        [StringLength(1)]
        public string PPStatus { get; set; }

        [StringLength(20)]
        public string PaymentType { get; set; }

        [StringLength(30)]
        public string CardNo { get; set; }

        [StringLength(20)]
        public string bank { get; set; }
        
        public DateTime inputTime { get; set; }
        
        [StringLength(50)]
        public string inputUN { get; set; }
        
        public DateTime buyDate { get; set; }

        [StringLength(50)]
        public string Kamar { get; set; }

        [StringLength(50)]
        public string Lantai { get; set; }
        
        [StringLength(50)]
        public string KPA { get; set; }
        
        [StringLength(6)]
        public string oldPPNo { get; set; }

        public int? TableNo { get; set; }

        public int? idSetting { get; set; }

        public int? Token { get; set; }

        [StringLength(300)]
        public string remarks { get; set; }

        [StringLength(50)]
        public string inputFrom { get; set; }
    }
}
