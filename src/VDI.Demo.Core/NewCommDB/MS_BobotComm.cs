﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("MS_BobotComm")]
    public class MS_BobotComm : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode + 
                    "-" + projectCode +
                    "-" + clusterCode +
                    "-" + scmCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(5)]
        public string projectCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(5)]
        public string clusterCode { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime inputTime { get; set; }

        public long? inputUN { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? modifTime { get; set; }

        public long? modifUN { get; set; }

        public bool isActive { get; set; }

        public double pctBobot { get; set; }
    }
}
