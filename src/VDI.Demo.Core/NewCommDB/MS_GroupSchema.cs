﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("MS_GroupSchema")]
    public class MS_GroupSchema : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + groupScmCode +
                    "-" + projectCode +
                    "-" + clusterCode +
                    "-" + scmCode +
                    "-" + validFrom;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(5)]
        public string groupScmCode { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(5)]
        public string projectCode { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(5)]
        public string clusterCode { get; set; }

        [Required]
        [StringLength(50)]
        public string groupScmName { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime validFrom { get; set; }

        public short dueDateComm { get; set; }

        public bool isDealCloserUpline0 { get; set; }

        public bool isEmail { get; set; }

        public string documentGrouping { get; set; }

        public bool isActive { get; set; }

        public DateTime modifTime { get; set; }

        [Required]
        [StringLength(40)]
        public string modifUN { get; set; }

        public DateTime inputTime { get; set; }

        [Required]
        [StringLength(40)]
        public string inputUN { get; set; }       

    }
}
