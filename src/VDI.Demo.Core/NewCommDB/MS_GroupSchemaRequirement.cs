﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("MS_GroupSchemaRequirement")]
    public class MS_GroupSchemaRequirement : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + groupScmCode +
                    "-" + reqNo +
                    "-" + termCode +
                    "-" + termNo;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        public Byte reqNo { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(5)]
        public string groupScmCode { get; set; }

        [Key]
        [Required]
        [StringLength(5)]
        public string termCode { get; set; }

        [Key]
        public short termNo { get; set; }

        [Required]
        [StringLength(40)]
        public string reqDesc { get; set; }

        public double pctPaid { get; set; }

        public double orPctPaid { get; set; }

        public bool? akad { get; set; }

        public bool? disburse { get; set; }

        public DateTime modifTime { get; set; }

        [Required]
        [StringLength(40)]
        public string modifUN { get; set; }

        public DateTime inputTime { get; set; }

        [Required]
        [StringLength(40)]
        public string inputUN { get; set; }
    }
}
