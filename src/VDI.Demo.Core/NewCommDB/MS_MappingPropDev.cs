﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("MS_MappingPropDev")]
    public class MS_MappingPropDev : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + scmCode +
                    "-" + propCode +
                    "-" + devCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(5)]
        public string propCode { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(5)]
        public string devCode { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime inputTime { get; set; }

        public long? inputUN { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? modifTime { get; set; }

        public long? modifUN { get; set; }
    }
}
