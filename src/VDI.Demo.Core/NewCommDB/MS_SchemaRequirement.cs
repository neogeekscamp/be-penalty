﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("MS_SchemaRequirement")]
    public class MS_SchemaRequirement : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + scmCode +
                    "-" + reqNo ;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Key]
        [Column(Order = 2)]
        public byte reqNo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime inputTime { get; set; }

        public long? inputUN { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? modifTime { get; set; }

        public long? modifUN { get; set; }

        public double orPctPaid { get; set; }

        public double pctPaid { get; set; }

        [Required]
        [StringLength(40)]
        public string reqDesc { get; set; }
    }
}
