﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("TR_CommDev")]
    public class TR_CommDev : AuditedEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + scmCode +
                    "-" + termCode +
                    "-" + termNo +
                    "-" + projectCode +
                    "-" + accCode +
                    "-" + bankCode +
                    "-" + bankBranchCode +
                    "-" + devCode +
                    "-" + bookNo;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(5)]
        public string termCode { get; set; }

        [Key]
        [Column(Order = 3)]
        public short termNo { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(5)]
        public string projectCode { get; set; }

        public double pctComm { get; set; }

        [Column(TypeName = "money")]
        public decimal amount { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(5)]
        public string accCode { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(5)]
        public string bankCode { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(5)]
        public string bankBranchCode { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(5)]
        public string devCode { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(20)]
        public string bookNo { get; set; }

        public double pphPct { get; set; }

        public DateTime? paidDate { get; set; }

        [Column("modifTime")]
        public override DateTime? LastModificationTime { get; set; }

        [Column("modifUN")]
        public override long? LastModifierUserId { get; set; }

        [Column("inputTime")]
        public override DateTime CreationTime { get; set; }

        [Column("inputUN")]
        public override long? CreatorUserId { get; set; }
    }
}
