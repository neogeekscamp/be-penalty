﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("TR_CommPct")]
    public class TR_CommPct : AuditedEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + devCode +
                    "-" + bookNo +
                    "-" + memberCodeR +
                    "-" + asUplineNo +
                    "-" + statusCode +
                    "-" + reqNo +
                    "-" + ACDNo;
            }
            set { /* nothing */ }
        }
        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(5)]
        public string devCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string bookNo { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(20)]
        public string memberCodeR { get; set; }

        [Key]
        [StringLength(5)]
        public string statusCode { get; set; }

        [Key]
        public byte reqNo { get; set; }

        [Key]
        public byte ACDNo { get; set; }

        [Required]
        [StringLength(20)]
        public string memberCodeN { get; set; }

        [Required]
        [StringLength(3)]
        public string commTypeCode { get; set; }

        [Key]
        [Column(Order = 4)]
        public short asUplineNo { get; set; }

        public double commPctPaid { get; set; }

        [Column(TypeName = "money")]
        public decimal nominal { get; set; }

        public bool calculateUseMaster { get; set; }

        [Column("modifTime")]
        public override DateTime? LastModificationTime { get; set; }

        [Column("modifUN")]
        public override long? LastModifierUserId { get; set; }

        [Column("inputTime")]
        public override DateTime CreationTime { get; set; }

        [Column("inputUN")]
        public override long? CreatorUserId { get; set; }
    }
}
