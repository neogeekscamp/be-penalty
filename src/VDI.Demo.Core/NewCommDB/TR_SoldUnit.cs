﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("TR_SoldUnit")]
    public class TR_SoldUnit : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + devCode +
                    "-" + bookNo +
                    "-" + scmCode;
            }
            set { /* nothing */ }
        }
        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(5)]
        public string devCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string bookNo { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Required]
        [StringLength(12)]
        public string ACDCode { get; set; }

        [Required]
        [StringLength(12)]
        public string CDCode { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime inputTime { get; set; }

        public long? inputUN { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? modifTime { get; set; }

        public long? modifUN { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? PPJBDate { get; set; }

        [StringLength(100)]
        public string Remarks { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime bookDate { get; set; }

        public bool? calculateUseMaster { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? cancelDate { get; set; }

        [StringLength(100)]
        public string changeDealClosureReason { get; set; }

        [Required]
        [StringLength(5)]
        public string clusterCode { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fullFillDoc { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? holdDate { get; set; }

        [StringLength(100)]
        public string holdReason { get; set; }

        [Required]
        [StringLength(12)]
        public string memberCode { get; set; }

        [Column(TypeName = "money")]
        public decimal netNetPrice { get; set; }

        public double pctBobot { get; set; }

        public double pctComm { get; set; }

        [Required]
        [StringLength(5)]
        public string projectCode { get; set; }

        [Required]
        [StringLength(5)]
        public string propCode { get; set; }

        [Required]
        [StringLength(20)]
        public string roadCode { get; set; }

        [Required]
        [StringLength(50)]
        public string roadName { get; set; }

        [StringLength(20)]
        public string status { get; set; }

        [Required]
        [StringLength(40)]
        public string termRemarks { get; set; }

        public float unitBuildArea { get; set; }

        public float unitLandArea { get; set; }

        [Required]
        [StringLength(12)]
        public string unitNo { get; set; }

        [Column(TypeName = "money")]
        public decimal unitPrice { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? xprocessDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? xreqInstPayDate { get; set; }

        [Required]
        [StringLength(5)]
        public string termCode { get; set; }

        public short termNo { get; set; }
    }
}
