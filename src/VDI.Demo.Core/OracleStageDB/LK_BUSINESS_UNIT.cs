﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.OracleStageDB
{
    [Table("LK_BUSINESS_UNIT")]
    public class LK_BUSINESS_UNIT : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return BIZ_UNIT_ID;
            }
            set { /* nothing */ }
        }

        [Key]
        [StringLength(2)]
        public string BIZ_UNIT_ID { get; set; }

        [StringLength(50)]
        public string BUSINESS_UNIT { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? INPUT_TIME { get; set; }

        [StringLength(40)]
        public string INPUT_USER { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? MODIF_TIME { get; set; }

        [StringLength(40)]
        public string MODIF_USER { get; set; }

        [StringLength(10)]
        public string NATURE_ACCOUNT_CD { get; set; }
    }
}
