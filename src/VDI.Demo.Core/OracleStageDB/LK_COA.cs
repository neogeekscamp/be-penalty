﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.OracleStageDB
{
    [Table("LK_COA")]
    public class LK_COA : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return NATURE_ACCOUNT +
                  "-" + REF_CODE;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string NATURE_ACCOUNT { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string REF_CODE { get; set; }

        [StringLength(50)]
        public string COA_NAME { get; set; }

        [StringLength(10)]
        public string INTER_COMPANY { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? MODIF_TIME { get; set; }

        [StringLength(50)]
        public string MODIF_USER { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? INPUT_TIME { get; set; }

        [StringLength(50)]
        public string INPUT_USER { get; set; }

        [StringLength(3)]
        public string DIVISION_ID { get; set; }
    }
}
