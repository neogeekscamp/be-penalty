﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.OracleStageDB
{
    [Table("LK_PROJECT")]
    public class LK_PROJECT_ORACLE : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return PROJECT_ID;
            }
            set { /* nothing */ }
        }

        [Key]
        [StringLength(3)]
        public string PROJECT_ID { get; set; }

        [Required]
        [StringLength(2)]
        public string PROJECT_GROUP_ID { get; set; }

        [StringLength(200)]
        public string PROJECT_NAME { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? INPUT_TIME { get; set; }

        [StringLength(40)]
        public string INPUT_USER { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? MODIF_TIME { get; set; }

        [StringLength(40)]
        public string MODIF_USER { get; set; }
    }
}
