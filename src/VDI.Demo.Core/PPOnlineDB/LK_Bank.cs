﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    public class LK_Bank : AuditedEntity
    {
        [Required]
        [StringLength(30)]
        public string bankName { get; set; }
    }
}
