﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("LK_OrderStatus")]
    public class LK_OrderStatus : AuditedEntity
    {
        [Required]
        public int orderStatus { get; set; }

        [Required]
        [StringLength(30)]
        public string orderStatusName { get; set; }

        [Required]
        [StringLength(10)]
        public string hexcOLOR { get; set; }

        public ICollection<TR_PPOrder> TR_PPOrder { get; set; }
    }
}
