﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("LK_Preferred_Type")]
    public class LK_Preferred_Type : AuditedEntity
    {
        [Required]
        [StringLength(50)]
        [Column("preferredTypeName")]
        public string productName { get; set; }

        public ICollection<MS_CategoryTypePreferred> MS_CategoryTypePreferred { get; set; }
    }
}
