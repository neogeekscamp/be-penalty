﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("MS_CategoryTypePreferred")]
    public class MS_CategoryTypePreferred : AuditedEntity
    {
        [ForeignKey("LK_CategoryType")]
        public int categoryTypeId { get; set; }
        public virtual LK_CategoryType LK_CategoryType { get; set; }

        [ForeignKey("LK_Preferred_Type")]
        public int preferredTypeId { get; set; }
        public virtual LK_Preferred_Type LK_Preferred_Type { get; set; }

        [ForeignKey("MS_ProjectProduct")]
        public int projectProductId { get; set; }
        public virtual MS_ProjectProduct MS_ProjectProduct { get; set; }

        public double ppAmt { get; set; }
    }
}
