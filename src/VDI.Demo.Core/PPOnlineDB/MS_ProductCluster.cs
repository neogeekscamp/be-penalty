﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("MS_ProductCluster")]
    public class MS_ProductCluster : AuditedEntity
    {
        [ForeignKey("MS_ProjectProduct")]
        public int projectProductId { get; set; }
        public virtual MS_ProjectProduct MS_ProjectProduct { get; set; }

        public int clusterID { get; set; }
    }
}
