﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VDI.Demo.PPOnlineDB
{
    [Table("MS_ProjectProduct")]
    public class MS_ProjectProduct : AuditedEntity
    {
        [ForeignKey("MS_Projects")]
        public int projectPPOnlineId { get; set; }
        public virtual MS_Projects MS_Projects { get; set; }

        [Required]
        [StringLength(10)]
        public string projectCode { get; set; }

        [Required]
        [StringLength(50)]
        public string productName { get; set; }

        [Required]
        [StringLength(10)]
        public string productCode { get; set; }

        public double ppAmt { get; set; }

        [Required]
        [StringLength(500)]
        public string productDesc { get; set; }


        public bool isActive { get; set; }

        public ICollection<MS_ProductCluster> MS_ProductCluster { get; set; }
        public ICollection<MS_CategoryTypePreferred> MS_CategoryTypePreferred { get; set; }
    }
}
