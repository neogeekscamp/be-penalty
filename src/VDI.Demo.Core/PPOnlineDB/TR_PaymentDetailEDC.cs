﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("TR_PaymentDetailEDC")]
    public class TR_PaymentDetailEDC : AuditedEntity
    {
        [ForeignKey("TR_Payment")]
        public int paymentID { get; set; }
        public virtual TR_Payment TR_Payment { get; set; }

        [StringLength(50)]
        public string TID { get; set; }

        [StringLength(50)]
        public string MID { get; set; }

        [StringLength(50)]
        public string approvalCode { get; set; }
        
        public string remarks { get; set; }
    }
}
