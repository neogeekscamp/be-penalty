﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    public class TR_PriorityPassRefund : AuditedEntity
    {
        public int refundRef { get; set; }

        [ForeignKey("LK_RefundReason")]
        public int refundReasonID { get; set; }
        public virtual LK_RefundReason LK_RefundReason { get; set; }

        [ForeignKey("TR_PriorityPass")]
        public int priorityPassID { get; set; }
        public virtual TR_PriorityPass TR_PriorityPass { get; set; }

        [StringLength(200)]
        [Required]
        public string refundReasonOthers { get; set; }

        public DateTime refundTime { get; set; }

        [StringLength(50)]
        [Required]
        public string namaBank { get; set; }

        [StringLength(50)]
        [Required]
        public string noRek { get; set; }

        [StringLength(50)]
        [Required]
        public string namaRek { get; set; }
    }
}
