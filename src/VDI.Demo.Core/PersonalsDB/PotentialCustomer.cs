﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PersonalsDB
{
    [Table("PotentialCustomer")]
    public class PotentialCustomer : AuditedEntity
    {

        [Required]
        [StringLength(12)]
        public string MemberCode { get; set; }

        [Required]
        [StringLength(100)]
        public string MemberName { get; set; }

        [StringLength(25)]
        public string DocumentType { get; set; }

        [StringLength(100)]
        public string Event { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(15)]
        public string PhoneNumber { get; set; }

        [StringLength(100)]
        public string Email { get; set; }
        
        public DateTime? BirthDate { get; set; }

        [StringLength(16)]
        public string IdNumber { get; set; }

        [Required]
        public string DocumentBinary { get; set; }
        
        [StringLength(50)]
        public string JobCustomer { get; set; }

        [StringLength(100)]
        public string JobAddress { get; set; }

        [StringLength(75)]
        public string PriceRange { get; set; }

        [StringLength(13)]
        public string CustomerStatus { get; set; }

        public string location { get; set; }

        [Required]
        public bool isPersonal { get; set; }

        [Column("modifTime")]
        public override DateTime? LastModificationTime { get; set; }

        [Column("modifUN")]
        public override long? LastModifierUserId { get; set; }

        [Column("inputTime")]
        public override DateTime CreationTime { get; set; }

        [Column("inputUN")]
        public override long? CreatorUserId { get; set; }
    }
}
