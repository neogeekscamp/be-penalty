﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_BankDetail")]
    public class TR_BankDetail : AuditedEntity
    {
        [ForeignKey("TR_CreditAgreement")]
        public int creditAgreementID { get; set; }
        public virtual TR_CreditAgreement TR_CreditAgreement { get; set; }

        [Required]
        [StringLength(200)]
        public string address { get; set; }
        [Required]
        [StringLength(50)]
        public string picName { get; set; }
        [Required]
        [StringLength(20)]
        public string phone { get; set; }
        [Required]
        [StringLength(50)]
        public string email { get; set; }
        [Required]
        [StringLength(20)]
        public string officePhone { get; set; }
        public bool isPriority { get; set; }
        public bool isShowOnPPOnline { get; set; }
        public bool isShowOnOB { get; set; }
        public bool isHaveUserAccount { get; set; }

        public ICollection<MS_UserAccountBank> MS_UserAccountBank { get; set; }

        public virtual ICollection<TR_SLIKCheckingDetail> TR_SLIKCheckingDetail { get; set; }
    }
}
