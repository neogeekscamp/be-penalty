﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_HistoryEmailBankPortal")]
    public class TR_HistoryEmailBankPortal : AuditedEntity
    {
        [ForeignKey("MS_UserAccountBank")]
        public int userAccountBankID { get; set; }
        public virtual MS_UserAccountBank MS_UserAccountBank { get; set; }

        [ForeignKey("TR_OfferingLetter")]
        public int offeringLetterID { get; set; }
        public virtual TR_OfferingLetter TR_OfferingLetter { get; set; }

        [Required]
        [StringLength(50)]
        public string bankBranchName { get; set; }

        [ForeignKey("MS_Bank")]
        public int bankID { get; set; }
        public virtual MS_Bank MS_Bank { get; set; }


    }
}
