﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_SLIKCheckingHistory")]
    public class TR_SLIKCheckingHistory : AuditedEntity
    {
        [ForeignKey("TR_SLIKCheckingDetail")]
        public int slikCheckingDetailID { get; set; }
        public virtual TR_SLIKCheckingDetail TR_SLIKCheckingDetail { get; set; }

        public int newSlikCheckingDetailID { get; set; }
        
        public bool isActive { get; set; }

        [Required]
        public string userType { get; set; }

    }
}
