﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("LK_LetterType")]
    public class LK_LetterType : AuditedEntity
    {
        [Required]
        [StringLength(30)]
        public string letterType { get; set; }

        [Required]
        [StringLength(30)]
        public string letterDesc { get; set; }

        public int duration { get; set; }

        public ICollection<MS_SPPeriod> MS_SPPeriod { get; set; }
    }
}
