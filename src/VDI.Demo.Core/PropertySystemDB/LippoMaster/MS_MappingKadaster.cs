﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_MappingKadaster")]
    public class MS_MappingKadaster : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }
        
        public int categoryID { get; set; }
        public bool isGeneratedBySystem { get; set; }
        public int? unitID { get; set; }
        public string kadasterCode { get; set; }
        public bool isActive { get; set; }

        [Required]
        public string kadasterLocation { get; set; }

    }
}
