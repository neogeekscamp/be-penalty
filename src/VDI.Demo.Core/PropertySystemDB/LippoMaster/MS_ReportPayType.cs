﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_ReportPayType")]
    public class MS_ReportPayType : AuditedEntity
    {
        [ForeignKey("LK_PayType")]
        public int payTypeID { get; set; }
        public virtual LK_PayType LK_PayType { get; set; }

        [ForeignKey("MS_ReportType")]
        public int reportTypeID { get; set; }
        public virtual MS_ReportType MS_ReportType { get; set; }

        public bool isDefault { get; set; }
    }
}
