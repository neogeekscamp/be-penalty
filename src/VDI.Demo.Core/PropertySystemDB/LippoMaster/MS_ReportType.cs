﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_ReportType")]
    public class MS_ReportType : AuditedEntity
    {
        [Required]
        [StringLength(30)]
        public string reportType { get; set; }

        [Required]
        [StringLength(30)]
        public string reportName { get; set; }

        public virtual ICollection<MS_ReportPayType> MS_ReportPayType { get; set; }

        public virtual ICollection<MS_ReportOthersType> MS_ReportOthersType { get; set; }
    }
}
