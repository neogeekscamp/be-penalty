﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_SPPeriod")]
    public class MS_SPPeriod : AuditedEntity
    {
        public int entityID { get; set; }

        public int daysDue { get; set; }

        [Required]
        [StringLength(100)]
        public string templateName { get; set; }

        [Required]
        [StringLength(200)]
        public string templateUrl { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        [ForeignKey("LK_LetterType")]
        public int letterTypeID { get; set; }
        public virtual LK_LetterType LK_LetterType { get; set; }

        [ForeignKey("LK_SPGenerate")]
        public int SPGenerateID { get; set; }
        public virtual LK_SPGenerate LK_SPGenerate { get; set; }

        [ForeignKey("MS_Account")]
        public int accountID { get; set; }
        public virtual MS_Account MS_Account { get; set; }

        public int kuasaDir1 { get; set; }

        public int kuasaDir2 { get; set; }

        public bool isDefault { get; set; }

        public virtual ICollection<TR_ReminderLetter> TR_ReminderLetter { get; set; }

    }
}
