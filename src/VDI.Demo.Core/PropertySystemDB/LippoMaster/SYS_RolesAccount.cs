﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("SYS_RolesAccount")]
    public class SYS_RolesAccount : AuditedEntity
    {
        public int entityID { get; set; }

        public int rolesID { get; set; }

        [ForeignKey("MS_Account")]
        public int accountID { get; set; }
        public virtual MS_Account MS_Account { get; set; }
    }
}
