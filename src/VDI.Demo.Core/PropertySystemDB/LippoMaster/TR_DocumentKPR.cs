﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_DocumentKPR")]
    public class TR_DocumentKPR : AuditedEntity
    {

        [ForeignKey("TR_BookingHeader")]
        public int bookingHeaderID { get; set; }
        public virtual TR_BookingHeader TR_BookingHeader { get; set; }

        [Required]
        [StringLength(8)]
        public string psCode { get; set; }

        [Required]
        [StringLength(30)]
        public string documentType { get; set; }

        [StringLength(10)]
        public string documentPicType { get; set; }

        public int? documentRef { get; set; }

        [Column(TypeName = "image")]
        public byte[] documentBinary { get; set; }

        public bool isVerified { get; set; }

        [StringLength(200)]
        public string remarks { get; set; }

        [StringLength(250)]
        public string remarkCustomer { get; set; }
    }

}
