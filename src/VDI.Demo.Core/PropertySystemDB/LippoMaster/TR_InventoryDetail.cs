﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_InventoryDetail")]
    public class TR_InventoryDetail : AuditedEntity
    {
        public int entityID { get; set; }

        [ForeignKey("TR_InventoryHeader")]
        public int inventoryHeaderID { get; set; }
        public virtual TR_InventoryHeader TR_InventoryHeader { get; set; }

        public int payNo { get; set; }

        [ForeignKey("LK_PayType")]
        public int payTypeID { get; set; }
        public virtual LK_PayType LK_PayType { get; set; }

        [ForeignKey("MS_Bank")]
        public int bankID { get; set; }
        public virtual MS_Bank MS_Bank { get; set; }

        [Required]
        [StringLength(30)]
        public string chequeNo { get; set; }

        [ForeignKey("LK_Status")]
        public int statusID { get; set; }
        public virtual LK_Status LK_Status { get; set; }

        [Required]
        [StringLength(200)]
        public string ket { get; set; }

        public DateTime dueDate { get; set; }

        [ForeignKey("LK_PayFor")]
        public int payForID { get; set; }
        public virtual LK_PayFor LK_PayFor { get; set; }

        [Column(TypeName = "money")]
        public decimal amount { get; set; }

        public DateTime? clearDate { get; set; }

        [Required]
        [StringLength(18)]
        public string transNoPayment { get; set; }
    }
}
