﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_PaymentChangeUnknown")]
    public class TR_PaymentChangeUnknown : AuditedEntity
    {
        public int entityID { get; set; }
        
        [Required]
        [StringLength(20)]
        public string changeNo { get; set; }

        public DateTime changeDate { get; set; }

        [Required]
        [StringLength(300)]
        public string reason { get; set; }

        [ForeignKey("TR_PaymentHeader")]
        public int paymentHeaderID { get; set; }
        public virtual TR_PaymentHeader TR_PaymentHeader { get; set; }
    }
}
