﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_PaymentDetailAllocHistory")]
    public partial class TR_PaymentDetailAllocHistory : AuditedEntity
    {
        public int entityID { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short schedNo { get; set; }

        [Column(TypeName = "money")]
        public decimal netAmt { get; set; }

        [Column(TypeName = "money")]
        public decimal vatAmt { get; set; }
        
        public int paymentDetailID { get; set; }

        public short historyNo { get; set; }
    }
}
