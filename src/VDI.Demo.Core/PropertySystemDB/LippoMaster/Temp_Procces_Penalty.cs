﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("Temp_Procces_Penalty")]
    public partial class Temp_Procces_Penalty
    {
        [Key]
        public string Bookcode { get; set; }
        public int ProjectID { get; set; }
        public DateTime InputTime { get; set; }
    }
}
