﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.CreditManagement;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;

namespace VDI.Demo.PropertySystemDB.MasterPlan.Unit
{
    [Table("MS_Cluster")]
    public class MS_Cluster : AuditedEntity
    {
        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(5)]
        public string clusterCode { get; set; }

        [Required]
        [StringLength(35)]
        public string clusterName { get; set; }

        public DateTime? dueDateDevelopment { get; set; }

        [StringLength(500)]
        public string dueDateRemarks { get; set; }

        public int sortNo { get; set; }

        [StringLength(100)]
        public string handOverPeriod { get; set; }

        [StringLength(100)]
        public string gracePeriod { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }
        
        public double penaltyRate { get; set; }
        
        public int startPenaltyDay { get; set; }

        public string termConditionFile { get; set; }

        public string website { get; set; }

        public ICollection<MS_Unit> MS_Unit { get; set; }

        public ICollection<MS_BankOLBooking> MS_BankOLBooking { get; set; }

        public ICollection<MS_ProjectOLBooking> MS_ProjectOLBooking { get; set; }

        public ICollection<MP_ClusterHandOverPeriode> MP_ClusterHandOverPeriode { get; set; }

        public ICollection<MS_ClusterPenalty> MS_ClusterPenalty { get; set; }

        public ICollection<MS_SPPeriod> MS_SPPeriod { get; set; }

        public ICollection<TR_AgreementDetail> TR_AgreementDetail { get; set; }

        public ICollection<MS_UserAccountBank> MS_UserAccountBank { get; set; }

        public ICollection<TR_UpdateRetention> TR_UpdateRetention { get; set; }

        public ICollection<MS_ProjectCluster> MS_ProjectCluster { get; set; }

        
    }
}
