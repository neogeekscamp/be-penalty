﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("MS_MessageTemplate")]
    public class MS_MessageTemplate : AuditedEntity
    {
        [ForeignKey("MS_NotificationType")]
        public int notifTypeID { get; set; }
        public virtual MS_NotificationType MS_NotificationType { get; set; }

        [Required]
        [StringLength(50)]
        public string messageName { get; set; }

        [Required]
        public string messageUrl { get; set; }

        public bool isActive { get; set; }

        [StringLength(50)]
        public string CreatorUserName { get; set; }

        [StringLength(50)]
        public string LastModifierUserName { get; set; }
    }
}
