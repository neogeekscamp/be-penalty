﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("TR_NotificationContactType")]
    public class TR_NotificationContactType : AuditedEntity
    {
        [Required]
        [StringLength(30)]
        public string contactTypeName { get; set; }

        public bool isActive { get; set; }

        [StringLength(50)]
        public string CreatorUserName { get; set; }

        [StringLength(50)]
        public string LastModifierUserName { get; set; }

        public ICollection<TR_HistoryRemarks> TR_HistoryRemarks { get; set; }

    }
}
