﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("TR_PromotionNotification")]
    public class TR_PromotionNotification : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [ForeignKey("MS_NotificationType")]
        public int notifTypeID { get; set; }
        public virtual MS_NotificationType MS_NotificationType { get; set; }

        [Required]
        [StringLength(50)]
        public string promo { get; set; }

        [Required]
        [StringLength(200)]
        public string url1 { get; set; }

        [StringLength(200)]
        public string url2 { get; set; }

        [StringLength(200)]
        public string url3 { get; set; }

        [Required]
        public string promoImages1 { get; set; }
        
        public string promoImages2 { get; set; }

        public string promoImages3 { get; set; }

        public bool isActive { get; set; }

        [StringLength(50)]
        public string CreatorUserName { get; set; }

        [StringLength(50)]
        public string LastModifierUserName { get; set; }

        public DateTime start { get; set; }

        public DateTime end { get; set; }

        [StringLength(10)]
        public string groupProject { get; set; }




    }
}
