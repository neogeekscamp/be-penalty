﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("TR_Variable")]
    public class TR_Variable : AuditedEntity
    {
        [Required]
        [StringLength(50)]
        public string viewName { get; set; }

        [ForeignKey("TR_TemplateNotification")]
        public int templateNotificationID { get; set; }
        public virtual TR_TemplateNotification TR_TemplateNotification { get; set; }

        [StringLength(50)]
        public string v1 { get; set; }

        [StringLength(50)]
        public string v2 { get; set; }

        [StringLength(50)]
        public string v3 { get; set; }

        [StringLength(50)]
        public string v4 { get; set; }

        [StringLength(50)]
        public string v5 { get; set; }

        [StringLength(50)]
        public string v6 { get; set; }

        [StringLength(50)]
        public string v7 { get; set; }

        [StringLength(50)]
        public string v8 { get; set; }

        [StringLength(50)]
        public string v9 { get; set; }

        [StringLength(50)]
        public string v10 { get; set; }

        [StringLength(50)]
        public string v11 { get; set; }

        [StringLength(50)]
        public string v12 { get; set; }

        [StringLength(50)]
        public string v13 { get; set; }

        [StringLength(50)]
        public string v14 { get; set; }

        [StringLength(50)]
        public string v15 { get; set; }

        [StringLength(50)]
        public string v16 { get; set; }

        [StringLength(50)]
        public string v17 { get; set; }

        [StringLength(50)]
        public string v18 { get; set; }

        [StringLength(50)]
        public string v19 { get; set; }

        [StringLength(50)]
        public string v20 { get; set; }

        [StringLength(50)]
        public string v21 { get; set; }

        [StringLength(50)]
        public string v22 { get; set; }

        [StringLength(50)]
        public string v23 { get; set; }

        [StringLength(50)]
        public string v24 { get; set; }

        [StringLength(50)]
        public string v25 { get; set; }

        [StringLength(50)]
        public string v26 { get; set; }

        [StringLength(50)]
        public string v27 { get; set; }

        [StringLength(50)]
        public string v28 { get; set; }

        [StringLength(50)]
        public string v29 { get; set; }

        [StringLength(50)]
        public string v30 { get; set; }

        public bool isActive { get; set; }

        [StringLength(50)]
        public string CreatorUserName { get; set; }

        [StringLength(50)]
        public string LastModifierUserName { get; set; }


    }
}
