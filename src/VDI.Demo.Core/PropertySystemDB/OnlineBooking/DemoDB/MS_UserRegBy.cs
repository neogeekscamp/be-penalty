﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.Authorization.Users;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.DemoDB
{
    public class MS_UserRegBy : AuditedEntity
    {
        public long userID { get; set; }
        
        public long userRegBy { get; set; }
    }
}
