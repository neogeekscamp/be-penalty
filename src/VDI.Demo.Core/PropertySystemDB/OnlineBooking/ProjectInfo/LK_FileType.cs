﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo
{
    [Table("LK_FileType")]
    public class LK_FileType : AuditedEntity
    {
        public string name { get; set; }
        public ICollection<TR_ProjectImageGallery> TR_ProjectImageGallery { get; set; }
    }
}
