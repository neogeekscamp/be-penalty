﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
namespace VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo
{
    public class TR_ProjectSocialMedia : AuditedEntity
    {
        [ForeignKey("MS_ProjectSocialMedia")]
        public int sosialMediaID { get; set; }
        public virtual MS_ProjectSocialMedia MS_ProjectSocialMedia { get; set; }

        [Required]
        [StringLength(100)]
        public string socialMediaLink { get; set; }

        [ForeignKey("MS_ProjectInfo")]
        public int projectInfoID { get; set; }
        public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }

        public bool? isActive { get; set; }
    }
}
