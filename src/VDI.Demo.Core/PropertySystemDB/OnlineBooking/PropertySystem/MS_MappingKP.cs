﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.Pricing;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    public class MS_MappingKP : AuditedEntity
    {
        public int entityID { get; set; }
        
        public int projectID { get; set; }
        
        public int termID { get; set; }

        [Required]
        [StringLength(100)]
        public string sender { get; set; }

        public string to { get; set; }

        public string cc { get; set; }

        public string headerTemplate { get; set; }

        public string footerTemplate { get; set; }

        public string bodyTemplate { get; set; }

        public string subjectEmail { get; set; }

        public string paymentMethod1 { get; set; }

        public string paymentMethod2 { get; set; }

        public string paymentMethod3 { get; set; }

        public string paymentMethod4 { get; set; }

        public string paymentMethod5 { get; set; }

        public string paymentMethod6 { get; set; }

        public string paymentMethod7 { get; set; }

        public string paymentMethod8 { get; set; }
    }
}
