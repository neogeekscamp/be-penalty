﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("TR_BookingMoveToE1")]
    public class TR_BookingMoveToE1 : AuditedEntity
    {
        public string bookCode { get; set; }
    }
}
