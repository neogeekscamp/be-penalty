﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    public class LK_PPStatus : AuditedEntity
    {
        [Required]
        [StringLength(1)]
        public string PPStatus { get; set; }

        [Required]
        [StringLength(50)]
        public string PPStatusName { get; set; }

        public virtual ICollection<TR_PriorityPass> TR_PriorityPass { get; set; }
    }
}
