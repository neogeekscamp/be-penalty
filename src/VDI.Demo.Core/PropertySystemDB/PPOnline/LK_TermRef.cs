﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("LK_TermRef")]
    public class LK_TermRef : AuditedEntity
    {
        [Required]
        [StringLength(30)]
        public string termName { get; set; }

        public ICollection<TR_PPOrder> TR_PPOrder { get; set; }
    }
}
