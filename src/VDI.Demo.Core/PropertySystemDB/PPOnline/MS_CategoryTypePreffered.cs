﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("MS_CategoryTypePreferred")]
    public class MS_CategoryTypePreferred : AuditedEntity
    {
        public double ppAmt { get; set; }

        public int? detailID { get; set; }

        [ForeignKey("MS_ProjectPPOnline")]
        public int projectPPID { get; set; }
        public virtual MS_ProjectPPOnline MS_ProjectPPOnline { get; set; }
    }
}
