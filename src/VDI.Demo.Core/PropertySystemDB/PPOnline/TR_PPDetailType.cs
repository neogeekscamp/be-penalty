﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("TR_PPDetailType")]
    public class TR_PPDetailType : AuditedEntity
    {
        [ForeignKey("TR_PPOrder")]
        public int ppOrderID { get; set; }
        public virtual TR_PPOrder TR_PPOrder { get; set; }

        public int detailID { get; set; }

        public int termRefID { get; set; }

        public int? bankID1 { get; set; }

        public int? bankID2 { get; set; }
    }

}
