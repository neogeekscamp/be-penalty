﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("TR_PPOrder")]
    public class TR_PPOrder : AuditedEntity
    {
        [Required]
        [StringLength(20)]
        public string orderCode { get; set; }
        
        [ForeignKey("MS_ProjectInfo")]
        public int projectInfoID { get; set; }
        public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }
        
        [ForeignKey("LK_TermRef")]
        public int termRefID { get; set; }
        public virtual LK_TermRef LK_TermRef { get; set; }

        [Required]
        public int qty { get; set; }

        [ForeignKey("LK_PaymentType")]
        public int paymentTypeID { get; set; }
        public virtual LK_PaymentType LK_PaymentType { get; set; }

        [ForeignKey("LK_BookingOnlineStatus")]
        public int statusID { get; set; }
        public virtual LK_BookingOnlineStatus LK_BookingOnlineStatus { get; set; }
        
        public int? bankID1 { get; set; }

        public int? bankID2 { get; set; }

        public string psCode { get; set; }

        public string memberCode { get; set; }

        public int detailID { get; set; }

        public bool? isPriorityLine { get; set; }

        public bool? isRefundable { get; set; }

        public bool? isTransferable { get; set; }

        public virtual ICollection<TR_PPOrderPPNo> TR_PPOrderPPNo { get; set; }
        public virtual ICollection<TR_PPPayment> TR_PPPayment { get; set; }

    }
}
