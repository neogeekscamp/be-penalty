﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("TR_PPPayment")]
    public class TR_PPPayment : AuditedEntity
    {
        [ForeignKey("TR_PPOrder")]
        public int PPOrderID { get; set; }
        public virtual TR_PPOrder TR_PPOrder { get; set; }

        [ForeignKey("LK_PaymentType")]
        public int? paymentTypeID { get; set; }
        public virtual LK_PaymentType LK_PaymentType { get; set; }

        public DateTime paymentDate { get; set; }

        public double paymentAmt { get; set; }
        
        [Required]
        [StringLength(50)]
        public string bankBranch { get; set; }

        [Required]
        [StringLength(50)]
        public string bankAccName { get; set; }

        [Required]
        [StringLength(50)]
        public string bankAccNo { get; set; }

        public DateTime? clearDate { get; set; }

        [StringLength(200)]
        public string docFile { get; set; }

        public int? bankID { get; set; }
    }
}
