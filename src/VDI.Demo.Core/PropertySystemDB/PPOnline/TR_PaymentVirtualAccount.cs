﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("TR_PaymentVirtualAccount")]
    public class TR_PaymentVirtualAccount : AuditedEntity
    {
        [Required]
        [StringLength(100)]
        public string VA_BankAccNo { get; set; }

        [Column(TypeName = "money")]
        public decimal amount { get; set; }

        [StringLength(100)]
        public string transactionStatus { get; set; }

        [StringLength(100)]
        public string statusBooking { get; set; }

        public string logAksesE3 { get; set; }
    }
}
