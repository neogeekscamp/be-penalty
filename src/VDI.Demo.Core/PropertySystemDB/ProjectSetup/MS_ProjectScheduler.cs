﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PropertySystemDB.ProjectSetup
{
    public class MS_ProjectScheduler : AuditedEntity
    {
        public string project { get; set; }
        public string module { get; set; }
    }
}
