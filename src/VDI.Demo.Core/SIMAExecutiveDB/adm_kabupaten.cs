using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("adm_kabupaten")]
    public class adm_kabupaten
    {
        public int Id { get; set; }

        public int? PropinsiId { get; set; }

        [StringLength(50)]
        public string Kode { get; set; }

        [Required]
        [StringLength(50)]
        public string Nama { get; set; }

        [StringLength(255)]
        public string Keterangan { get; set; }

        public int? Published { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<sima_jenis_perijinan_wilayah> sima_jenis_perijinan_wilayah { get; set; }

        public virtual ICollection<sima_perijinan> sima_perijinan { get; set; }
    }
}
