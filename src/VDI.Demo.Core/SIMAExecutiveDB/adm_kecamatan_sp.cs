using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("adm_kecamatan_sp")]
    public class adm_kecamatan_sp
    {
        [Key]
        public int KecamatanId { get; set; }
    }
}
