using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("geometry_columns")]
    public class geometry_columns
    {
        [Key]
        [Column(Order = 0)]
        public string f_table_catalog { get; set; }

        [Key]
        [Column(Order = 1)]
        public string f_table_schema { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(256)]
        public string f_table_name { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(256)]
        public string f_geometry_column { get; set; }

        public int coord_dimension { get; set; }

        public int srid { get; set; }

        [Required]
        [StringLength(30)]
        public string geometry_type { get; set; }
    }
}
