using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("map_file")]
    public class map_file
    {
        public int Id { get; set; }

        [StringLength(250)]
        public string NAME { get; set; }

        [StringLength(250)]
        public string STATUS { get; set; }

        [StringLength(250)]
        public string SIZE { get; set; }

        [StringLength(250)]
        public string EXTENT { get; set; }

        [StringLength(250)]
        public string UNITS { get; set; }

        [StringLength(250)]
        public string SHAPEPATH { get; set; }

        [StringLength(250)]
        public string TRANSPARENT { get; set; }

        [StringLength(250)]
        public string TEMPLATEPATTERN { get; set; }

        [StringLength(250)]
        public string IMAGETYPE { get; set; }

        [StringLength(250)]
        public string WEB_METADATA { get; set; }

        [StringLength(250)]
        public string WEB_IMAGEPATH { get; set; }

        [StringLength(250)]
        public string WEB_IMAGEURL { get; set; }

        [StringLength(500)]
        public string wms_title { get; set; }

        [StringLength(500)]
        public string wms_onlineresource { get; set; }

        [StringLength(500)]
        public string wms_srs { get; set; }

        [StringLength(500)]
        public string wms_enable_request { get; set; }

        [StringLength(500)]
        public string wms_server_version { get; set; }

        [StringLength(500)]
        public string wms_feature_info_mime_type { get; set; }

        [StringLength(250)]
        public string PROJECTION { get; set; }

        [StringLength(8000)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
