using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("map_layer")]
    public class map_layer
    {
        public int Id { get; set; }

        [StringLength(250)]
        public string NAME { get; set; }

        [StringLength(250)]
        public string STATUS { get; set; }

        [StringLength(250)]
        public string FILTER { get; set; }

        [StringLength(250)]
        public string FILTERITEM { get; set; }

        [StringLength(250)]
        public string CONNECTIONTYPE { get; set; }

        [StringLength(250)]
        public string PLUGIN { get; set; }

        [StringLength(250)]
        public string CONNECTION { get; set; }

        [StringLength(250)]
        public string DATA { get; set; }

        [StringLength(250)]
        public string TYPE { get; set; }

        [StringLength(250)]
        public string EXTENT { get; set; }

        [StringLength(250)]
        public string PROJECTION { get; set; }

        [StringLength(250)]
        public string TEMPLATE { get; set; }

        [StringLength(250)]
        public string HEADER { get; set; }

        [StringLength(250)]
        public string FOOTER { get; set; }

        [StringLength(8000)]
        public string CLASS { get; set; }

        [StringLength(500)]
        public string wms_title { get; set; }

        [StringLength(500)]
        public string wms_srs { get; set; }

        [StringLength(500)]
        public string ows_include_items { get; set; }

        [StringLength(8000)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
