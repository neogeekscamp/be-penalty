using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("map_layer_class")]
    public class map_layer_class
    {
        public int Id { get; set; }

        public int LAYER_Id { get; set; }

        [StringLength(250)]
        public string NAME { get; set; }

        [StringLength(250)]
        public string EXPRESSION { get; set; }

        [StringLength(150)]
        public string LABEL_FONT { get; set; }

        [StringLength(150)]
        public string LABEL_TYPE { get; set; }

        [StringLength(150)]
        public string LABEL_ENCODING { get; set; }

        [StringLength(50)]
        public string LABEL_SIZE { get; set; }

        [StringLength(150)]
        public string LABEL_POSITION { get; set; }

        [StringLength(150)]
        public string LABEL_PARTIALS { get; set; }

        [StringLength(150)]
        public string LABEL_COLOR { get; set; }

        [StringLength(150)]
        public string LABEL_OUTLINECOLOR { get; set; }

        [StringLength(50)]
        public string STYLE_SIZE { get; set; }

        [StringLength(50)]
        public string STYLE_ANGLE { get; set; }

        [StringLength(50)]
        public string STYLE_COLOR { get; set; }

        [StringLength(50)]
        public string STYLE_SYMBOL { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
