using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_akuisisi_komposisi_share")]
    public partial class sima_akuisisi_komposisi_share
    {
        public int Id { get; set; }

        public int? AkuisisiShareId { get; set; }

        public int? StakeholderId { get; set; }

        public double? Komposisi { get; set; }

        public bool? StatusKomposisi { get; set; }

        public virtual sima_akuisisi_share sima_akuisisi_share { get; set; }

        public virtual sima_stakeholder sima_stakeholder { get; set; }
    }
}
