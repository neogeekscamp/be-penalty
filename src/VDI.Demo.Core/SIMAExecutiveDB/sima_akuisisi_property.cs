using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_akuisisi_property")]
    public class sima_akuisisi_property
    {
        public int Id { get; set; }

        public int StakeHolderId { get; set; }

        [StringLength(50)]
        public string StakeHolderKode { get; set; }

        [StringLength(150)]
        public string StakeHolderNama { get; set; }

        [Column(TypeName = "date")]
        public DateTime TanggalPengajuanAkuisisi { get; set; }

        [StringLength(50)]
        public string KodeAkuisisi { get; set; }

        [StringLength(200)]
        public string NamaAsetProspektif { get; set; }

        [StringLength(100)]
        public string PICId { get; set; }

        [StringLength(500)]
        public string PIC { get; set; }

        [StringLength(500)]
        public string AlamatAset { get; set; }

        [StringLength(500)]
        public string BatasTimur { get; set; }

        [StringLength(500)]
        public string BatasSelatan { get; set; }

        [StringLength(500)]
        public string BatasBarat { get; set; }

        [StringLength(500)]
        public string BatasUtara { get; set; }

        [StringLength(200)]
        public string PemilikSebelumnya { get; set; }

        public double? LuasTanahTotal { get; set; }

        public double? PerkiraanInvestasi { get; set; }

        public int? JangkaWaktuAkuisisi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalMulaiAkuisisi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBerakhirAkuisisi { get; set; }

        public int? JenisAsetId { get; set; }

        public int? JenisKategoriPengembanganId { get; set; }

        [StringLength(50)]
        public string JenisKategoriPengembangan { get; set; }

        [StringLength(50)]
        public string JenisPengembanganId { get; set; }

        [StringLength(500)]
        public string JenisPengembangan { get; set; }

        public byte? StatusInventoriId { get; set; }

        [StringLength(50)]
        public string StatusInventori { get; set; }

        public bool? HasilCekDokumen { get; set; }

        public double? BiayaPraAkuisisi { get; set; }

        public int? PerusahaanPembayarId { get; set; }

        [StringLength(250)]
        public string PerusahaanPembayar { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalSerahTerima { get; set; }

        [StringLength(500)]
        public string TempatSerahTerima { get; set; }

        [StringLength(255)]
        public string NamaPerwakilan1 { get; set; }

        [StringLength(255)]
        public string JabatanPerwakilan1 { get; set; }

        [StringLength(255)]
        public string NamaPerwakilan2 { get; set; }

        [StringLength(255)]
        public string JabatanPerwakilan2 { get; set; }

        public int? StatusAkuisisiId { get; set; }

        [StringLength(500)]
        public string KeteranganAkuisisi { get; set; }

        public byte? StatusPembayaran { get; set; }

        [StringLength(500)]
        public string KeteranganPembayaran { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? KotaId { get; set; }

        [StringLength(20)]
        public string KotaKode { get; set; }

        public int? NomorUrut { get; set; }

        [StringLength(50)]
        public string Notaris { get; set; }

        [StringLength(50)]
        public string NomorAJB { get; set; }

        public virtual ICollection<sima_akuisisi_property_pembayaran> sima_akuisisi_property_pembayaran { get; set; }

        public virtual sima_stakeholder sima_stakeholder { get; set; }

        public virtual sima_status_akuisisi sima_status_akuisisi { get; set; }

        [NotMapped]
        public virtual ICollection<sima_aset> sima_aset { get; set; }
    }
}
