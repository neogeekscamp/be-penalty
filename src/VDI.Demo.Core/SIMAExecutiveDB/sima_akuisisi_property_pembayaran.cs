using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_akuisisi_property_pembayaran")]
    public class sima_akuisisi_property_pembayaran
    {
        public int Id { get; set; }

        public int AkuisisiId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Tanggal { get; set; }

        public double Jumlah { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(50)]
        public string NomorAPV { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalAPV { get; set; }

        [StringLength(500)]
        public string KeteranganAPV { get; set; }

        [StringLength(50)]
        public string Accounting { get; set; }

        [StringLength(50)]
        public string NomorBDV { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPembayaran { get; set; }

        public double? JumlahPembayaran { get; set; }

        [StringLength(500)]
        public string KeteranganBDV { get; set; }

        [StringLength(50)]
        public string Finance { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? AsetId { get; set; }

        public virtual sima_akuisisi_property sima_akuisisi_property { get; set; }
    }
}
