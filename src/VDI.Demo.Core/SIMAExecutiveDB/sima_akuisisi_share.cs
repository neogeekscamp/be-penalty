using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_akuisisi_share")]
    public class sima_akuisisi_share
    {
        public int Id { get; set; }

        public int StakeHolderId { get; set; }

        [StringLength(50)]
        public string StakeHolderKode { get; set; }

        [StringLength(150)]
        public string StakeHolderNama { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPengajuanAkuisisi { get; set; }

        [StringLength(50)]
        public string KodeAkuisisi { get; set; }

        [StringLength(250)]
        public string NamaProject { get; set; }

        [StringLength(100)]
        public string PICId { get; set; }

        [StringLength(500)]
        public string PIC { get; set; }

        public int? PerusahaanId { get; set; }

        [StringLength(150)]
        public string NamaParusahaan { get; set; }

        [StringLength(500)]
        public string DomisiliPerusahaan { get; set; }

        [StringLength(500)]
        public string PemilikSahamYangDiakuisisi { get; set; }

        [StringLength(1000)]
        public string KomposisiSebelum { get; set; }

        [StringLength(1000)]
        public string KomposisiSetelah { get; set; }

        public int? StatusAsetId { get; set; }

        [StringLength(50)]
        public string PeriodeJaminan { get; set; }

        public double? NilaiJaminan { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalAkuisisi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalSerahTerima { get; set; }

        [StringLength(255)]
        public string TempatSerahTerima { get; set; }

        [StringLength(255)]
        public string NamaPerwakilan1 { get; set; }

        [StringLength(255)]
        public string JabatanPerwakilan1 { get; set; }

        [StringLength(255)]
        public string NamaPerwakilan2 { get; set; }

        [StringLength(255)]
        public string JabatanPerwakilan2 { get; set; }

        public int? StatusAkuisisiId { get; set; }

        [StringLength(500)]
        public string KeteranganAkuisisi { get; set; }

        public double? BiayaPraAkuisisi { get; set; }

        public int? PerusahaanPembayarId { get; set; }

        [StringLength(250)]
        public string PerusahaanPembayar { get; set; }

        public byte? StatusPembayaran { get; set; }

        [StringLength(500)]
        public string KeteranganPembayaran { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? KotaId { get; set; }

        [StringLength(20)]
        public string KotaKode { get; set; }

        public int? NomorUrut { get; set; }

        [StringLength(50)]
        public string Notaris { get; set; }

        [StringLength(50)]
        public string NomorAJB { get; set; }

        public int? JangkaWaktuAkuisisi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalMulaiAkuisisi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBerakhirAkuisisi { get; set; }

        public virtual ICollection<sima_akuisisi_komposisi_share> sima_akuisisi_komposisi_share { get; set; }

        public virtual ICollection<sima_akuisisi_share_pembayaran> sima_akuisisi_share_pembayaran { get; set; }

        public virtual sima_stakeholder sima_stakeholder { get; set; }

        public virtual sima_status_akuisisi sima_status_akuisisi { get; set; }
    }
}
