using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_asuransi")]
    public class sima_aset_asuransi
    {
        public int Id { get; set; }

        public int AsetId { get; set; }

        public int AsuransiId { get; set; }

        public virtual sima_aset sima_aset { get; set; }

        public virtual sima_asuransi sima_asuransi { get; set; }
    }
}
