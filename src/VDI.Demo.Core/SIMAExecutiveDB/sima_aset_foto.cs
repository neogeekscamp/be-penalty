using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_foto")]
    public class sima_aset_foto
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(150)]
        public string TableNameKey { get; set; }

        [Key]
        [Column(Order = 1)]
        public int TablePKValue { get; set; }

        [Key]
        [Column(Order = 2)]
        public int Nomor { get; set; }

        [Required]
        [StringLength(350)]
        public string NamaFile { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
