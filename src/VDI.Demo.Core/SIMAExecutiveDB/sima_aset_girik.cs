using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_girik")]
    public class sima_aset_girik
    {
        public long Id { get; set; }

        public int AsetId { get; set; }

        [Required]
        [StringLength(50)]
        public string NomorGirik { get; set; }

        [Required]
        [StringLength(50)]
        public string NomorPersil { get; set; }

        [Required]
        [StringLength(50)]
        public string NomorSPH { get; set; }

        public DateTime? TanggalSPH { get; set; }

        [StringLength(50)]
        public string NamaPemegangHak { get; set; }

        public double LuasTanah { get; set; }

        public int? PropinsiId { get; set; }

        [StringLength(50)]
        public string Propinsi { get; set; }

        public int? KabupatenId { get; set; }

        [StringLength(50)]
        public string Kabupaten { get; set; }

        public int? KecamatanId { get; set; }

        [StringLength(50)]
        public string Kecamatan { get; set; }

        [StringLength(500)]
        public string AlamatGirik { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public byte? LastData { get; set; }
    }
}
