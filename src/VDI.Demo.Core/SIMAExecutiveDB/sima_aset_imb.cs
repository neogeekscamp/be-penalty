using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_imb")]
    public class sima_aset_imb
    {
        public int Id { get; set; }

        public int? AsetId { get; set; }

        [StringLength(50)]
        public string NomorDokumen { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalDokumen { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalAwal { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalAkhir { get; set; }

        [StringLength(500)]
        public string AlamatSertipikat { get; set; }

        [StringLength(150)]
        public string AtasNama { get; set; }

        public int? PeruntukanId { get; set; }

        [StringLength(100)]
        public string PeruntukanTanah { get; set; }

        [StringLength(50)]
        public string KlasifikasiBangunan { get; set; }

        public double? LuasTanah { get; set; }

        public double? LuasBangunan { get; set; }

        public int? JumlahLantai { get; set; }

        public int? JumlahLantaiMaksimum { get; set; }

        public double? TinggiBangunan { get; set; }

        public double? KetinggianMaksimum { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        public bool? StopReminder { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual sima_aset sima_aset { get; set; }
    }
}
