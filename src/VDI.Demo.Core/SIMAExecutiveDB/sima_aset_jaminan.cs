using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_jaminan")]
    public class sima_aset_jaminan
    {
        public int Id { get; set; }

        public int? AsetId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalMulai { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBerakhir { get; set; }

        [StringLength(500)]
        public string NamaPenjamain { get; set; }

        [StringLength(50)]
        public string NomorHakTanggungan { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalHakTanggungan { get; set; }

        [StringLength(50)]
        public string PeringkatHakTanggungan { get; set; }

        [StringLength(50)]
        public string PPATHakTanggungan { get; set; }

        [StringLength(50)]
        public string NomorAPHT { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalAPHT { get; set; }

        [StringLength(50)]
        public string NomorRoya { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalRoya { get; set; }

        [StringLength(2000)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
