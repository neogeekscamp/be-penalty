using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_kasus")]
    public class sima_aset_kasus
    {
        public int Id { get; set; }

        public int AsetId { get; set; }

        public int KasusId { get; set; }

        public virtual sima_aset sima_aset { get; set; }

        public virtual sima_kasus sima_kasus { get; set; }
    }
}
