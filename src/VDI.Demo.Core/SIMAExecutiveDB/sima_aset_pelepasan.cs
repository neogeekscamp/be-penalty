using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_pelepasan")]
    public class sima_aset_pelepasan
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string KodeAset { get; set; }

        [StringLength(250)]
        public string NoSP { get; set; }

        public DateTime? TanggalSP { get; set; }

        public DateTime? TanggalLepasHak { get; set; }

        public int? JenisLepasId { get; set; }

        [StringLength(150)]
        public string TujuanLepasHak { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
