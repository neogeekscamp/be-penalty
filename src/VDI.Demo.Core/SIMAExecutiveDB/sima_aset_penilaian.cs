using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_penilaian")]
    public class sima_aset_penilaian
    {
        public int Id { get; set; }

        public int? AsetId { get; set; }

        public int? PenilaianId { get; set; }

        public double? NilaiPasar { get; set; }

        public double? NilaiLikuidasi { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual sima_aset sima_aset { get; set; }

        public virtual sima_penilaian sima_penilaian { get; set; }
    }
}
