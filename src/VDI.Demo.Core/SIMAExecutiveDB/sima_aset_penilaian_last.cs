using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_penilaian_last")]
    public class sima_aset_penilaian_last
    {
        [Key]
        public int AsetId { get; set; }

        public int? PenilaianId { get; set; }

        public double? NilaiPasar { get; set; }

        public double? NilaiLikuidasi { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }
    }
}
