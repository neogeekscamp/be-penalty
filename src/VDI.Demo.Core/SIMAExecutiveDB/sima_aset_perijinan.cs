using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_perijinan")]
    public class sima_aset_perijinan
    {
        public int Id { get; set; }

        public int AsetId { get; set; }

        public int PerijinanId { get; set; }

        public virtual sima_aset sima_aset { get; set; }

        public virtual sima_jenis_perijinan sima_jenis_perijinan { get; set; }
    }
}
