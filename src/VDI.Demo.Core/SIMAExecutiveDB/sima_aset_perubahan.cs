using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_perubahan")]
    public class sima_aset_perubahan
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int? AsetId { get; set; }

        public DateTime? TanggalPerubahan { get; set; }

        [StringLength(500)]
        public string JenisPerubahan { get; set; }

        [StringLength(500)]
        public string AlasanPerubahan { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
