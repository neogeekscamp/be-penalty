using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_reminder")]
    public class sima_aset_reminder
    {
        [Key]
        public int AsetId { get; set; }

        public bool? Sertipikat1 { get; set; }

        public bool? Sertipikat2 { get; set; }

        public bool? Sertipikat3 { get; set; }

        public bool? Sertipikat4 { get; set; }

        public bool? Sertipikat6 { get; set; }

        public bool? PBB1 { get; set; }

        public bool? PBB2 { get; set; }

        public bool? PBB3 { get; set; }

        public bool? PBB4 { get; set; }

        public bool? IMB1 { get; set; }

        public bool? IMB2 { get; set; }

        public bool? IMB3 { get; set; }

        public bool? IMB4 { get; set; }

        public bool? Penilaian1 { get; set; }

        public bool? Penilaian2 { get; set; }

        public bool? Penilaian3 { get; set; }

        public bool? Penilaian4 { get; set; }

        public bool? Penilaian5 { get; set; }

        public bool? Penilaian6 { get; set; }

        public bool? Perijinan1 { get; set; }

        public bool? Perijinan2 { get; set; }

        public bool? Perijinan3 { get; set; }

        public bool? Perijinan4 { get; set; }

        public virtual sima_aset sima_aset { get; set; }
    }
}
