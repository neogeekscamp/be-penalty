﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_sertipikat")]
    public class sima_aset_sertipikat  
    {
        [Key]
        public int AsetID { get; set; }

        [Required]
        [StringLength(50)]
        public string NomorSertipikat { get; set; }

        [StringLength(100)]
        public string AlamatSertipikat { get; set; }

        public int JenisHakAtasTanahID { get; set; }

        public DateTime? TanggalBerakhirHak { get; set; }

        [StringLength(50)]
        public string KantorBPN { get; set; }

        [StringLength(50)]
        public string NomorRegister { get; set; }

        [StringLength(50)]
        public string NIBLetakTanah { get; set; }

        public int? AsalHakAtasTanahID { get; set; }

        [StringLength(255)]
        public string DasarPendaftaran { get; set; }

        public DateTime? TanggalSuratUkur { get; set; }

        [StringLength(50)]
        public string NomorSuratUkur { get; set; }

        public double? LuasTanah { get; set; }

        [StringLength(50)]
        public string NamaPemegangHak { get; set; }

        public DateTime? TanggalLahirAkta { get; set; }

        public DateTime? TanggalPembukuan { get; set; }

        [StringLength(50)]
        public string NamaPejabatPembukuan { get; set; }

        [StringLength(50)]
        public string NIPPejabatPembukuan { get; set; }

        public DateTime? TanggalPenerbitan { get; set; }

        [StringLength(50)]
        public string NamaPejabatPenerbit { get; set; }

        [StringLength(50)]
        public string NIPPejabatPenerbit { get; set; }

        [StringLength(255)]
        public string Penunjuk { get; set; }

        [StringLength(50)]
        public string Peta { get; set; }

        [StringLength(50)]
        public string NomorPetaPendaftaran { get; set; }

        [StringLength(50)]
        public string Lembar { get; set; }

        [StringLength(50)]
        public string Kotak { get; set; }

        [StringLength(255)]
        public string KeadaanTanah { get; set; }

        [StringLength(255)]
        public string TandaBatas { get; set; }

        [StringLength(50)]
        public string PenunjukanPenetapanBatas { get; set; }

        [StringLength(50)]
        public string AsalSuratUkur { get; set; }

        [StringLength(50)]
        public string PemohonSuratUkur { get; set; }

        [StringLength(50)]
        public string PetugasUkur { get; set; }

        public DateTime? TanggalDaftarIsian302 { get; set; }

        [StringLength(50)]
        public string NomorDaftarIsian302 { get; set; }

        public DateTime? TanggalDaftarIsian307 { get; set; }

        [StringLength(50)]
        public string NomorDaftarIsian307 { get; set; }

        [StringLength(50)]
        public string NamaPejabatUkur { get; set; }

        [StringLength(50)]
        public string NIPPejabatUkur { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(50)]
        public string StopReminder { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public long Id { get; set; }

        public byte? LastData { get; set; }

        public virtual sima_jenis_hak_atas_tanah sima_jenis_hak_atas_tanah { get; set; }
    }
}
