using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_aset_stakeholder")]
    public class sima_aset_stakeholder
    {
        [Key]
        public int AsetId { get; set; }

        public int PemilikId { get; set; }

        public int? DeveloperId { get; set; }

        public int? OperatorId { get; set; }

        public int? PenyewaId { get; set; }

        public virtual sima_aset sima_aset { get; set; }
    }
}
