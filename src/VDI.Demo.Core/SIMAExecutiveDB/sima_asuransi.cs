using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_asuransi")]
    public class sima_asuransi
    {
        public int Id { get; set; }

        public int? PerusahaanAsuransiId { get; set; }

        [StringLength(150)]
        public string PerusahaanAsuransi { get; set; }

        [StringLength(50)]
        public string NomorPolis { get; set; }

        [StringLength(150)]
        public string PemegangPolis { get; set; }

        [StringLength(500)]
        public string Alamat { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalMulaiPolis { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBerakhirPolis { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalJatuhTempoPremi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPelunasanPremi { get; set; }

        public double? NilaiPertanggungan { get; set; }

        public int? TipeResikoAsuransiId { get; set; }

        [StringLength(500)]
        public string TipeResikoAsuransi { get; set; }

        [StringLength(500)]
        public string ResikoTambahan { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalTerbitPolis { get; set; }

        public double? BiayaPremi { get; set; }

        public double? Rate { get; set; }

        public double? Biaya { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBayar { get; set; }

        [StringLength(50)]
        public string Pembayar { get; set; }

        [StringLength(100)]
        public string PICId { get; set; }

        [StringLength(500)]
        public string PIC { get; set; }

        public bool? StopReminder { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sima_aset_asuransi> sima_aset_asuransi { get; set; }
    }
}
