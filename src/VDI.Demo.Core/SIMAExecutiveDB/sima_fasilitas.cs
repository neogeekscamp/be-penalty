using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_fasilitas")]
    public class sima_fasilitas
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Kode { get; set; }

        [StringLength(50)]
        public string Nama { get; set; }

        public int? KawasanId { get; set; }

        [StringLength(50)]
        public string KawasanKode { get; set; }

        [StringLength(50)]
        public string Kawasan { get; set; }

        public int? ClusterId { get; set; }

        [StringLength(50)]
        public string ClusterKode { get; set; }

        [StringLength(50)]
        public string Cluster { get; set; }

        public int? JenisFasilitasId { get; set; }

        [StringLength(50)]
        public string JenisFasilitasKode { get; set; }

        [StringLength(50)]
        public string JenisFasilitas { get; set; }

        [StringLength(255)]
        public string Keterangan { get; set; }

        public int? Published { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
