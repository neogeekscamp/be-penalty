using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_jenis_perijinan_wilayah")]
    public class sima_jenis_perijinan_wilayah
    {
        public int Id { get; set; }

        public int? JenisPerijinanId { get; set; }

        public int? PropinsiId { get; set; }

        public int? KabupatenId { get; set; }

        public int? InstansiId { get; set; }

        [StringLength(150)]
        public string Instansi { get; set; }

        public int? Timeline { get; set; }

        public int? TimeUnitId { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        public int? Published { get; set; }

        public int? Ordering { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual adm_kabupaten adm_kabupaten { get; set; }

        public virtual adm_propinsi adm_propinsi { get; set; }

        public virtual sima_instansi_perijinan sima_instansi_perijinan { get; set; }

        public virtual sima_jenis_perijinan sima_jenis_perijinan { get; set; }
    }
}
