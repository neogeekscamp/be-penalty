using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_kasus")]
    public class sima_kasus
    {
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string NoPerkara { get; set; }

        public int? JenisKasusId { get; set; }

        public DateTime? TanggalKasus { get; set; }

        public int? StatusId { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(4000)]
        public string DeskripsiKasus { get; set; }

        [StringLength(1000)]
        public string PihakLawan { get; set; }

        [StringLength(255)]
        public string PenanggungJawabInternal { get; set; }

        [StringLength(255)]
        public string KuasaHukum { get; set; }

        [StringLength(100)]
        public string PengadialanTingkatPertama { get; set; }

        public int? StatusPutusanTingkatPertama { get; set; }

        [StringLength(250)]
        public string NoPutusanTingkatPertama { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPutusanTingkatPertama { get; set; }

        [StringLength(4000)]
        public string IsiPutusanTingkatPertama { get; set; }

        [StringLength(100)]
        public string PengadialanTingkatBanding { get; set; }

        public int? StatusPutusanTingkatBanding { get; set; }

        [StringLength(250)]
        public string NoPutusanTingkatBanding { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPutusanTingkatBanding { get; set; }

        [StringLength(4000)]
        public string IsiPutusanTingkatBanding { get; set; }

        public int? StatusPutusanTingkatKasasi { get; set; }

        [StringLength(250)]
        public string NoPutusanTingkatKasasi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPutusanTingkatKasasi { get; set; }

        [StringLength(4000)]
        public string IsiPutusanTingkatKasasi { get; set; }

        public int? StatusPutusanTingkatPK { get; set; }

        [StringLength(250)]
        public string NoPutusanTingkatPK { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPutusanTingkatPK { get; set; }

        [StringLength(4000)]
        public string IsiPutusanTingkatPK { get; set; }

        [StringLength(2000)]
        public string Keterangan { get; set; }

        [StringLength(100)]
        public string PICId { get; set; }

        [StringLength(500)]
        public string PIC { get; set; }

        public bool? StopReminder { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? StatusKasusId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalStatusKasus { get; set; }

        public virtual ICollection<sima_aset_kasus> sima_aset_kasus { get; set; }

        public virtual sima_jenis_kasus sima_jenis_kasus { get; set; }

        public virtual sima_kasus_pihak sima_kasus_pihak { get; set; }
    }
}
