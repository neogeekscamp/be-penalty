using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_kerjasama")]
    public class sima_kerjasama
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public int? JenisKerjasamaId { get; set; }

        [StringLength(500)]
        public string Perihal { get; set; }

        [StringLength(150)]
        public string NomorSurat { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalSurat { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalMulai { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBeroperasi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBerakhir { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        public int? GracePeriod { get; set; }

        public int? JumlahGracePeriod { get; set; }

        public int? OpsiPerpanjangan { get; set; }

        public int? PerusahaanJVId { get; set; }

        public int? FirstRightsOfRefusal { get; set; }

        public double? HargaSewa { get; set; }

        public int? StatusKerjasamaId { get; set; }

        [StringLength(100)]
        public string PICId { get; set; }

        [StringLength(500)]
        public string PIC { get; set; }

        public bool? StopReminder { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(500)]
        public string KeteranganSewa { get; set; }

        [StringLength(150)]
        public string NoPraPerjanjian { get; set; }

        public int? NomorUrut { get; set; }

        [StringLength(50)]
        public string Kode { get; set; }

        [StringLength(150)]
        public string PerusahaanJVNama { get; set; }

        public virtual sima_jenis_kerjasama sima_jenis_kerjasama { get; set; }

        public virtual ICollection<sima_kerjasama_pembayaran> sima_kerjasama_pembayaran { get; set; }

        public virtual ICollection<sima_kerjasama_reminder> sima_kerjasama_reminder { get; set; }

        public virtual sima_status_kerjasama sima_status_kerjasama { get; set; }

        [NotMapped]
        public virtual ICollection<sima_kerjasama_stakeholder> sima_kerjasama_stakeholder { get; set; }

        [NotMapped]
        public virtual ICollection<sima_kerjasama_tahapan> sima_kerjasama_tahapan { get; set; }

        [NotMapped]
        public virtual ICollection<sima_aset> sima_aset { get; set; }
    }
}
