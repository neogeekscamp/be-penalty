using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_kerjasama_reminder")]
    public class sima_kerjasama_reminder
    {
        [Key]
        [Column(Order = 0)]
        public int KerjasamaId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int ReminderId { get; set; }

        public int? JumlahReminder { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalReminder { get; set; }

        [StringLength(500)]
        public string EmailReminder { get; set; }

        public virtual sima_kerjasama sima_kerjasama { get; set; }

        public virtual sima_reminder sima_reminder { get; set; }
    }
}
