using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_kerjasama_stakeholder")]
    public class sima_kerjasama_stakeholder
    {
        public int Id { get; set; }

        public int KerjasamaId { get; set; }

        public int JenisKerjasamaId { get; set; }

        public int StatusPihakId { get; set; }

        public int StakeHolderId { get; set; }

        [StringLength(150)]
        public string Nama { get; set; }

        [StringLength(150)]
        public string Jabatan { get; set; }

        [StringLength(500)]
        public string Alamat { get; set; }

        [StringLength(1000)]
        public string Keterangan { get; set; }

        public double? Jumlah { get; set; }

        public double? Nominal { get; set; }

        public double? Persen { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual sima_kerjasama sima_kerjasama { get; set; }

        public virtual sima_stakeholder sima_stakeholder { get; set; }

        public virtual sima_status_pihak sima_status_pihak { get; set; }
    }
}
