using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_kerjasama_tahapan")]
    public class sima_kerjasama_tahapan
    {
       public int Id { get; set; }

        public int? KerjasamaId { get; set; }

        public int? TahapanId { get; set; }

        [StringLength(250)]
        public string Nama { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalAwal { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalAkhir { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        public bool? Status { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual sima_kerjasama sima_kerjasama { get; set; }
    }
}
