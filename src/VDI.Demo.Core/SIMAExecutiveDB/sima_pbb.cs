using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_pbb")]
    public class sima_pbb
    {
       public int Id { get; set; }

        public int? Tahun { get; set; }

        [StringLength(50)]
        public string NomorSPPT { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalSPPT { get; set; }

        [StringLength(50)]
        public string NOP { get; set; }

        [StringLength(255)]
        public string LetakObjekPajak { get; set; }

        [StringLength(50)]
        public string NamaWP { get; set; }

        [StringLength(255)]
        public string AlamatWP { get; set; }

        [StringLength(50)]
        public string NPWP { get; set; }

        public double? LuasBumi { get; set; }

        [StringLength(50)]
        public string KelasBumi { get; set; }

        public double? NJOPBumiPerM2 { get; set; }

        public double? NJOPBumi { get; set; }

        public double? LuasBangunan { get; set; }

        [StringLength(50)]
        public string KelasBangunan { get; set; }

        public double? NJOPBangunanPerM2 { get; set; }

        public double? NJOPBangunan { get; set; }

        public double? NJOPDasar { get; set; }

        public double? NJOPTKP { get; set; }

        public double? NJOPPerhitungan { get; set; }

        public double? NJKP { get; set; }

        public double? PBBTerutang { get; set; }

        public double? PBBHarusDibayar { get; set; }

        [Column(TypeName = "date")]
        public DateTime TanggalJatuhTempo { get; set; }

        [StringLength(50)]
        public string TempatPembayaran { get; set; }

        [StringLength(50)]
        public string NomorSTTS { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPembayaran { get; set; }

        public double? JumlahPembayaran { get; set; }

        public double? JumlahDenda { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        public bool? StopReminder { get; set; }

        public double? NJKPPercent { get; set; }

        public double? PBBPercent { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [NotMapped]
        public virtual ICollection<sima_aset> sima_aset { get; set; }
    }
}
