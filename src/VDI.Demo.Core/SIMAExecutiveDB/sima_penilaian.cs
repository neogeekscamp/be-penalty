using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_penilaian")]
    public class sima_penilaian
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string NoDokumen { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPenilaian { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBerakhir { get; set; }

        public int? CurrencyId { get; set; }

        public double? NilaiPasar { get; set; }

        public double? NilaiLikuidasi { get; set; }

        public int? PerusahaanPenilaiId { get; set; }

        [StringLength(150)]
        public string PerusahaanPenilai { get; set; }

        [StringLength(500)]
        public string AlamatPerusahaanPenilai { get; set; }

        public int? PenilaiId { get; set; }

        [StringLength(150)]
        public string NamaPenilai { get; set; }

        [StringLength(500)]
        public string AlamatPenilai { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(100)]
        public string PICId { get; set; }

        [StringLength(500)]
        public string PIC { get; set; }

        public bool? StopReminder { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<sima_aset_penilaian> sima_aset_penilaian { get; set; }

        public virtual sima_penilai sima_penilai { get; set; }

        public virtual sima_perusahaan_penilai sima_perusahaan_penilai { get; set; }
    }
}
