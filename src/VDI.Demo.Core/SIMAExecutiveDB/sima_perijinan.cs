using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_perijinan")]
    public class sima_perijinan
    {
        public int Id { get; set; }

        public int? JenisPerijinanId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalPerijinan { get; set; }

        [StringLength(50)]
        public string NoPerijinan { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalMulai { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalBerakhir { get; set; }

        public int? PropinsiId { get; set; }

        public int? KabupatenId { get; set; }

        public int? InstansiPemberiPerijinanId { get; set; }

        [StringLength(200)]
        public string InstansiPemberiPerijinan { get; set; }

        [StringLength(4000)]
        public string Keterangan { get; set; }

        [StringLength(100)]
        public string PICId { get; set; }

        [StringLength(500)]
        public string PIC { get; set; }

        public bool? StopReminder { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual adm_kabupaten adm_kabupaten { get; set; }

        public virtual adm_propinsi adm_propinsi { get; set; }

        public virtual sima_instansi_perijinan sima_instansi_perijinan { get; set; }

        public virtual sima_jenis_perijinan sima_jenis_perijinan { get; set; }
    }
}
