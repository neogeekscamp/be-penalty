using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_reminder")]
    public class sima_reminder
    {

        public int Id { get; set; }

        public int? KategoriId { get; set; }

        [StringLength(50)]
        public string Kategori { get; set; }

        public int? StatusId { get; set; }

        [StringLength(250)]
        public string Nama { get; set; }

        [StringLength(250)]
        public string NamaEn { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        public int? Offset { get; set; }

        public int? SatuanOffset { get; set; }

        public int? Periode { get; set; }

        public int? SatuanPeriode { get; set; }

        [StringLength(500)]
        public string EmailSubject { get; set; }

        [StringLength(4000)]
        public string EmailBody { get; set; }

        public int? Published { get; set; }

        public int? Ordering { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(500)]
        public string PICId { get; set; }

        [StringLength(1000)]
        public string PIC { get; set; }

        public virtual ICollection<sima_kerjasama_reminder> sima_kerjasama_reminder { get; set; }
    }
}
