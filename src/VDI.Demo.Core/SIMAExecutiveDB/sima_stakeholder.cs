using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_stakeholder")]
    public class sima_stakeholder
    {
        public int Id { get; set; }

        public int JenisStakeholderId { get; set; }

        public int? Internal { get; set; }

        public int? GrupStakeholderId { get; set; }

        [StringLength(50)]
        public string Kode { get; set; }

        [Required]
        [StringLength(150)]
        public string Nama { get; set; }

        [StringLength(500)]
        public string Alamat { get; set; }

        [StringLength(150)]
        public string TempatLahir { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalLahir { get; set; }

        public short? JenisKelamin { get; set; }

        [StringLength(150)]
        public string Pekerjaan { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        public int? Published { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<sima_akuisisi_komposisi_share> sima_akuisisi_komposisi_share { get; set; }

        public virtual ICollection<sima_akuisisi_property> sima_akuisisi_property { get; set; }

        public virtual ICollection<sima_akuisisi_share> sima_akuisisi_share { get; set; }

        public virtual ICollection<sima_kerjasama_stakeholder> sima_kerjasama_stakeholder { get; set; }
    }
}
