using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_status_pihak")]
    public class sima_status_pihak
    {
        
        [Key]
        [Column(Order = 0)]
        public int JenisKerjasamaId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }

        [StringLength(100)]
        public string Nama { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        public int? Published { get; set; }

        public int? Ordering { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<sima_kerjasama_stakeholder> sima_kerjasama_stakeholder { get; set; }
    }
}
