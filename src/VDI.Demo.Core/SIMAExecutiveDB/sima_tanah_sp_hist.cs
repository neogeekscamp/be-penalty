using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_tanah_sp_hist")]
    public class sima_tanah_sp_hist
    {
        public int Id { get; set; }

        public int AsetId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalTerbit { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalHapus { get; set; }
    }
}
