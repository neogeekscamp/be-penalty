using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_tanah_sp_ori")]
    public class sima_tanah_sp_ori
    {
        [Key]
        public int AsetId { get; set; }
    }
}
