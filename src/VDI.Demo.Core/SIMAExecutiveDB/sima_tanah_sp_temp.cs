using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sima_tanah_sp_temp")]
    public class sima_tanah_sp_temp
    {
        public int Id { get; set; }

        public int? AsetId { get; set; }

        public int? ParentId { get; set; }

        public int? JenisAsetId { get; set; }

        [StringLength(50)]
        public string Kode { get; set; }

        [StringLength(50)]
        public string Nama { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalAkuisisi { get; set; }

        public double? LuasTanah { get; set; }

        public double? LuasBangunan { get; set; }

        public int? JumlahLantai { get; set; }

        public int? JumlahLantaiMaksimum { get; set; }

        public double? TinggiBangunan { get; set; }

        public double? KetinggianMaksimum { get; set; }

        public int? KondisiId { get; set; }

        [StringLength(200)]
        public string Kondisi { get; set; }

        public int? StatusKonstruksiId { get; set; }

        [StringLength(500)]
        public string Alamat { get; set; }

        public int? DesaId { get; set; }

        [StringLength(50)]
        public string Desa { get; set; }

        public int? KecamatanId { get; set; }

        [StringLength(50)]
        public string Kecamatan { get; set; }

        public int? KabupatenId { get; set; }

        [StringLength(50)]
        public string Kabupaten { get; set; }

        public int? PropinsiId { get; set; }

        [StringLength(50)]
        public string Propinsi { get; set; }

        [StringLength(500)]
        public string BatasTimur { get; set; }

        [StringLength(500)]
        public string BatasSelatan { get; set; }

        [StringLength(500)]
        public string BatasBarat { get; set; }

        [StringLength(500)]
        public string BatasUtara { get; set; }

        public int? StatusAsetId { get; set; }

        public int? StatusJaminanId { get; set; }

        [StringLength(50)]
        public string PeriodeJaminan { get; set; }

        public double? NilaiJaminan { get; set; }

        [StringLength(50)]
        public string PeriodeBOT { get; set; }

        [StringLength(50)]
        public string PeriodeKSO { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalDikembalikan { get; set; }

        public int? JenisKategoriPengembanganId { get; set; }

        [StringLength(50)]
        public string JenisKategoriPengembangan { get; set; }

        [StringLength(50)]
        public string JenisPengembanganId { get; set; }

        [StringLength(500)]
        public string JenisPengembangan { get; set; }

        public byte? StatusInventoriId { get; set; }

        [StringLength(50)]
        public string StatusInventori { get; set; }

        public byte? StatusOperasionalId { get; set; }

        [StringLength(50)]
        public string StatusOperasional { get; set; }

        public int? JenisPeruntukanId { get; set; }

        public int? JenisPenggunaanId { get; set; }

        public int? JenisPemanfaatanId { get; set; }

        public double? NilaiAset { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalSerahTerima { get; set; }

        [StringLength(50)]
        public string PeriodePerijinan { get; set; }

        [StringLength(50)]
        public string PeriodeKonstruksi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TanggalOperasi { get; set; }

        public int? StatusDisputeId { get; set; }

        [StringLength(500)]
        public string KeteranganDispute { get; set; }

        public int? JenisDisposedId { get; set; }

        [StringLength(500)]
        public string KeteranganDisposed { get; set; }

        [StringLength(2000)]
        public string Keterangan { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? Published { get; set; }

        [StringLength(50)]
        public string SessId { get; set; }
    }
}
