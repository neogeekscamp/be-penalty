using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_audit_config")]
    public class sys_audit_config
    {
        [Key]
        [StringLength(50)]
        public string TableName { get; set; }

        public int AuditOption { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }
    }
}
