using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_audittrail")]
    public class sys_audittrail
    {
        public long id { get; set; }

        public DateTime DateActivity { get; set; }

        [Required]
        [StringLength(15)]
        public string UserHostAddress { get; set; }

        [Required]
        [StringLength(30)]
        public string Username { get; set; }

        [Required]
        [StringLength(50)]
        public string TableName { get; set; }

        [StringLength(250)]
        public string PrymariKeyName { get; set; }

        [StringLength(250)]
        public string PrymariKeyValue { get; set; }

        [Required]
        [StringLength(50)]
        public string ActionType { get; set; }

        [Required]
        [StringLength(250)]
        public string TitleAction { get; set; }

        [StringLength(250)]
        public string RawUrl { get; set; }

        [StringLength(250)]
        public string UserAgent { get; set; }

        [Column(TypeName = "text")]
        public string Detail { get; set; }
    }
}
