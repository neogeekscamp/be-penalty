using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_bulan")]
    public class sys_bulan
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Kode { get; set; }

        [StringLength(50)]
        public string Nama { get; set; }
    }
}
