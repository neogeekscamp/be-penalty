using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_config")]
    public class sys_config
    {
        [Required]
        [StringLength(50)]
        public string Module { get; set; }

        [Key]
        [StringLength(50)]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Value { get; set; }

        public int Ordering { get; set; }
    }
}
