using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_forms")]
    public class sys_forms
    {
        [Key]
        [StringLength(255)]
        public string FormName { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        [NotMapped]
        public virtual ICollection<sys_rules> sys_rules { get; set; }
    }
}
