using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_groups")]
    public class sys_groups
    {
        [Key]
        public int GroupId { get; set; }

        public int? ParentId { get; set; }

        [Required]
        [StringLength(100)]
        public string GroupName { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public int Enable { get; set; }

        public int Ordering { get; set; }

        public int Lvl { get; set; }

        public int Lft { get; set; }

        public int Rgt { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<sys_groups> sys_groups1 { get; set; }

        public virtual sys_groups sys_groups2 { get; set; }

        [NotMapped]
        public virtual ICollection<sys_rules> sys_rules { get; set; }

        [NotMapped]
        public virtual ICollection<sys_users> sys_users { get; set; }
    }
}
