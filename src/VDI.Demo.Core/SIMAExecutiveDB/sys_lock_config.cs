using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_lock_config")]
    public class sys_lock_config
    {
        [Key]
        [StringLength(50)]
        public string TableName { get; set; }

        public int LockOption { get; set; }

        public int? LockTime { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public int? Ordering { get; set; }
    }
}
