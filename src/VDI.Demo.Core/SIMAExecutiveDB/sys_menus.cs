using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_menus")]
    public class sys_menus
    {
        [Key]
        public int MenuId { get; set; }

        public int? ParentId { get; set; }

        public int? MenuTypeId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(250)]
        public string LangVar { get; set; }

        [StringLength(1024)]
        public string Link { get; set; }

        [Column(TypeName = "text")]
        public string Params { get; set; }

        [StringLength(16)]
        public string Target { get; set; }

        public byte Enable { get; set; }

        [StringLength(255)]
        public string Img { get; set; }

        public int Ordering { get; set; }

        public int Lvl { get; set; }

        public int Lft { get; set; }

        public int Rgt { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual sys_menu_type sys_menu_type { get; set; }

        [NotMapped]
         public virtual ICollection<sys_menus> sys_menus1 { get; set; }

        public virtual sys_menus sys_menus2 { get; set; }

        [NotMapped]
        public virtual ICollection<sys_rules> sys_rules { get; set; }
    }
}
