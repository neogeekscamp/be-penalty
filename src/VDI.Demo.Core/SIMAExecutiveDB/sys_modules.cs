using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_modules")]
    public class sys_modules
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        [StringLength(50)]
        public string Kode { get; set; }

        [StringLength(100)]
        public string Nama { get; set; }

        [StringLength(50)]
        public string Kewenangan { get; set; }

        [StringLength(250)]
        public string NamaForm { get; set; }

        [StringLength(4000)]
        public string Batasan { get; set; }

        [StringLength(4000)]
        public string Keterangan { get; set; }

        public int? Versi { get; set; }

        [StringLength(50)]
        public string DevBy { get; set; }

        public DateTime? DevDate { get; set; }

        [StringLength(4000)]
        public string DevDesc { get; set; }

        public double? DevProgres { get; set; }

        [StringLength(50)]
        public string TestBy { get; set; }

        public DateTime? TestDate { get; set; }

        public double? TestProgres { get; set; }

        public int? Status { get; set; }

        public int? Ordering { get; set; }

        public byte? Lvl { get; set; }

        public int? Lft { get; set; }

        public int? Rgt { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
