using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_rules")]
    public class sys_rules
    {
        [Key]
        public int RuleId { get; set; }

        public int? ParentId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public byte Enable { get; set; }

        public int Ordering { get; set; }

        public byte Lvl { get; set; }

        public int Lft { get; set; }

        public int Rgt { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [NotMapped]
        public virtual ICollection<sys_rules> sys_rules1 { get; set; }

        public virtual sys_rules sys_rules2 { get; set; }

        [NotMapped]
        public virtual ICollection<sys_forms> sys_forms { get; set; }

        [NotMapped]
        public virtual ICollection<sys_groups> sys_groups { get; set; }

        [NotMapped]
        public virtual ICollection<sys_menus> sys_menus { get; set; }
    }
}
