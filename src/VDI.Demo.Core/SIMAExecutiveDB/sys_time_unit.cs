using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_time_unit")]
    public class sys_time_unit
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Kode { get; set; }

        [StringLength(50)]
        public string Nama { get; set; }

        public bool Published { get; set; }
    }
}
