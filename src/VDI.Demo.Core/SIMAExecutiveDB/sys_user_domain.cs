using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_user_domain")]
    public class sys_user_domain
    {
        [Key]
        [StringLength(30)]
        public string Username { get; set; }

        [StringLength(255)]
        public string FullName { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public int ActionType { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }

        [StringLength(50)]
        public string DomainName { get; set; }
    }
}
