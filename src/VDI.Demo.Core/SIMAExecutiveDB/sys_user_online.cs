using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_user_online")]
    public class sys_user_online
    {
        [Key]
        [StringLength(50)]
        public string SessionId { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string HostAddress { get; set; }

        public DateTime LoginTime { get; set; }

        [Required]
        [StringLength(500)]
        public string UserAgent { get; set; }

        [Required]
        [StringLength(1024)]
        public string Uri { get; set; }

        [Required]
        [StringLength(500)]
        public string CurrentPage { get; set; }

        public DateTime? LastVisit { get; set; }

        [Column(TypeName = "text")]
        public string Data { get; set; }
    }
}
