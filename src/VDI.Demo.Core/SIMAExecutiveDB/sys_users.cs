using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.SIMAExecutiveDB
{
    [Table("sys_users")]
    public class sys_users
    {
        [Key]
        public int UserId { get; set; }

        [StringLength(255)]
        public string FullName { get; set; }

        [Required]
        [StringLength(30)]
        public string Username { get; set; }

        [StringLength(100)]
        public string Password { get; set; }

        [StringLength(500)]
        public string OldPassword { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(255)]
        public string OtherEmail { get; set; }

        public int? MustChangePassword { get; set; }

        public int? LoginCount { get; set; }

        public DateTime? LastLogin { get; set; }

        public int? AllowConcurrentLogin { get; set; }

        public int? PasswordChanged { get; set; }

        public int? AllowChangePassword { get; set; }

        public DateTime? LastChangePassword { get; set; }

        public int? EnablePasswordExpired { get; set; }

        public int? ReminderPasswordExpired { get; set; }

        public int? FailedLoginCount { get; set; }

        public DateTime? LastFailedLogin { get; set; }

        public DateTime? RegisteredDate { get; set; }

        public DateTime? UserExpired { get; set; }

        public int BlockUser { get; set; }

        [StringLength(100)]
        public string CodeValidationUser { get; set; }

        [StringLength(100)]
        public string CodeActivationUser { get; set; }

        [StringLength(100)]
        public string CodeForgetPassword { get; set; }

        [StringLength(5000)]
        public string Params { get; set; }

        [StringLength(500)]
        public string GroupUserId { get; set; }

        [StringLength(500)]
        public string GroupUser { get; set; }

        [StringLength(500)]
        public string UnitKerjaId { get; set; }

        [StringLength(500)]
        public string UnitKerja { get; set; }

        [StringLength(500)]
        public string SekUnitKerjaId { get; set; }

        [StringLength(500)]
        public string SekUnitKerja { get; set; }

        [StringLength(50)]
        public string DomainName { get; set; }

        [StringLength(30)]
        public string LockBy { get; set; }

        public DateTime? LockOn { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [NotMapped]
        public virtual ICollection<sys_groups> sys_groups { get; set; }
    }
}
