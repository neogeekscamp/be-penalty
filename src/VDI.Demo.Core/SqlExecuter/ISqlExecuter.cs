﻿using System;
using System.Collections.Generic;
using System.Data;

namespace VDI.Demo.SqlExecuter
{
    public interface ISqlExecuter
    {
        int Execute(string sql, params object[] parameters);
        int ExecutePropertySystem(string sql, params object[] parameters);
        IReadOnlyList<T> GetFromPersonals<T>(string sql, object parameters = null, CommandType? commandType = null);
        IReadOnlyList<T> GetFromCommPayment<T>(string sql, object parameters = null, CommandType? commandType = null);
        IReadOnlyList<T> GetFromEngin3<T>(string sql, object parameters = null, CommandType? commandType = null);
        IReadOnlyList<T> GetFromPropertySystem<T>(string sql, object parameters = null, CommandType? commandType = null);
        IReadOnlyList<T> GetFromPPOnline<T>(string server, string catalog, string user, string pass, string sql, object parameters = null, CommandType? commandType = null);
        IReadOnlyList<T> GetFromSima<T>(string server, string catalog, string user, string pass, string sql, object parameters = null, CommandType? commandType = null);
    }
}
