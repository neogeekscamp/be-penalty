﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.TAXDB
{
    [Table("FP_MS_Currency")]
    public class FP_MS_Currency : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                  "-" + currencyCode;
            }
            set { /* nothing */ }
        }
        
        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(5)]
        public string currencyCode { get; set; }

        [Required]
        [StringLength(50)]
        public string currencyName { get; set; }

        [Column(TypeName = "money")]
        public decimal currencyRate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal sort { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime modifTime { get; set; }

        [Required]
        [StringLength(50)]
        public string modifUN { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime inputTime { get; set; }

        [Required]
        [StringLength(50)]
        public string inputUN { get; set; }

        public virtual ICollection<FP_TR_FPDetail> FP_TR_FPDetail { get; set; }
    }
}
