﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.TAXDB
{
    [Table("msBatchPajakLastNo")]
    public class msBatchPajakLastNo : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return CoCode +
                  "-" + FPBranchCode +
                  "-" + FPYear;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(5)]
        public string CoCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string FPBranchCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(2)]
        public string FPYear { get; set; }

        [StringLength(50)]
        public string YearPeriod { get; set; }

        [StringLength(8)]
        public string LastFPNo { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
    }
}
