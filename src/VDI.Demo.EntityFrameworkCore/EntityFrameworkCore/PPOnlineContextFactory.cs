﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Configuration;
using VDI.Demo.Web;

namespace VDI.Demo.EntityFrameworkCore
{
    public class PPOnlineContextFactory : IDesignTimeDbContextFactory<PPOnlineDbContext>
    {
        public PPOnlineDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PPOnlineDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            PPOnlineDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DemoConsts.ConnectionStringPPOnline));

            return new PPOnlineDbContext(builder.Options);
        }
    }
}
