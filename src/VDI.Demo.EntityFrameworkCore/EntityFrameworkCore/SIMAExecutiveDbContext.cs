﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.SIMAExecutiveDB;

namespace VDI.Demo.EntityFrameworkCore
{
    public class SIMAExecutiveDbContext : AbpDbContext
    {
        public virtual DbSet<adm_kabupaten> adm_kabupaten { get; set; }
        public virtual DbSet<adm_kecamatan> adm_kecamatan { get; set; }
        public virtual DbSet<adm_kecamatan_sp> adm_kecamatan_sp { get; set; }
        public virtual DbSet<adm_negara> adm_negara { get; set; }
        public virtual DbSet<adm_propinsi> adm_propinsi { get; set;}
        public virtual DbSet<geometry_columns> geometry_columns { get; set;}
        public virtual DbSet<kota> kota { get; set;}
        public virtual DbSet<map_file> map_file { get; set;}
        public virtual DbSet<map_layer> map_layer { get; set;}
        public virtual DbSet<map_layer_class> map_layer_class { get; set;}
        public virtual DbSet<sima_akuisisi_komposisi_share> sima_akuisisi_komposisi_share { get; set;}
        public virtual DbSet<sima_akuisisi_property> sima_akuisisi_property { get; set;}
        public virtual DbSet<sima_akuisisi_property_pembayaran> sima_akuisisi_property_pembayaran { get; set;}
        public virtual DbSet<sima_akuisisi_share> sima_akuisisi_share { get; set;}
        public virtual DbSet<sima_akuisisi_share_pembayaran> sima_akuisisi_share_pembayaran { get; set;}
        public virtual DbSet<sima_aset> sima_aset { get; set;}
        public virtual DbSet<sima_aset_asuransi> sima_aset_asuransi { get; set;}
        public virtual DbSet<sima_aset_dokumen> sima_aset_dokumen { get; set;}
        public virtual DbSet<sima_aset_foto> sima_aset_foto { get; set;}
        public virtual DbSet<sima_aset_girik> sima_aset_girik { get; set;}
        public virtual DbSet<sima_aset_imb> sima_aset_imb { get; set;}
        public virtual DbSet<sima_aset_jaminan> sima_aset_jaminan{ get; set;}
        public virtual DbSet<sima_aset_kasus> sima_aset_kasus { get; set;}
        public virtual DbSet<sima_aset_pelepasan> sima_aset_pelepasan { get; set;}
        public virtual DbSet<sima_aset_penilaian> sima_aset_penilaian { get; set;}
        public virtual DbSet<sima_aset_penilaian_last> sima_aset_penilaian_last { get; set;}
        public virtual DbSet<sima_aset_perijinan> sima_aset_perijinan { get; set;}
        public virtual DbSet<sima_aset_perubahan> sima_aset_perubahan { get; set;}
        public virtual DbSet<sima_aset_relasi> sima_aset_relasi { get; set;}
        public virtual DbSet<sima_aset_reminder> sima_aset_reminder { get; set;}
        public virtual DbSet<sima_aset_sertipikat> sima_aset_sertipikat { get; set;}
        public virtual DbSet<sima_aset_sp> sima_aset_sp { get; set;}
        public virtual DbSet<sima_aset_stakeholder> sima_aset_stakeholder { get; set;}
        public virtual DbSet<sima_asuransi> sima_asuransi { get; set;}
        public virtual DbSet<sima_fasilitas> sima_fasilitas { get; set;}
        public virtual DbSet<sima_fasilitas_sp> sima_fasilitas_sp { get; set;}
        public virtual DbSet<sima_grup_stakeholder> sima_grup_stakeholder{ get; set;}
        public virtual DbSet<sima_instansi_perijinan> sima_instansi_perijinan { get; set;}
        public virtual DbSet<sima_jenis_aset> sima_jenis_aset { get; set;}
        public virtual DbSet<sima_jenis_bangunan> sima_jenis_bangunan { get; set;}
        public virtual DbSet<sima_jenis_disposed> sima_jenis_disposed { get; set;}
        public virtual DbSet<sima_jenis_fasilitas> sima_jenis_fasilitas { get; set;}
        public virtual DbSet<sima_jenis_hak_atas_tanah> sima_jenis_hak_atas_tanah { get; set; }
        public virtual DbSet<sima_jenis_kasus> sima_jenis_kasus { get; set;}
        public virtual DbSet<sima_jenis_kerjasama> sima_jenis_kerjasama { get; set;}
        public virtual DbSet<sima_jenis_pemanfaatan> sima_jenis_pemanfaatan { get; set;}
        public virtual DbSet<sima_jenis_pengembangan> sima_jenis_pengembangan { get; set;}
        public virtual DbSet<sima_jenis_penggunaan> sima_jenis_penggunaan { get; set;}
        public virtual DbSet<sima_jenis_perijinan> sima_jenis_perijinan { get; set;}
        public virtual DbSet<sima_jenis_perijinan_wilayah> sima_jenis_perijinan_wilayah { get; set;}
        public virtual DbSet<sima_jenis_peruntukan> sima_jenis_peruntukan { get; set;}
        public virtual DbSet<sima_kasus> sima_kasus { get; set;}
        public virtual DbSet<sima_kasus_pihak> sima_kasus_pihak { get; set;}
        public virtual DbSet<sima_kategori_perijinan> sima_kategori_perijinan { get; set;}
        public virtual DbSet<sima_kerjasama> sima_kerjasama { get; set;}
        public virtual DbSet<sima_kerjasama_pembayaran> sima_kerjasama_pembayaran { get; set;}
        public virtual DbSet<sima_kerjasama_reminder> sima_kerjasama_reminder { get; set;}
        public virtual DbSet<sima_kerjasama_stakeholder> sima_kerjasama_stakeholder { get; set;}
        public virtual DbSet<sima_kerjasama_tahapan> sima_kerjasama_tahapan { get; set;}
        public virtual DbSet<sima_kondisi> sima_kondisi{ get; set;}
        public virtual DbSet<sima_pbb> sima_pbb { get; set;}
        public virtual DbSet<sima_penilai> sima_penilai{ get; set;}
        public virtual DbSet<sima_penilaian> sima_penilaian { get; set;}
        public virtual DbSet<sima_perijinan> sima_perijinan { get; set;}
        public virtual DbSet<sima_perusahaan_asuransi> sima_perusahaan_asuransi { get; set;}
        public virtual DbSet<sima_reminder> sima_reminder { get; set;}
        public virtual DbSet<sima_stakeholder> sima_stakeholder { get; set;}
        public virtual DbSet<sima_stakeholder_temp> sima_stakeholder_temp { get; set;}
        public virtual DbSet<sima_status_akuisisi> sima_status_akuisisi { get; set;}
        public virtual DbSet<sima_status_aset> sima_status_aset { get; set;}
        public virtual DbSet<sima_status_dispute> sima_status_dispute { get; set;}
        public virtual DbSet<sima_status_kasus> sima_status_kasus { get; set;}
        public virtual DbSet<sima_status_kepemilikan> sima_status_kepemilikan { get; set;}
        public virtual DbSet<sima_status_kerjasama> sima_status_kerjasama { get; set;}
        public virtual DbSet<sima_status_konstruksi> sima_status_konstruksi { get; set;}
        public virtual DbSet<sima_status_operasional> sima_status_operasional { get; set;}
        public virtual DbSet<sima_status_pihak> sima_status_pihak { get; set;}
        public virtual DbSet<sima_status_putusan> sima_status_putusan { get; set;}
        public virtual DbSet<sima_tanah_sp_hist > sima_tanah_sp_hist { get; set;}
        public virtual DbSet<sima_tanah_sp_ori> sima_tanah_sp_ori { get; set;}
        public virtual DbSet<sima_tanah_sp_temp> sima_tanah_sp_temp { get; set;}
        public virtual DbSet<sima_tingkat_pengadilan> sima_tingkat_pengadilan { get; set;}
        public virtual DbSet<spatial_ref_sys> spatial_rey_sys { get; set;}
        public virtual DbSet<sys_audit_config> sys_audit_config { get; set;}
        public virtual DbSet<sys_audittrail> sys_audittrail{ get; set;}
        public virtual DbSet<sys_bulan> sys_bulan { get; set;}
        public virtual DbSet<sys_config> sys_config { get; set;}
        public virtual DbSet<sys_forms> sys_forms { get; set;}
        public virtual DbSet<sys_groups> sys_groups { get; set;}
        public virtual DbSet<sys_lock_config> sys_lock_config { get; set;}
        public virtual DbSet<sys_menus> sys_menus { get; set;}
        public virtual DbSet<sys_modules> sys_modules { get; set;}
        public virtual DbSet<sys_rules> sys_rules { get; set;}
        public virtual DbSet<sys_time_unit> sys_time_unit { get; set;}
        public virtual DbSet<sys_user_domain> sys_user_domain { get; set;}
        public virtual DbSet<sys_user_online> sys_user_online { get; set;}
        public virtual DbSet<sys_users> sys_users{ get; set;}
        public virtual DbSet<sys_users_bak> sys_user_bak{ get; set;}
        public virtual DbSet<sysdiagram> sysdiagram { get; set;}
        public virtual DbSet<TR_ReportParameter> TR_ReportParameter{ get; set;}

    public SIMAExecutiveDbContext(DbContextOptions<SIMAExecutiveDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<adm_kecamatan_sp>()
               .HasKey(p => new {p.KecamatanId });

            modelBuilder.Entity<geometry_columns>()
                .HasKey(p => new {p.f_table_catalog, p.f_table_schema, p.f_table_name, p.f_geometry_column});

            modelBuilder.Entity<sima_aset_dokumen>()
                .HasKey(p => new {p.TableNameKey, p.TablePKValue, p.Nomor });

            modelBuilder.Entity<sima_aset_foto>()
                .HasKey(p => new { p.TableNameKey, p.TablePKValue, p.Nomor });

            modelBuilder.Entity<sima_aset_relasi>()
                .HasKey(p => new {p.AsetId, p.AsalAsetId });

            modelBuilder.Entity<sima_aset_sp>()
                .HasKey(p => new { p.AsetId });

            modelBuilder.Entity<sima_fasilitas_sp>()
                .HasKey(p => new { p.FasilitasId });

            modelBuilder.Entity<sima_kerjasama_reminder>()
                .HasKey(p => new { p.KerjasamaId, p.ReminderId });

            modelBuilder.Entity<sima_status_pihak>()
                .HasKey(p => new { p.JenisKerjasamaId, p.Id });

            modelBuilder.Entity<sima_status_putusan>()
                .HasKey(p => new { p.Id, p.Kode, p.Nama });

            modelBuilder.Entity<sima_tanah_sp_ori>()
                .HasKey(p => new { p.AsetId });

            modelBuilder.Entity<spatial_ref_sys>()
                .HasKey(p => new { p.srid });

            modelBuilder.Entity<sys_audit_config>()
                .HasKey(p => new { p.TableName });

            modelBuilder.Entity<sys_config>()
                .HasKey(p => new { p.Name });

            modelBuilder.Entity<sys_forms>()
                .HasKey(p => new { p.FormName });

            modelBuilder.Entity<sys_groups>()
                .HasKey(p => new { p.GroupId });

            modelBuilder.Entity<sys_lock_config>()
                .HasKey(p => new { p.TableName });

            modelBuilder.Entity<sys_menu_type>()
                .HasKey(p => new { p.MenuTypeId });

            modelBuilder.Entity<sys_menus>()
                .HasKey(p => new { p.MenuId });

            modelBuilder.Entity<sys_rules>()
                .HasKey(p => new { p.RuleId });

            modelBuilder.Entity<sys_user_domain>()
                .HasKey(p => new { p.Username });

            modelBuilder.Entity<sys_user_online>()
                .HasKey(p => new { p.SessionId });

            modelBuilder.Entity<sys_users>()
                .HasKey(p => new { p.UserId });

            modelBuilder.Entity<sys_users_bak>()
                .HasKey(p => new { p.UserId });
            
            modelBuilder.Entity<sysdiagram>()
                .HasKey(p => new { p.diagram_id });

            modelBuilder.Entity<sima_aset>()
                .HasOne(a => a.sima_aset_reminder)
                .WithOne(b => b.sima_aset)
                .HasForeignKey<sima_aset_reminder>(e => e.AsetId);

            modelBuilder.Entity<sima_aset>()
                .HasOne(a => a.sima_aset_stakeholder)
                .WithOne(b => b.sima_aset)
                .HasForeignKey<sima_aset_stakeholder>(e => e.AsetId);



            base.OnModelCreating(modelBuilder);
        }
    }
}
