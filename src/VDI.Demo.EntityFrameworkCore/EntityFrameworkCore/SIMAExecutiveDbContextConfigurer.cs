﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace VDI.Demo.EntityFrameworkCore
{
    public class SIMAExecutiveDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<SIMAExecutiveDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<SIMAExecutiveDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
