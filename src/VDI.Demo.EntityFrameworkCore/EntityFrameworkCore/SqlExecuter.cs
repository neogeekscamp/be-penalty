﻿using Abp.Dependency;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VDI.Demo.SqlExecuter;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Data;
using System;
using Abp.UI;

namespace VDI.Demo.EntityFrameworkCore
{
    public class SqlExecuter : ISqlExecuter, ITransientDependency
    {
        private readonly IDbContextProvider<PersonalsNewDbContext> _dbContextPersonals;
        private readonly IDbContextProvider<PropertySystemDbContext> _dbContextPropertySystem;
        private readonly IDbContextProvider<DemoDbContext> _dbContextDemo;
        private readonly IDbContextProvider<NewCommDbContext> _dbContextNewComm;

        public SqlExecuter(
            IDbContextProvider<PersonalsNewDbContext> dbContextPersonals,
            IDbContextProvider<DemoDbContext> dbContextDemo,
            IDbContextProvider<PropertySystemDbContext> dbContextPropertySystem,
            IDbContextProvider<NewCommDbContext> dbContextNewComm
            )
        {
            _dbContextPersonals = dbContextPersonals;
            _dbContextDemo = dbContextDemo;
            _dbContextPropertySystem = dbContextPropertySystem;
            _dbContextNewComm = dbContextNewComm;

        }

        public int Execute(string sql, params object[] parameters)
        {
            return _dbContextPersonals.GetDbContext().Database.ExecuteSqlCommand(sql, parameters);
        }

        public IReadOnlyList<T> GetFromCommPayment<T>(string sql, object parameters = null, CommandType? commandType = null)
        {
            var tempDbConn = _dbContextNewComm.GetDbContext();
            string tempConnStr = tempDbConn.Database.GetDbConnection().ConnectionString;

            using (var conn = new SqlConnection(tempConnStr))
            {
                return conn.Query<T>(sql, parameters).ToList();
            }
        }

        public IReadOnlyList<T> GetFromEngin3<T>(string sql, object parameters = null, CommandType? commandType = null)
        {
            var tempDbConn = _dbContextPropertySystem.GetDbContext();
            string tempConnStr = tempDbConn.Database.GetDbConnection().ConnectionString;
            
            using (var conn = new SqlConnection(tempConnStr))
            {
                return conn.Query<T>(sql, parameters).ToList();
            }
                        
        }

        public IReadOnlyList<T> GetFromPPOnline<T>(string server, string catalog, string user, string pass, string sql, object parameters = null, CommandType? commandType = null)
        {
            string connectionStr = "Persist Security Info=True;Data Source="+server+"; Initial Catalog="+catalog+"; User ID="+user+";Password="+pass+";";
            try
            {
                using (var conn = new SqlConnection(connectionStr))
                {
                    return conn.Query<T>(sql, parameters).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : " + ex.Message);
            }
        }

        public IReadOnlyList<T> GetFromPersonals<T>(string sql, object parameters = null, CommandType? commandType = null)
        {
            var tempDbConn = _dbContextPersonals.GetDbContext();
            string tempConnStr = tempDbConn.Database.GetDbConnection().ConnectionString;

            using (var conn = new SqlConnection(tempConnStr))
            {
                return conn.Query<T>(sql, parameters).ToList();
            }
        }

        public IReadOnlyList<T> GetFromPropertySystem<T>(string sql, object parameters = null, CommandType? commandType = null)
        {
            var tempDbConn = _dbContextPropertySystem.GetDbContext();
            string tempConnStr = tempDbConn.Database.GetDbConnection().ConnectionString;

            using (var conn = new SqlConnection(tempConnStr))
            {
                return conn.Query<T>(sql, parameters).ToList();
            }
        }

        public int ExecutePropertySystem(string sql, params object[] parameters)
        {
            return _dbContextPropertySystem.GetDbContext().Database.ExecuteSqlCommand(sql, parameters);
        }

        public IReadOnlyList<T> GetFromSima<T>(string server, string catalog, string user, string pass, string sql, object parameters = null, CommandType? commandType = null)
        {
            string connectionStr = "Persist Security Info=True;Data Source=" + server + "; Initial Catalog=" + catalog + "; User ID=" + user + ";Password=" + pass + ";";
            try
            {
                using (var conn = new SqlConnection(connectionStr))
                {
                    return conn.Query<T>(sql, parameters).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : " + ex.Message);
            }
        }
    }
}
