using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using VDI.Demo.Authorization.Users.Profile.Dto;
using VDI.Demo.IO;
using VDI.Demo.Web.Helpers;

namespace VDI.Demo.Web.Controllers
{
    public abstract class ProfileControllerBase : DemoControllerBase
    {
        private readonly IAppFolders _appFolders;
        private const int MaxProfilePictureSize = 5242880; //5MB

        protected ProfileControllerBase(IAppFolders appFolders)
        {
            _appFolders = appFolders;
        }

        public UploadProfilePictureOutput UploadProfilePicture()
        {
            try
            {
                var profilePictureFile = Request.Form.Files.First();

                //Check input
                if (profilePictureFile == null)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
                }

                if (profilePictureFile.Length > MaxProfilePictureSize)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit", AppConsts.MaxProfilPictureBytesUserFriendlyValue));
                }

                byte[] fileBytes;
                using (var stream = profilePictureFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                if (!ImageFormatHelper.GetRawImageFormat(fileBytes).IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                {
                    throw new Exception("Uploaded file is not an accepted image file !");
                }

                //Delete old temp profile pictures
                AppFileHelper.DeleteFilesInFolderIfExists(_appFolders.TempFileDownloadFolder, "userProfileImage_" + AbpSession.GetUserId());

                //Save new picture
                var fileInfo = new FileInfo(profilePictureFile.FileName);
                var tempFileName = "userProfileImage_" + AbpSession.GetUserId() + fileInfo.Extension;
                var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
                System.IO.File.WriteAllBytes(tempFilePath, fileBytes);

                using (var bmpImage = new Bitmap(tempFilePath))
                {
                    return new UploadProfilePictureOutput
                    {
                        FileName = tempFileName,
                        Width = bmpImage.Width,
                        Height = bmpImage.Height
                    };
                }
            }
            catch (UserFriendlyException ex)
            {
                return new UploadProfilePictureOutput(new ErrorInfo(ex.Message));
            }
        }

        public JsonResult UploadSalesDocument()
        {
            try
            {
                //Check input
                if (Request.Form.Files.Count <= 0 || Request.Form.Files[0] == null)
                {
                    throw new UserFriendlyException(L("File_Change_Error"));
                }

                var file = Request.Form.Files[0];

                if (file.Length > MaxProfilePictureSize)
                {
                    throw new UserFriendlyException(L("File_Warn_SizeLimit"));
                }

                if (!Directory.Exists(_appFolders.TempFileDownloadFolder + "\\SalesDocument\\"))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(_appFolders.TempFileDownloadFolder + "\\SalesDocument\\");
                }

                byte[] fileBytes;
                using (var stream = file.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var date = DateTime.Now.ToString("yyyyMMddHHmmss");

                var fileInfo = new FileInfo(file.FileName);
                string ext = fileInfo.Extension.ToLower().Trim();
                if (ext.Equals(".pdf") || ext.Equals(".jpg") || ext.Equals(".jpeg") || ext.Equals(".png"))
                {
                    var tempFileName = "SalesDocument_" + date + fileInfo.Extension;
                    var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder + "\\SalesDocument\\", tempFileName);
                    tempFilePath.Replace("\\\\", "\\");
                    System.IO.File.WriteAllBytes(tempFilePath, fileBytes);
                    //var fileName = Path.GetFileName(tempFilePath);

                    return Json(new AjaxResponse(new { fileName = tempFileName }));
                }
                else
                {
                    throw new UserFriendlyException("Uploaded file format is not correct !");
                }
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}